
import sys
import argparse
import time
import uuid
import re
import io
import json
from datetime import datetime
import dateutil.parser
from html import escape as html_escape
import utils
import cda_austrian_extension
import fhir4
from fhir4 import string, base64Binary, markdown, code, dateTime, uri, boolean, decimal

fhir4.Resource.exportJsonAttributes = utils.exportJsonAttributesResource
fhir4.ResourceContainer.exportJsonResult = utils.exportJsonResultResourceContainer
fhir4.Narrative.exportJsonResult = utils.exportJsonResultNarrative
fhir4.Element.exportJsonResult = utils.exportJsonResultElement

description_text = "This has been compiled by the MApping LAnguage Compiler for Health Data, short MaLaC-HD. See arguments for more details."
one_timestamp = datetime(2000,1,1)
fhirpath_utils = utils.FHIRPathUtils(fhir4)
shared_vars = {}

curr_uuid = 0
def dummy_uuid():
  global curr_uuid
  curr_uuid += 1
  return str(uuid.uuid3(uuid.NAMESPACE_URL, str(curr_uuid)))

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description_text)
    parser.add_argument(
       '-s', '--source', help='the source file path', required=True
    )
    parser.add_argument(
       '-t', '--target', help='the target file path the result will be written to', required=True
    )
    return parser

def transform(source_path, target_path):
    start = time.time()
    print('+++++++ Transformation from '+source_path+' to '+target_path+' started +++++++')

    if source_path.endswith('.xml'):
        cda = cda_austrian_extension.parse(source_path, silence=True)
    else:
        raise BaseException('Unknown source file ending')
    fhir_bundle = fhir4.Bundle()
    CdaToFhirBundle(cda, fhir_bundle)
    with open(target_path, 'w', newline='', encoding='utf-8') as f:
        if target_path.endswith('.xml'):
            f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            fhir_bundle.export(f, 0, namespacedef_='xmlns="http://hl7.org/fhir" xmlns:v3="urn:hl7-org:v3"')
        elif target_path.endswith('.json'):
            json.dump(fhir_bundle.exportJson(), f)
        else:
            raise BaseException('Unknown target file ending')

    print('altogether in '+str(round(time.time()-start,3))+' seconds.')
    print('+++++++ Transformation from '+source_path+' to '+target_path+' ended  +++++++')

def CdaToFhirBundle(cda, fhir_bundle):
    fhir_bundle.id = string(value=dummy_uuid())
    fhir_bundle.type_ = string(value='document')
    fhir_bundle_meta = fhir4.Meta()
    if fhir_bundle.meta is not None:
        fhir_bundle_meta = fhir_bundle.meta
    else:
        fhir_bundle.meta = fhir_bundle_meta
    fhir_bundle_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/Bundle-eu-lab'))
    if cda.id:
        fhir_bundle.identifier = fhir4.Identifier()
        II(cda.id, fhir_bundle.identifier)
    if cda.effectiveTime:
        fhir_bundle.timestamp = fhir4.instant()
        TSInstant(cda.effectiveTime, fhir_bundle.timestamp)
    fhir_bundle_entry_1 = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry_1)
    fhir_composition = fhir4.Composition()
    fhir_bundle_entry_1.resource = fhir4.ResourceContainer(Composition=fhir_composition)
    fhir_composition_uuid = string(value=dummy_uuid())
    fhir_composition.id = fhir_composition_uuid
    fhir_composition.status = string(value='final')
    fhir_bundle_entry_1.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_composition_uuid is None else fhir_composition_uuid if isinstance(fhir_composition_uuid, str) else fhir_composition_uuid.value)))
    fhir_bundle_entry_4 = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry_4)
    fhir_diagnosticReport = fhir4.DiagnosticReport()
    fhir_bundle_entry_4.resource = fhir4.ResourceContainer(DiagnosticReport=fhir_diagnosticReport)
    fhir_diagnosticReport_id = string(value=dummy_uuid())
    fhir_diagnosticReport.id = fhir_diagnosticReport_id
    fhir_diagnosticReport.status = string(value='final')
    fhir_bundle_entry_4.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_diagnosticReport_id is None else fhir_diagnosticReport_id if isinstance(fhir_diagnosticReport_id, str) else fhir_diagnosticReport_id.value)))
    fhir_bundle_entry_2 = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry_2)
    fhir_patient = fhir4.Patient()
    fhir_bundle_entry_2.resource = fhir4.ResourceContainer(Patient=fhir_patient)
    fhir_patient_uuid = string(value=dummy_uuid())
    fhir_patient.id = fhir_patient_uuid
    fhir_bundle_entry_2.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_patient_uuid is None else fhir_patient_uuid if isinstance(fhir_patient_uuid, str) else fhir_patient_uuid.value)))
    fhir_patient_meta = fhir4.Meta()
    if fhir_patient.meta is not None:
        fhir_patient_meta = fhir_patient.meta
    else:
        fhir_patient.meta = fhir_patient_meta
    fhir_patient_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/Patient-eu-xpandh'))
    fhir_bundle_entry_5 = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry_5)
    fhir_serviceRequest = fhir4.ServiceRequest()
    fhir_bundle_entry_5.resource = fhir4.ResourceContainer(ServiceRequest=fhir_serviceRequest)
    fhir_serviceRequest_id = string(value=dummy_uuid())
    fhir_serviceRequest.id = fhir_serviceRequest_id
    fhir_bundle_entry_5.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_serviceRequest_id is None else fhir_serviceRequest_id if isinstance(fhir_serviceRequest_id, str) else fhir_serviceRequest_id.value)))
    fhir_serviceRequest_meta = fhir4.Meta()
    if fhir_serviceRequest.meta is not None:
        fhir_serviceRequest_meta = fhir_serviceRequest.meta
    else:
        fhir_serviceRequest.meta = fhir_serviceRequest_meta
    fhir_serviceRequest.status = string(value='completed')
    fhir_serviceRequest.intent = string(value='order')
    fhir_serviceRequest_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/ServiceRequest-lab-xpandh'))
    fhir_composition_extenstion = fhir4.Extension()
    fhir_composition.extension.append(fhir_composition_extenstion)
    fhir_composition_extenstion.url = 'http://hl7.eu/fhir/laboratory/StructureDefinition/composition-basedOn-order-or-requisition'
    fhir_diagnosticReport_composition_reference = fhir4.Reference()
    fhir_composition_extenstion.valueReference = fhir_diagnosticReport_composition_reference
    if not fhir_serviceRequest.id:
        fhir_serviceRequest.id = string(value=dummy_uuid())
    fhir_diagnosticReport_composition_reference.reference = string(value=type(fhir_serviceRequest).__name__ + '/' + fhir_serviceRequest.id.value)
    fhir_diagnosticReport_composition_reference.type_ = uri(value='ServiceRequest')
    fhir_composition_subject_reference = fhir4.Reference()
    fhir_composition.subject = fhir_composition_subject_reference
    if not fhir_patient.id:
        fhir_patient.id = string(value=dummy_uuid())
    fhir_composition_subject_reference.reference = string(value=type(fhir_patient).__name__ + '/' + fhir_patient.id.value)
    fhir_composition_subject_reference.type_ = uri(value='Patient')
    fhir_diagnosticReport_extension = fhir4.Extension()
    fhir_diagnosticReport.extension.append(fhir_diagnosticReport_extension)
    fhir_diagnosticReport_extension.url = 'http://hl7.org/fhir/5.0/StructureDefinition/extension-DiagnosticReport.composition'
    fhir_diagnosticReport_composition_reference = fhir4.Reference()
    fhir_diagnosticReport_extension.valueReference = fhir_diagnosticReport_composition_reference
    if not fhir_composition.id:
        fhir_composition.id = string(value=dummy_uuid())
    fhir_diagnosticReport_composition_reference.reference = string(value=type(fhir_composition).__name__ + '/' + fhir_composition.id.value)
    fhir_diagnosticReport_composition_reference.type_ = uri(value='Composition')
    fhir_diagnosticReport_basedOn_reference = fhir4.Reference()
    fhir_diagnosticReport.basedOn.append(fhir_diagnosticReport_basedOn_reference)
    if not fhir_serviceRequest.id:
        fhir_serviceRequest.id = string(value=dummy_uuid())
    fhir_diagnosticReport_basedOn_reference.reference = string(value=type(fhir_serviceRequest).__name__ + '/' + fhir_serviceRequest.id.value)
    fhir_diagnosticReport_basedOn_reference.type_ = uri(value='ServiceRequest')
    fhir_diagnosticReport_subject_reference = fhir4.Reference()
    fhir_diagnosticReport.subject = fhir_diagnosticReport_subject_reference
    if not fhir_patient.id:
        fhir_patient.id = string(value=dummy_uuid())
    fhir_diagnosticReport_subject_reference.reference = string(value=type(fhir_patient).__name__ + '/' + fhir_patient.id.value)
    fhir_diagnosticReport_subject_reference.type_ = uri(value='Patient')
    fhir_serviceRequest_subject_reference = fhir4.Reference()
    fhir_serviceRequest.subject = fhir_serviceRequest_subject_reference
    if not fhir_patient.id:
        fhir_patient.id = string(value=dummy_uuid())
    fhir_serviceRequest_subject_reference.reference = string(value=type(fhir_patient).__name__ + '/' + fhir_patient.id.value)
    fhir_serviceRequest_subject_reference.type_ = uri(value='Patient')
    fhir_bundle_entry01 = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry01)
    fhir_practitionerRole = fhir4.PractitionerRole()
    fhir_bundle_entry01.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
    fhir_practitionerRole_id = string(value=dummy_uuid())
    fhir_practitionerRole.id = fhir_practitionerRole_id
    fhir_bundle_entry01.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
    CdaHeaderToFhirComposition(cda, fhir_composition, fhir_patient, fhir_diagnosticReport, fhir_serviceRequest, fhir_bundle)
    CdaHeaderToFhirDiagnosticReport(cda, fhir_diagnosticReport)
    cda_component = cda.component
    if cda_component:
        cda_structuredBody = cda_component.structuredBody
        CdaToPractitionerRole(cda, fhir_practitionerRole, fhir_bundle)
        CdaBodyToFhirComposition(cda, cda_structuredBody, fhir_composition, fhir_practitionerRole, fhir_patient, fhir_diagnosticReport, fhir_bundle)

def CdaHeaderToFhirComposition(cda, fhir_composition, fhir_patient, fhir_diagnosticReport, fhir_serviceRequest, fhir_bundle):
    fhir_composition_meta = fhir4.Meta()
    if fhir_composition.meta is not None:
        fhir_composition_meta = fhir_composition.meta
    else:
        fhir_composition.meta = fhir_composition_meta
    fhir_composition_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/Composition-eu-lab'))
    cda_code = cda.code
    if cda_code:
        fhir_composition_category = fhir4.CodeableConcept()
        fhir_composition.category.append(fhir_composition_category)
        if cda_code.originalText:
            fhir_composition_category.text = fhir4.string()
            EDstring(cda_code.originalText, fhir_composition_category.text)
        coding = fhir4.Coding()
        fhir_composition_category.coding.append(coding)
        CECoding(cda_code, coding)
    cda_code = cda.code
    if cda_code:
        for translation in cda_code.translation or []:
            fhir_composition.type_ = fhir4.CodeableConcept()
            CDCodeableConcept(translation, fhir_composition.type_)
        if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda_code] for v2 in fhirpath_utils.get(v1,'translation')])])):
            type_coding = fhir4.CodeableConcept()
            fhir_composition.type_ = type_coding
            coding_coding = fhir4.Coding()
            type_coding.coding.append(coding_coding)
            coding_coding.system = uri(value='http://loinc.org')
            coding_coding.code = string(value='11502-2')
    cda_title = cda.title
    if cda_title:
        fhir_composition.title = string(value=utils.single([v2 for v1 in [cda_title] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    cda_statusCode = cda.statusCode
    if cda_statusCode:
        if utils.single([bool([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'statusCode')])]):
            cda_code = cda_statusCode.code
            if cda_code:
                trans_out = translate(url='cda-sdtc-statuscode-2-fhir-composition-status', code=(cda_code if isinstance(cda_code, str) else cda_code.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                fhir_composition.status = string(value=match)
    if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'statusCode')])])):
        fhir_composition.status = string(value='final')
    if cda.effectiveTime:
        fhir_composition.date = fhir4.dateTime()
        TSDateTime(cda.effectiveTime, fhir_composition.date)
    if cda.confidentialityCode:
        fhir_composition.confidentiality = fhir4.Confidentiality()
        transform_default(cda.confidentialityCode, fhir_composition.confidentiality, code)
    if cda.languageCode:
        fhir_composition.language = fhir4.code()
        transform_default(cda.languageCode, fhir_composition.language)
    if cda.setId:
        fhir_composition.identifier = fhir4.Identifier()
        II(cda.setId, fhir_composition.identifier)
    cda_versionNumber = cda.versionNumber
    if cda_versionNumber:
        cda_versionNumber_value = cda_versionNumber.value
        if cda_versionNumber_value:
            fhir_composition_extenstion = fhir4.Extension()
            fhir_composition.extension.append(fhir_composition_extenstion)
            fhir_composition_extenstion.url = 'http://hl7.org/fhir/StructureDefinition/composition-clinicaldocument-versionNumber'
            fhir_composition_extenstion.valueString = string(value=str(cda_versionNumber_value))
    for cda_recordTarget in cda.recordTarget or []:
        cda_patientRole = cda_recordTarget.patientRole
        CdaPatientRoleToFhirPatient(cda_patientRole, fhir_patient, fhir_bundle)
    for cda_author in cda.author or []:
        if utils.single([bool([v4 for v3 in [v2 for v1 in [cda_author] for v2 in fhirpath_utils.get(v1,'assignedAuthor')] for v4 in fhirpath_utils.get(v3,'assignedPerson')])]):
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_practitionerRole = fhir4.PractitionerRole()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
            fhir_practitionerRole_id = string(value=dummy_uuid())
            fhir_practitionerRole.id = fhir_practitionerRole_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
            fhir_composition_author_reference = fhir4.Reference()
            fhir_composition.author.append(fhir_composition_author_reference)
            if not fhir_practitionerRole.id:
                fhir_practitionerRole.id = string(value=dummy_uuid())
            fhir_composition_author_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
            fhir_composition_author_reference.type_ = uri(value='PractitionerRole')
            CdaAuthorToFhirPractitionerRole(cda_author, fhir_practitionerRole, fhir_bundle)
    for cda_author in cda.author or []:
        if utils.single([bool([v4 for v3 in [v2 for v1 in [cda_author] for v2 in fhirpath_utils.get(v1,'assignedAuthor')] for v4 in fhirpath_utils.get(v3,'assignedAuthoringDevice')])]):
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_device = fhir4.Device()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(Device=fhir_device)
            fhir_device_id = string(value=dummy_uuid())
            fhir_device.id = fhir_device_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_device_id is None else fhir_device_id if isinstance(fhir_device_id, str) else fhir_device_id.value)))
            fhir_composition_author_reference = fhir4.Reference()
            fhir_composition.author.append(fhir_composition_author_reference)
            if not fhir_device.id:
                fhir_device.id = string(value=dummy_uuid())
            fhir_composition_author_reference.reference = string(value=type(fhir_device).__name__ + '/' + fhir_device.id.value)
            fhir_composition_author_reference.type_ = uri(value='Device')
            CdaAuthorToFhirDevice(cda_author, fhir_device, fhir_bundle)
    cda_custodian = cda.custodian
    if cda_custodian:
        cda_assignedCustodian = cda_custodian.assignedCustodian
        if cda_assignedCustodian:
            cda_representedCustodianOrganization = cda_assignedCustodian.representedCustodianOrganization
            if cda_representedCustodianOrganization:
                fhir_bundle_entry = fhir4.Bundle_Entry()
                fhir_bundle.entry.append(fhir_bundle_entry)
                fhir_custodian_organization = fhir4.Organization()
                fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_custodian_organization)
                fhir_custodian_organization_id = string(value=dummy_uuid())
                fhir_custodian_organization.id = fhir_custodian_organization_id
                fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_custodian_organization_id is None else fhir_custodian_organization_id if isinstance(fhir_custodian_organization_id, str) else fhir_custodian_organization_id.value)))
                fhir_composition_custodian_reference = fhir4.Reference()
                fhir_composition.custodian = fhir_composition_custodian_reference
                if not fhir_custodian_organization.id:
                    fhir_custodian_organization.id = string(value=dummy_uuid())
                fhir_composition_custodian_reference.reference = string(value=type(fhir_custodian_organization).__name__ + '/' + fhir_custodian_organization.id.value)
                fhir_composition_custodian_reference.type_ = uri(value='Organization')
                for id_ in cda_representedCustodianOrganization.id or []:
                    fhir_custodian_organization.identifier.append(fhir4.Identifier())
                    II(id_, fhir_custodian_organization.identifier[-1])
                if cda_representedCustodianOrganization.name:
                    fhir_custodian_organization.name = fhir4.string()
                    transform_default(cda_representedCustodianOrganization.name, fhir_custodian_organization.name)
                for telecom in cda_representedCustodianOrganization.telecom or []:
                    fhir_custodian_organization.telecom.append(fhir4.ContactPoint())
                    TELContactPoint(telecom, fhir_custodian_organization.telecom[-1])
                if cda_representedCustodianOrganization.addr:
                    fhir_custodian_organization.address.append(fhir4.Address())
                    CdaAdressCompilationToFhirAustrianAddress(cda_representedCustodianOrganization.addr, fhir_custodian_organization.address[-1])
    for cda_legalAuthenticator in cda.legalAuthenticator or []:
        fhir_composition_attester = fhir4.Composition_Attester()
        fhir_composition.attester.append(fhir_composition_attester)
        if cda_legalAuthenticator.time:
            fhir_composition_attester.time = fhir4.dateTime()
            TSDateTime(cda_legalAuthenticator.time, fhir_composition_attester.time)
        fhir_composition_attester.mode = string(value='legal')
        cda_legalAuthenticator_assignedEntity = cda_legalAuthenticator.assignedEntity
        if cda_legalAuthenticator_assignedEntity:
            fhir_bundle_entry01 = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry01)
            fhir_practitionerRole = fhir4.PractitionerRole()
            fhir_bundle_entry01.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
            fhir_practitionerRole_id = string(value=dummy_uuid())
            fhir_practitionerRole.id = fhir_practitionerRole_id
            fhir_bundle_entry01.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
            fhir_composition_attester_reference = fhir4.Reference()
            fhir_composition_attester.party = fhir_composition_attester_reference
            if not fhir_practitionerRole.id:
                fhir_practitionerRole.id = string(value=dummy_uuid())
            fhir_composition_attester_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
            fhir_composition_attester_reference.type_ = uri(value='PractitionerRole')
            CdaAssignedEntityToFhirPractitionerRole(cda_legalAuthenticator_assignedEntity, fhir_practitionerRole, fhir_bundle)
    for cda_orderingProvider in cda.participant or []:
        if utils.single(fhirpath_utils.equals([v2 for v1 in [cda_orderingProvider] for v2 in fhirpath_utils.get(v1,'typeCode')], '==', ['REF'])):
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_practitionerRole = fhir4.PractitionerRole()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
            fhir_practitionerRole_id = string(value=dummy_uuid())
            fhir_practitionerRole.id = fhir_practitionerRole_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
            fhir_serviceRequest_requester_reference = fhir4.Reference()
            fhir_serviceRequest.requester = fhir_serviceRequest_requester_reference
            if not fhir_practitionerRole.id:
                fhir_practitionerRole.id = string(value=dummy_uuid())
            fhir_serviceRequest_requester_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
            fhir_serviceRequest_requester_reference.type_ = uri(value='PractitionerRole')
            cda_orderingProvider_time = cda_orderingProvider.time
            if cda_orderingProvider_time:
                v = cda_orderingProvider_time.value
                if v:
                    fhir_serviceRequest.authoredOn = dateTime(value=dateutil.parser.parse(v).isoformat())
            cda_associatedEntity = cda_orderingProvider.associatedEntity
            CdaAssociatedEntityToFhirPractitionerRole(cda_associatedEntity, fhir_practitionerRole, fhir_bundle)
    for cda_inFulFillmentOf in cda.inFulfillmentOf or []:
        cda_inFulFillmentOf_order = cda_inFulFillmentOf.order
        if cda_inFulFillmentOf_order:
            for id__ in cda_inFulFillmentOf_order.id or []:
                fhir_serviceRequest.identifier.append(fhir4.Identifier())
                II(id__, fhir_serviceRequest.identifier[-1])
    for cda_documentationOf in cda.documentationOf or []:
        cda_documentationOf_serviceEvent = cda_documentationOf.serviceEvent
        if cda_documentationOf_serviceEvent:
            fhir_composition_event = fhir4.Composition_Event()
            fhir_composition.event.append(fhir_composition_event)
            for serviceEvent_id in cda_documentationOf_serviceEvent.id or []:
                serviceEvent_id_root = serviceEvent_id.root
                if serviceEvent_id_root:
                    event_codeableConcept = fhir4.CodeableConcept()
                    fhir_composition_event.code.append(event_codeableConcept)
                    event_codeableConcept.text = string(value='serviceEvent-id')
                    event_codeableConcept_coding = fhir4.Coding()
                    event_codeableConcept.coding.append(event_codeableConcept_coding)
                    event_codeableConcept_coding.code = string(value=serviceEvent_id_root)
            if cda_documentationOf_serviceEvent.code:
                fhir_composition_event.code.append(fhir4.CodeableConcept())
                transform_default(cda_documentationOf_serviceEvent.code, fhir_composition_event.code[-1])
            if cda_documentationOf_serviceEvent.effectiveTime:
                fhir_composition_event.period = fhir4.Period()
                IVLTSPeriod(cda_documentationOf_serviceEvent.effectiveTime, fhir_composition_event.period)
    for cda_relatedDocument in cda.relatedDocument or []:
        cda_parentDocument = cda_relatedDocument.parentDocument
        if cda_parentDocument:
            fhir_composition_relatesTo = fhir4.Composition_RelatesTo()
            fhir_composition.relatesTo.append(fhir_composition_relatesTo)
            fhir_composition_relatesTo.code = string(value='replaces')
            for cda_parentDocument_id in cda_parentDocument.id or []:
                fhir_target_identifier = fhir4.Identifier()
                fhir_composition_relatesTo.targetIdentifier = fhir_target_identifier
                II(cda_parentDocument_id, fhir_target_identifier)
    cda_componentOf = cda.componentOf
    if cda_componentOf:
        cda_encompassingEncounter = cda_componentOf.encompassingEncounter
        if cda_encompassingEncounter:
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_encounter = fhir4.Encounter()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(Encounter=fhir_encounter)
            fhir_encounter_id = string(value=dummy_uuid())
            fhir_encounter.id = fhir_encounter_id
            fhir_encounter.status = string(value='finished')
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_encounter_id is None else fhir_encounter_id if isinstance(fhir_encounter_id, str) else fhir_encounter_id.value)))
            fhir_composition_encounter_reference = fhir4.Reference()
            fhir_composition.encounter = fhir_composition_encounter_reference
            if not fhir_encounter.id:
                fhir_encounter.id = string(value=dummy_uuid())
            fhir_composition_encounter_reference.reference = string(value=type(fhir_encounter).__name__ + '/' + fhir_encounter.id.value)
            fhir_composition_encounter_reference.type_ = uri(value='Encounter')
            CdaEncompassingEncounterToFhirEncounter(cda_encompassingEncounter, fhir_encounter, fhir_bundle)

def CdaHeaderToFhirDiagnosticReport(cda, fhir_diagnosticReport):
    fhir_diagnosticReport_meta = fhir4.Meta()
    if fhir_diagnosticReport.meta is not None:
        fhir_diagnosticReport_meta = fhir_diagnosticReport.meta
    else:
        fhir_diagnosticReport.meta = fhir_diagnosticReport_meta
    fhir_diagnosticReport_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/DiagnosticReport-eu-lab'))
    cda_code = cda.code
    if cda_code:
        fhir_diagnosticReport_category = fhir4.CodeableConcept()
        fhir_diagnosticReport.category.append(fhir_diagnosticReport_category)
        if cda_code.originalText:
            fhir_diagnosticReport_category.text = fhir4.string()
            EDstring(cda_code.originalText, fhir_diagnosticReport_category.text)
        coding = fhir4.Coding()
        fhir_diagnosticReport_category.coding.append(coding)
        CECoding(cda_code, coding)
    cda_code = cda.code
    if cda_code:
        for translation in cda_code.translation or []:
            fhir_diagnosticReport.code = fhir4.CodeableConcept()
            CDCodeableConcept(translation, fhir_diagnosticReport.code)
        if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda_code] for v2 in fhirpath_utils.get(v1,'translation')])])):
            code_coding = fhir4.CodeableConcept()
            fhir_diagnosticReport.code = code_coding
            coding_coding = fhir4.Coding()
            code_coding.coding.append(coding_coding)
            coding_coding.system = uri(value='http://loinc.org')
            coding_coding.code = string(value='11502-2')
    cda_statusCode = cda.statusCode
    if cda_statusCode:
        if utils.single([bool([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'statusCode')])]):
            cda_code = cda_statusCode.code
            if cda_code:
                trans_out = translate(url='cda-sdtc-statuscode-2-fhir-composition-status', code=(cda_code if isinstance(cda_code, str) else cda_code.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                fhir_diagnosticReport.status = string(value=match)
    if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'statusCode')])])):
        fhir_diagnosticReport.status = string(value='final')
    cda_effectiveTime = cda.effectiveTime
    if cda_effectiveTime:
        fhir_diagnosticReport_effective = fhir4.dateTime()
        fhir_diagnosticReport.effectiveDateTime = fhir_diagnosticReport_effective
        TSDateTime(cda_effectiveTime, fhir_diagnosticReport_effective)
    if cda.setId:
        fhir_diagnosticReport.identifier.append(fhir4.Identifier())
        II(cda.setId, fhir_diagnosticReport.identifier[-1])

def CdaPatientRoleToFhirPatient(cda_patientRole, fhir_patient, fhir_bundle):
    fhir_patient_meta = fhir4.Meta()
    if fhir_patient.meta is not None:
        fhir_patient_meta = fhir_patient.meta
    else:
        fhir_patient.meta = fhir_patient_meta
    fhir_patient_meta.profile.append(string(value='http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-patient'))
    if len(cda_patientRole.id) > 0:
        cda_patientRole_id = cda_patientRole.id[0]
        fhir_patient_identifier = fhir4.Identifier()
        fhir_patient.identifier.append(fhir_patient_identifier)
        II(cda_patientRole_id, fhir_patient_identifier)
        identifier_type = fhir4.CodeableConcept()
        if fhir_patient_identifier.type_ is not None:
            identifier_type = fhir_patient_identifier.type_
        else:
            fhir_patient_identifier.type_ = identifier_type
        type_coding = fhir4.Coding()
        identifier_type.coding.append(type_coding)
        type_coding.system = uri(value='http://terminology.hl7.org/CodeSystem/v2-0203')
        type_coding.code = string(value='PI')
        type_coding.display = string(value='Patient internal identifier')
    for cda_patientRole_id in cda_patientRole.id[1:]:
        fhir_patient_identifier = fhir4.Identifier()
        fhir_patient.identifier.append(fhir_patient_identifier)
        II(cda_patientRole_id, fhir_patient_identifier)
        if utils.single((fhirpath_utils.equals([v2 for v1 in [cda_patientRole_id] for v2 in fhirpath_utils.get(v1,'root')], '==', ['1.2.40.0.10.1.4.3.1']))):
            assigner = fhir4.Reference()
            if fhir_patient_identifier.assigner is not None:
                assigner = fhir_patient_identifier.assigner
            else:
                fhir_patient_identifier.assigner = assigner
            assigner.display = string(value='Dachverband der österreichischen Sozialversicherungsträger')
            identifier_type = fhir4.CodeableConcept()
            if fhir_patient_identifier.type_ is not None:
                identifier_type = fhir_patient_identifier.type_
            else:
                fhir_patient_identifier.type_ = identifier_type
            type_coding = fhir4.Coding()
            identifier_type.coding.append(type_coding)
            type_coding.system = uri(value='http://terminology.hl7.org/CodeSystem/v2-0203')
            type_coding.code = string(value='SS')
            type_coding.display = string(value='Social Security Number')
        if utils.single((fhirpath_utils.equals([v2 for v1 in [cda_patientRole_id] for v2 in fhirpath_utils.get(v1,'root')], '==', ['1.2.40.0.10.2.1.1.149']))):
            assigner = fhir4.Reference()
            if fhir_patient_identifier.assigner is not None:
                assigner = fhir_patient_identifier.assigner
            else:
                fhir_patient_identifier.assigner = assigner
            assigner.display = string(value='Bundesministerium für Inneres')
            identifier_type = fhir4.CodeableConcept()
            if fhir_patient_identifier.type_ is not None:
                identifier_type = fhir_patient_identifier.type_
            else:
                fhir_patient_identifier.type_ = identifier_type
            type_coding = fhir4.Coding()
            identifier_type.coding.append(type_coding)
            type_coding.system = uri(value='http://terminology.hl7.org/CodeSystem/v2-0203')
            type_coding.code = string(value='NI')
            type_coding.display = string(value='National unique individual identifier')
    for addr in cda_patientRole.addr or []:
        fhir_patient.address.append(fhir4.Address())
        CdaAdressCompilationToFhirAustrianAddress(addr, fhir_patient.address[-1])
    for telecom in cda_patientRole.telecom or []:
        fhir_patient.telecom.append(fhir4.ContactPoint())
        TELContactPoint(telecom, fhir_patient.telecom[-1])
    cda_patient = cda_patientRole.patient
    if cda_patient:
        for name in cda_patient.name or []:
            fhir_patient.name.append(fhir4.HumanName())
            transform_default(name, fhir_patient.name[-1])
        cda_patient_gender = cda_patient.administrativeGenderCode
        if cda_patient_gender:
            cda_patient_gender_code = cda_patient_gender.code
            if cda_patient_gender_code:
                trans_out = translate(url='ELGAAdministrativeGenderFHIRGender', code=(cda_patient_gender_code if isinstance(cda_patient_gender_code, str) else cda_patient_gender_code.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                fhir_patient.gender = string(value=match)
        if cda_patient.birthTime:
            fhir_patient.birthDate = fhir4.date()
            TSDate(cda_patient.birthTime, fhir_patient.birthDate)
        cda_patient_birthTime = cda_patient.birthTime
        if cda_patient_birthTime:
            if utils.single((fhirpath_utils.compare([v6 for v5 in [v4 for v3 in [v2 for v1 in [cda_patient] for v2 in fhirpath_utils.get(v1,'birthTime')] for v4 in fhirpath_utils.get(v3,'value')] for v6 in fhirpath_utils.strlength(v5)], '>', [10]))):
                fhir_patient_birthDate = fhir4.date()
                if fhir_patient.birthDate is not None:
                    fhir_patient_birthDate = fhir_patient.birthDate
                else:
                    fhir_patient.birthDate = fhir_patient_birthDate
                extension = fhir4.Extension()
                fhir_patient_birthDate.extension.append(extension)
                extension.url = 'http://hl7.org/fhir/StructureDefinition/patient-birthTime'
                fhir_patient_birthTime_dateTime = fhir4.dateTime()
                extension.valueDateTime = fhir_patient_birthTime_dateTime
                TSDateTime(cda_patient_birthTime, fhir_patient_birthTime_dateTime)
        cda_patient_deceasedInd = cda_patient.deceasedInd
        if cda_patient_deceasedInd:
            if utils.single([not([v2 for v1 in [cda_patient] for v2 in fhirpath_utils.get(v1,'deceasedTime')])]):
                fhir_patient_deceased = fhir4.boolean()
                fhir_patient.deceasedBoolean = fhir_patient_deceased
                BL(cda_patient_deceasedInd, fhir_patient_deceased)
        cda_patient_deceasedTime = cda_patient.deceasedTime
        if cda_patient_deceasedTime:
            fhir_patient_deceased = fhir4.dateTime()
            fhir_patient.deceasedDateTime = fhir_patient_deceased
            TSDateTime(cda_patient_deceasedTime, fhir_patient_deceased)
        if cda_patient.maritalStatusCode:
            fhir_patient.maritalStatus = fhir4.CodeableConcept()
            transform_default(cda_patient.maritalStatusCode, fhir_patient.maritalStatus)
        cda_patient_religiousAffiliationCode = cda_patient.religiousAffiliationCode
        if cda_patient_religiousAffiliationCode:
            religion_extension = fhir4.Extension()
            fhir_patient.extension.append(religion_extension)
            religion_extension.url = 'http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-patient-religion'
            religion_extension_code = fhir4.Extension()
            religion_extension.extension.append(religion_extension_code)
            religion_extension_code.url = 'code'
            religion_extension_codeableConcept = fhir4.CodeableConcept()
            religion_extension_code.valueCodeableConcept = religion_extension_codeableConcept
            CECodeableConcept(cda_patient_religiousAffiliationCode, religion_extension_codeableConcept)
        for cda_patient_guardian in cda_patient.guardian or []:
            fhir_patient_contact = fhir4.Patient_Contact()
            fhir_patient.contact.append(fhir_patient_contact)
            for addr_ in cda_patient_guardian.addr or []:
                fhir_patient_contact.address = fhir4.Address()
                CdaAdressCompilationToFhirAustrianAddress(addr_, fhir_patient_contact.address)
            for telecom_ in cda_patient_guardian.telecom or []:
                fhir_patient_contact.telecom.append(fhir4.ContactPoint())
                TELContactPoint(telecom_, fhir_patient_contact.telecom[-1])
            cda_guardian_person = cda_patient_guardian.guardianPerson
            if cda_guardian_person:
                for name_ in cda_guardian_person.name or []:
                    fhir_patient_contact.name = fhir4.HumanName()
                    transform_default(name_, fhir_patient_contact.name)
            cda_guardian_organization = cda_patient_guardian.guardianOrganization
            if cda_guardian_organization:
                for cda_organization_name in cda_guardian_organization.name or []:
                    fhir_bundle_entry = fhir4.Bundle_Entry()
                    fhir_bundle.entry.append(fhir_bundle_entry)
                    fhir_contact_organization = fhir4.Organization()
                    fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_contact_organization)
                    fhir_contact_organization_id = string(value=dummy_uuid())
                    fhir_contact_organization.id = fhir_contact_organization_id
                    fhir_contact_organization.name = string(value=utils.single([v2 for v1 in [cda_organization_name] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
                    fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_contact_organization_id is None else fhir_contact_organization_id if isinstance(fhir_contact_organization_id, str) else fhir_contact_organization_id.value)))
                    fhir_contact_organization_reference = fhir4.Reference()
                    fhir_patient_contact.organization = fhir_contact_organization_reference
                    if not fhir_contact_organization.id:
                        fhir_contact_organization.id = string(value=dummy_uuid())
                    fhir_contact_organization_reference.reference = string(value=type(fhir_contact_organization).__name__ + '/' + fhir_contact_organization.id.value)
        cda_patient_birthplace = cda_patient.birthplace
        if cda_patient_birthplace:
            cda_patient_place = cda_patient_birthplace.place
            if cda_patient_place:
                cda_patient_birthaddr = cda_patient_place.addr
                if cda_patient_birthaddr:
                    birthplace_extension = fhir4.Extension()
                    fhir_patient.extension.append(birthplace_extension)
                    birthplace_extension.url = 'http://hl7.org/fhir/StructureDefinition/patient-birthPlace'
                    birthplace_extension_addr = fhir4.Address()
                    birthplace_extension.valueAddress = birthplace_extension_addr
                    CdaAdressCompilationToFhirAustrianAddress(cda_patient_birthaddr, birthplace_extension_addr)
        for cda_patient_language in cda_patient.languageCommunication or []:
            fhir_patient_communication = fhir4.Patient_Communication()
            fhir_patient.communication.append(fhir_patient_communication)
            if cda_patient_language.languageCode:
                fhir_patient_communication.language = fhir4.CodeableConcept()
                transform_default(cda_patient_language.languageCode, fhir_patient_communication.language)
            if cda_patient_language.preferenceInd:
                fhir_patient_communication.preferred = fhir4.boolean()
                BL(cda_patient_language.preferenceInd, fhir_patient_communication.preferred)
            if utils.single(fhirpath_utils.bool_or([bool([v2 for v1 in [cda_patient_language] for v2 in fhirpath_utils.get(v1,'modeCode')])], [bool([v4 for v3 in [cda_patient_language] for v4 in fhirpath_utils.get(v3,'proficiencyLevelCode')])])):
                communication_extension = fhir4.Extension()
                fhir_patient_communication.extension.append(communication_extension)
                communication_extension.url = 'http://hl7.org/fhir/StructureDefinition/patient-proficiency'
                cda_patient_language_modeCode = cda_patient_language.modeCode
                if cda_patient_language_modeCode:
                    communication_extension_type = fhir4.Extension()
                    communication_extension.extension.append(communication_extension_type)
                    communication_extension_type.url = 'type'
                    communication_extension_type_coding = fhir4.Coding()
                    communication_extension_type.valueCoding = communication_extension_type_coding
                    CECoding(cda_patient_language_modeCode, communication_extension_type_coding)
                cda_patient_language_proficiencyLevelCode = cda_patient_language.proficiencyLevelCode
                if cda_patient_language_proficiencyLevelCode:
                    communication_extension_level = fhir4.Extension()
                    communication_extension.extension.append(communication_extension_level)
                    communication_extension_level.url = 'level'
                    communication_extension_level_coding = fhir4.Coding()
                    communication_extension_level.valueCoding = communication_extension_level_coding
                    CECoding(cda_patient_language_proficiencyLevelCode, communication_extension_level_coding)

def CdaAuthorToFhirPractitionerRole(cda_author, fhir_practitionerRole, fhir_bundle):
    fhir_bundle_entry = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry)
    fhir_practitioner = fhir4.Practitioner()
    fhir_bundle_entry.resource = fhir4.ResourceContainer(Practitioner=fhir_practitioner)
    fhir_practitioner_id = string(value=dummy_uuid())
    fhir_practitioner.id = fhir_practitioner_id
    fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitioner_id is None else fhir_practitioner_id if isinstance(fhir_practitioner_id, str) else fhir_practitioner_id.value)))
    fhir_practitionerRole_practitioner_reference = fhir4.Reference()
    fhir_practitionerRole.practitioner = fhir_practitionerRole_practitioner_reference
    if not fhir_practitioner.id:
        fhir_practitioner.id = string(value=dummy_uuid())
    fhir_practitionerRole_practitioner_reference.reference = string(value=type(fhir_practitioner).__name__ + '/' + fhir_practitioner.id.value)
    fhir_practitionerRole_practitioner_reference.type_ = uri(value='Practitioner')
    if cda_author.functionCode:
        fhir_practitionerRole.code.append(fhir4.CodeableConcept())
        transform_default(cda_author.functionCode, fhir_practitionerRole.code[-1])
    cda_author_assignedAuthor = cda_author.assignedAuthor
    if cda_author_assignedAuthor:
        for id_ in cda_author_assignedAuthor.id or []:
            fhir_practitioner.identifier.append(fhir4.Identifier())
            II(id_, fhir_practitioner.identifier[-1])
        cda_author_assignedAuthor_code = cda_author_assignedAuthor.code
        if cda_author_assignedAuthor_code:
            fhir_practitioner_qualification = fhir4.Practitioner_Qualification()
            fhir_practitioner.qualification.append(fhir_practitioner_qualification)
            fhir_practitioner_qualification_code = fhir4.CodeableConcept()
            if fhir_practitioner_qualification.code is not None:
                fhir_practitioner_qualification_code = fhir_practitioner_qualification.code
            else:
                fhir_practitioner_qualification.code = fhir_practitioner_qualification_code
            CECodeableConcept(cda_author_assignedAuthor_code, fhir_practitioner_qualification_code)
        for telecom in cda_author_assignedAuthor.telecom or []:
            fhir_practitioner.telecom.append(fhir4.ContactPoint())
            TELContactPoint(telecom, fhir_practitioner.telecom[-1])
        cda_assignedPerson = cda_author_assignedAuthor.assignedPerson
        if cda_assignedPerson:
            for name in cda_assignedPerson.name or []:
                fhir_practitioner.name.append(fhir4.HumanName())
                transform_default(name, fhir_practitioner.name[-1])
        cda_representedOrganization = cda_author_assignedAuthor.representedOrganization
        if cda_representedOrganization:
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_organization = fhir4.Organization()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_organization)
            fhir_organization_id = string(value=dummy_uuid())
            fhir_organization.id = fhir_organization_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_organization_id is None else fhir_organization_id if isinstance(fhir_organization_id, str) else fhir_organization_id.value)))
            fhir_practitionerRole_organization = fhir4.Reference()
            fhir_practitionerRole.organization = fhir_practitionerRole_organization
            if not fhir_organization.id:
                fhir_organization.id = string(value=dummy_uuid())
            fhir_practitionerRole_organization.reference = string(value=type(fhir_organization).__name__ + '/' + fhir_organization.id.value)
            fhir_practitionerRole_organization.type_ = uri(value='Organization')
            CdaOrganizationCompilationToFhirOrganization(cda_representedOrganization, fhir_organization)

def CdaAuthorToFhirDevice(cda_author, fhir_device, fhir_bundle):
    if cda_author.functionCode:
        fhir_device.type_ = fhir4.CodeableConcept()
        transform_default(cda_author.functionCode, fhir_device.type_)
    cda_author_assignedAuthor = cda_author.assignedAuthor
    if cda_author_assignedAuthor:
        for id_ in cda_author_assignedAuthor.id or []:
            fhir_device.identifier.append(fhir4.Identifier())
            II(id_, fhir_device.identifier[-1])
        for telecom in cda_author_assignedAuthor.telecom or []:
            fhir_device.contact.append(fhir4.ContactPoint())
            TELContactPoint(telecom, fhir_device.contact[-1])
        cda_assignedAuthoringDevice = cda_author_assignedAuthor.assignedAuthoringDevice
        if cda_assignedAuthoringDevice:
            cda_manufacturerModelName = cda_assignedAuthoringDevice.manufacturerModelName
            if cda_manufacturerModelName:
                fhir_device_deviceName = fhir4.Device_DeviceName()
                fhir_device.deviceName.append(fhir_device_deviceName)
                fhir_device_deviceName.name = string(value=utils.single([v2 for v1 in [cda_manufacturerModelName] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
                fhir_device_deviceName.type_ = string(value='model-name')
            cda_softwareName = cda_assignedAuthoringDevice.softwareName
            if cda_softwareName:
                fhir_device_deviceName = fhir4.Device_DeviceName()
                fhir_device.deviceName.append(fhir_device_deviceName)
                fhir_device_deviceName.name = string(value=utils.single([v2 for v1 in [cda_softwareName] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
                fhir_device_deviceName.type_ = string(value='other')
        cda_representedOrganization = cda_author_assignedAuthor.representedOrganization
        if cda_representedOrganization:
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_organization = fhir4.Organization()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_organization)
            fhir_organization_id = string(value=dummy_uuid())
            fhir_organization.id = fhir_organization_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_organization_id is None else fhir_organization_id if isinstance(fhir_organization_id, str) else fhir_organization_id.value)))
            fhir_device_owner = fhir4.Reference()
            fhir_device.owner = fhir_device_owner
            if not fhir_organization.id:
                fhir_organization.id = string(value=dummy_uuid())
            fhir_device_owner.reference = string(value=type(fhir_organization).__name__ + '/' + fhir_organization.id.value)
            fhir_device_owner.type_ = uri(value='Organization')
            CdaOrganizationCompilationToFhirOrganization(cda_representedOrganization, fhir_organization)

def CdaEncompassingEncounterToFhirEncounter(cda_encompassingEncounter, fhir_encounter, fhir_bundle):
    for id_ in cda_encompassingEncounter.id or []:
        fhir_encounter.identifier.append(fhir4.Identifier())
        II(id_, fhir_encounter.identifier[-1])
    if cda_encompassingEncounter.code:
        fhir_encounter.class_ = fhir4.Coding()
        transform_default(cda_encompassingEncounter.code, fhir_encounter.class_)
    if cda_encompassingEncounter.effectiveTime:
        fhir_encounter.period = fhir4.Period()
        IVLTSPeriod(cda_encompassingEncounter.effectiveTime, fhir_encounter.period)
    cda_responsibleParty = cda_encompassingEncounter.responsibleParty
    if cda_responsibleParty:
        cda_assignedEntity = cda_responsibleParty.assignedEntity
        if cda_assignedEntity:
            fhir_encounter_participant = fhir4.Encounter_Participant()
            fhir_encounter.participant.append(fhir_encounter_participant)
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_practitionerRole = fhir4.PractitionerRole()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
            fhir_practitionerRole_id = string(value=dummy_uuid())
            fhir_practitionerRole.id = fhir_practitionerRole_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
            fhir_practitionerRole_reference = fhir4.Reference()
            fhir_encounter_participant.individual = fhir_practitionerRole_reference
            if not fhir_practitionerRole.id:
                fhir_practitionerRole.id = string(value=dummy_uuid())
            fhir_practitionerRole_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
            fhir_practitionerRole_reference.type_ = uri(value='PractitionerRole')
            CdaAssignedEntityToFhirPractitionerRole(cda_assignedEntity, fhir_practitionerRole, fhir_bundle)
    cda_location = cda_encompassingEncounter.location
    if cda_location:
        cda_healthCareFacility = cda_location.healthCareFacility
        if cda_healthCareFacility:
            fhir_encounter_location = fhir4.Encounter_Location()
            fhir_encounter.location.append(fhir_encounter_location)
            fhir_bundle_entry = fhir4.Bundle_Entry()
            fhir_bundle.entry.append(fhir_bundle_entry)
            fhir_location = fhir4.Location()
            fhir_bundle_entry.resource = fhir4.ResourceContainer(Location=fhir_location)
            fhir_location_id = string(value=dummy_uuid())
            fhir_location.id = fhir_location_id
            fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_location_id is None else fhir_location_id if isinstance(fhir_location_id, str) else fhir_location_id.value)))
            fhir_location_reference = fhir4.Reference()
            fhir_encounter_location.location = fhir_location_reference
            if not fhir_location.id:
                fhir_location.id = string(value=dummy_uuid())
            fhir_location_reference.reference = string(value=type(fhir_location).__name__ + '/' + fhir_location.id.value)
            fhir_location_reference.type_ = uri(value='Location')
            if cda_healthCareFacility.code:
                fhir_location.type_.append(fhir4.CodeableConcept())
                transform_default(cda_healthCareFacility.code, fhir_location.type_[-1])
            cda_serviceProviderOrganization = cda_healthCareFacility.serviceProviderOrganization
            if cda_serviceProviderOrganization:
                fhir_bundle_entry = fhir4.Bundle_Entry()
                fhir_bundle.entry.append(fhir_bundle_entry)
                fhir_organization = fhir4.Organization()
                fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_organization)
                fhir_organization_id = string(value=dummy_uuid())
                fhir_organization.id = fhir_organization_id
                fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_organization_id is None else fhir_organization_id if isinstance(fhir_organization_id, str) else fhir_organization_id.value)))
                fhir_location_managingOrganization = fhir4.Reference()
                fhir_location.managingOrganization = fhir_location_managingOrganization
                if not fhir_organization.id:
                    fhir_organization.id = string(value=dummy_uuid())
                fhir_location_managingOrganization.reference = string(value=type(fhir_organization).__name__ + '/' + fhir_organization.id.value)
                fhir_location_managingOrganization.type_ = uri(value='Organization')
                CdaOrganizationCompilationToFhirOrganization(cda_serviceProviderOrganization, fhir_organization)

def CdaBodyToFhirComposition(cda, cda_structuredBody, fhir_composition, fhir_practitionerRole, fhir_patient, fhir_diagnosticReport, fhir_bundle):
    for cda_component in cda_structuredBody.component or []:
        cda_section = cda_component.section
        if cda_section:
            if utils.single(fhirpath_utils.bool_or(fhirpath_utils.bool_or(fhirpath_utils.bool_or(fhirpath_utils.bool_or(fhirpath_utils.bool_or(fhirpath_utils.bool_or([v1 for v1 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v1,'code'), '==', ['BRIEFT'])), (fhirpath_utils.equals(fhirpath_utils.get(v1,'codeSystem'), '==', ['1.2.40.0.34.5.40']))) == [True]], (fhirpath_utils.bool_and([v2 for v2 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v2,'code'), '==', ['46239-0'])), (fhirpath_utils.equals(fhirpath_utils.get(v2,'codeSystem'), '==', ['2.16.840.1.113883.6.1']))) == [True]], (fhirpath_utils.bool_or([v3 for v3 in fhirpath_utils.get(cda_section,'templateId') if fhirpath_utils.equals(fhirpath_utils.get(v3,'root'), '==', ['1.2.40.0.34.6.0.11.2.114']) == [True]], [v4 for v4 in fhirpath_utils.get(cda_section,'templateId') if fhirpath_utils.equals(fhirpath_utils.get(v4,'root'), '==', ['1.2.40.0.34.11.4.2.4']) == [True]]))))), (fhirpath_utils.bool_and([v5 for v5 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v5,'code'), '==', ['10164-2'])), (fhirpath_utils.equals(fhirpath_utils.get(v5,'codeSystem'), '==', ['2.16.840.1.113883.6.1']))) == [True]], [v6 for v6 in fhirpath_utils.get(cda_section,'templateId') if fhirpath_utils.equals(fhirpath_utils.get(v6,'root'), '==', ['1.2.40.0.34.6.0.11.2.111']) == [True]]))), (fhirpath_utils.bool_and([v7 for v7 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v7,'code'), '==', ['400999005'])), (fhirpath_utils.equals(fhirpath_utils.get(v7,'codeSystem'), '==', ['2.16.840.1.113883.6.96']))) == [True]], [v8 for v8 in fhirpath_utils.get(cda_section,'templateId') if fhirpath_utils.equals(fhirpath_utils.get(v8,'root'), '==', ['1.2.40.0.34.6.0.11.2.112']) == [True]]))), [v9 for v9 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v9,'code'), '==', ['20'])), (fhirpath_utils.equals(fhirpath_utils.get(v9,'codeSystem'), '==', ['1.2.40.0.34.5.11']))) == [True]]), [v10 for v10 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v10,'code'), '==', ['BEIL'])), (fhirpath_utils.equals(fhirpath_utils.get(v10,'codeSystem'), '==', ['1.2.40.0.34.5.40']))) == [True]]), (fhirpath_utils.bool_and([v11 for v11 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v11,'code'), '==', ['ABBEM'])), (fhirpath_utils.equals(fhirpath_utils.get(v11,'codeSystem'), '==', ['1.2.40.0.34.5.40']))) == [True]], [v12 for v12 in fhirpath_utils.get(cda_section,'templateId') if fhirpath_utils.equals(fhirpath_utils.get(v12,'root'), '==', ['1.2.40.0.34.6.0.11.2.70']) == [True]])))):
                fhir_section = fhir4.Composition_Section()
                fhir_composition.section.append(fhir_section)
                CdaSectionToFhirSection(cda_section, fhir_section, fhir_bundle)
        cda_section = cda_component.section
        if cda_section:
            if utils.single([v1 for v1 in fhirpath_utils.get(cda_section,'code') if fhirpath_utils.bool_and((fhirpath_utils.equals(fhirpath_utils.get(v1,'code'), '==', ['10'])), (fhirpath_utils.equals(fhirpath_utils.get(v1,'codeSystem'), '==', ['1.2.40.0.34.5.11']))) == [True]]):
                fhir_section = fhir4.Composition_Section()
                fhir_composition.section.append(fhir_section)
                CdaSpecimenSectionToFhirSection(cda_section, fhir_section, fhir_patient, fhir_diagnosticReport, fhir_bundle)
        cda_section = cda_component.section
        if cda_section:
            if utils.single([v1 for v1 in fhirpath_utils.get(cda_section,'templateId') if fhirpath_utils.bool_or((fhirpath_utils.equals(fhirpath_utils.get(v1,'root'), '==', ['1.2.40.0.34.6.0.11.2.102'])), (fhirpath_utils.equals(fhirpath_utils.get(v1,'root'), '==', ['1.3.6.1.4.1.19376.1.3.3.2.1']))) == [True]]):
                fhir_section = fhir4.Composition_Section()
                fhir_composition.section.append(fhir_section)
                CdaLaboratorySpecialtySectionToFhirSection(cda, cda_section, fhir_section, fhir_practitionerRole, fhir_patient, fhir_diagnosticReport, fhir_bundle)

def CdaToPractitionerRole(cda, fhir_practitionerRole, fhir_bundle):
    if utils.single([bool([v6 for v5 in [v4 for v3 in fhirpath_utils.at_index([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'documentationOf')], [0]) for v4 in fhirpath_utils.get(v3,'serviceEvent')] for v6 in fhirpath_utils.get(v5,'performer')])]):
        if len(cda.documentationOf) > 0:
            cda_documentationOf = cda.documentationOf[0]
            cda_documentationOf_serviceEvent = cda_documentationOf.serviceEvent
            if cda_documentationOf_serviceEvent:
                for cda_documentationOf_serviceEvent_performer in cda_documentationOf_serviceEvent.performer or []:
                    cda_performer_assignedEntity = cda_documentationOf_serviceEvent_performer.assignedEntity
                    CdaAssignedEntityToFhirPractitionerRole(cda_performer_assignedEntity, fhir_practitionerRole, fhir_bundle)
    if utils.single(fhirpath_utils.bool_not([bool([v6 for v5 in [v4 for v3 in fhirpath_utils.at_index([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'documentationOf')], [0]) for v4 in fhirpath_utils.get(v3,'serviceEvent')] for v6 in fhirpath_utils.get(v5,'performer')])])):
        for cda_author in cda.author:
            if utils.single([bool([v4 for v3 in [v2 for v1 in [cda_author] for v2 in fhirpath_utils.get(v1,'assignedAuthor')] for v4 in fhirpath_utils.get(v3,'assignedPerson')])]):
                CdaAuthorToFhirPractitionerRole(cda_author, fhir_practitionerRole, fhir_bundle)

def CdaSectionToFhirSection(cda_section, fhir_section, fhir_bundle):
    if cda_section.code:
        fhir_section.code = fhir4.CodeableConcept()
        transform_default(cda_section.code, fhir_section.code)
    cda_section_title = cda_section.title
    if cda_section_title:
        fhir_section.title = string(value=utils.single([v2 for v1 in [cda_section_title] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    cda_section_text = cda_section.text
    if cda_section_text:
        fhir_section_text = fhir4.Narrative()
        if fhir_section.text is not None:
            fhir_section_text = fhir_section.text
        else:
            fhir_section.text = fhir_section_text
        fhir_section_text.status = string(value='generated')
        if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda_section] for v2 in fhirpath_utils.get(v1,'languageCode')])])):
            fhir_section_text.div = utils.strucdoctext2html(fhir4, cda_section_text)
        if utils.single([bool([v2 for v1 in [cda_section] for v2 in fhirpath_utils.get(v1,'languageCode')])]):
            cda_languageCode = cda_section.languageCode
            if cda_languageCode:
                cda_languageCode_code = cda_languageCode.code
                if cda_languageCode_code:
                    fhir_section_text.div = utils.builddiv(fhir4, '<div xml:lang="' + ('' if cda_languageCode_code is None else cda_languageCode_code if isinstance(cda_languageCode_code, str) else cda_languageCode_code.value) + '">' + cda_section_text.valueOf_ + '</div>')
    for cda_section_author in cda_section.author or []:
        fhir_bundle_entry = fhir4.Bundle_Entry()
        fhir_bundle.entry.append(fhir_bundle_entry)
        fhir_practitionerRole = fhir4.PractitionerRole()
        fhir_bundle_entry.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
        fhir_practitionerRole_id = string(value=dummy_uuid())
        fhir_practitionerRole.id = fhir_practitionerRole_id
        fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
        fhir_section_author_reference = fhir4.Reference()
        fhir_section.author.append(fhir_section_author_reference)
        if not fhir_practitionerRole.id:
            fhir_practitionerRole.id = string(value=dummy_uuid())
        fhir_section_author_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
        fhir_section_author_reference.type_ = uri(value='PractitionerRole')
        CdaAuthorToFhirPractitionerRole(cda_section_author, fhir_practitionerRole, fhir_bundle)
    for cda_section_entry in cda_section.entry or []:
        if utils.single([bool([v2 for v1 in [cda_section_entry] for v2 in fhirpath_utils.get(v1,'observationMedia')])]):
            cda_observationMedia = cda_section_entry.observationMedia
            if cda_observationMedia:
                fhir_bundle_entry = fhir4.Bundle_Entry()
                fhir_bundle.entry.append(fhir_bundle_entry)
                fhir_binary = fhir4.Binary()
                fhir_bundle_entry.resource = fhir4.ResourceContainer(Binary=fhir_binary)
                fhir_binary_id = string(value=dummy_uuid())
                fhir_binary.id = fhir_binary_id
                fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_binary_id is None else fhir_binary_id if isinstance(fhir_binary_id, str) else fhir_binary_id.value)))
                fhir_section_entry_reference = fhir4.Reference()
                fhir_section.entry.append(fhir_section_entry_reference)
                if not fhir_binary.id:
                    fhir_binary.id = string(value=dummy_uuid())
                fhir_section_entry_reference.reference = string(value=type(fhir_binary).__name__ + '/' + fhir_binary.id.value)
                fhir_section_entry_reference.type_ = uri(value='Binary')
                cda_observationMedia_value = cda_observationMedia.value
                if cda_observationMedia_value:
                    fhir_binary.data = base64Binary(value=utils.single([v2 for v1 in [cda_observationMedia_value] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
                    cda_mediaType = cda_observationMedia_value.mediaType
                    if cda_mediaType:
                        fhir_binary.contentType = string(value=cda_mediaType)
    for cda_entry in cda_section.entry or []:
        if utils.single([v5 for v5 in [v4 for v3 in [v2 for v1 in [cda_entry] for v2 in fhirpath_utils.get(v1,'act')] for v4 in fhirpath_utils.get(v3,'templateId')] if fhirpath_utils.equals(fhirpath_utils.get(v5,'root'), '==', ['2.16.840.1.113883.10.20.1.40']) == [True]]):
            cda_act = cda_entry.act
            if cda_act:
                cda_act_text = cda_act.text
                if cda_act_text:
                    fhir_section_section = fhir4.Composition_Section()
                    fhir_section.section.append(fhir_section_section)
                    fhir_section_section_text = fhir4.Narrative()
                    if fhir_section_section.text is not None:
                        fhir_section_section_text = fhir_section_section.text
                    else:
                        fhir_section_section.text = fhir_section_section_text
                    fhir_section_section_text.status = string(value='generated')
                    fhir_section_section_text.div = utils.ed2html(fhir4, cda_act_text)
    for cda_section_component in cda_section.component or []:
        cda_section_component_section = cda_section_component.section
        if cda_section_component_section:
            if utils.single([v1 for v1 in fhirpath_utils.get(cda_section_component_section,'templateId') if fhirpath_utils.equals(fhirpath_utils.get(v1,'root'), '==', ['1.2.40.0.34.6.0.11.2.8']) == [True]]):
                fhir_section_section = fhir4.Composition_Section()
                fhir_section.section.append(fhir_section_section)
                CdaSectionToFhirSection(cda_section_component_section, fhir_section_section, fhir_bundle)

def CdaSpecimenSectionToFhirSection(cda_section, fhir_section, fhir_patient, fhir_diagnosticReport, fhir_bundle):
    CdaSectionToFhirSection(cda_section, fhir_section, fhir_bundle)
    for cda_section_entry in cda_section.entry or []:
        cda_act = cda_section_entry.act
        if cda_act:
            if utils.single([v3 for v3 in [v2 for v1 in [cda_act] for v2 in fhirpath_utils.get(v1,'code')] if fhirpath_utils.equals(fhirpath_utils.get(v3,'code'), '==', ['10']) == [True]]):
                for cda_entryRelationship in cda_act.entryRelationship or []:
                    cda_procedure = cda_entryRelationship.procedure
                    if cda_procedure:
                        fhir_bundle_entry = fhir4.Bundle_Entry()
                        fhir_bundle.entry.append(fhir_bundle_entry)
                        fhir_specimen = fhir4.Specimen()
                        fhir_bundle_entry.resource = fhir4.ResourceContainer(Specimen=fhir_specimen)
                        fhir_specimen_id = string(value=dummy_uuid())
                        fhir_specimen.id = fhir_specimen_id
                        fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_specimen_id is None else fhir_specimen_id if isinstance(fhir_specimen_id, str) else fhir_specimen_id.value)))
                        fhir_specimen_patient_reference = fhir4.Reference()
                        fhir_specimen.subject = fhir_specimen_patient_reference
                        if not fhir_patient.id:
                            fhir_patient.id = string(value=dummy_uuid())
                        fhir_specimen_patient_reference.reference = string(value=type(fhir_patient).__name__ + '/' + fhir_patient.id.value)
                        fhir_specimen_patient_reference.type_ = uri(value='Patient')
                        fhir_diagnosticReport_specimen_reference = fhir4.Reference()
                        fhir_diagnosticReport.specimen.append(fhir_diagnosticReport_specimen_reference)
                        if not fhir_specimen.id:
                            fhir_specimen.id = string(value=dummy_uuid())
                        fhir_diagnosticReport_specimen_reference.reference = string(value=type(fhir_specimen).__name__ + '/' + fhir_specimen.id.value)
                        fhir_diagnosticReport_specimen_reference.type_ = uri(value='Specimen')
                        fhir_section_specimen_reference = fhir4.Reference()
                        fhir_section.entry.append(fhir_section_specimen_reference)
                        if not fhir_specimen.id:
                            fhir_specimen.id = string(value=dummy_uuid())
                        fhir_section_specimen_reference.reference = string(value=type(fhir_specimen).__name__ + '/' + fhir_specimen.id.value)
                        fhir_section_specimen_reference.type_ = uri(value='Specimen')
                        fhir_specimen_collection = fhir4.Specimen_Collection()
                        if fhir_specimen.collection is not None:
                            fhir_specimen_collection = fhir_specimen.collection
                        else:
                            fhir_specimen.collection = fhir_specimen_collection
                        cda_effectiveTime = cda_procedure.effectiveTime
                        if cda_effectiveTime:
                            if utils.single([bool([v2 for v1 in [cda_effectiveTime] for v2 in fhirpath_utils.get(v1,'value')])]):
                                v = cda_effectiveTime.value
                                if v:
                                    fhir_specimen_collection.collectedDateTime = dateTime(value=dateutil.parser.parse(v).isoformat())
                        for targetSiteCode in cda_procedure.targetSiteCode or []:
                            fhir_specimen_collection.bodySite = fhir4.CodeableConcept()
                            CDCodeableConcept(targetSiteCode, fhir_specimen_collection.bodySite)
                        for cda_participant in cda_procedure.participant or []:
                            cda_participantRole = cda_participant.participantRole
                            if cda_participantRole:
                                cda_playingEntity = cda_participantRole.playingEntity
                                if cda_playingEntity:
                                    for id_ in cda_participantRole.id or []:
                                        fhir_specimen.accessionIdentifier = fhir4.Identifier()
                                        II(id_, fhir_specimen.accessionIdentifier)
                                    if cda_playingEntity.code:
                                        fhir_specimen.type_ = fhir4.CodeableConcept()
                                        transform_default(cda_playingEntity.code, fhir_specimen.type_)
                        for cda_entryRelationship in cda_procedure.entryRelationship or []:
                            cda_act = cda_entryRelationship.act
                            if cda_act:
                                cda_effectiveTime = cda_act.effectiveTime
                                if cda_effectiveTime:
                                    if utils.single([bool([v2 for v1 in [cda_effectiveTime] for v2 in fhirpath_utils.get(v1,'value')])]):
                                        v = cda_effectiveTime.value
                                        if v:
                                            fhir_specimen.receivedTime = dateTime(value=dateutil.parser.parse(v).isoformat())
                                for cda_comment_entryRelationship in cda_act.entryRelationship or []:
                                    cda_comment_act = cda_comment_entryRelationship.act
                                    if cda_comment_act:
                                        cda_comment_act_text = cda_comment_act.text
                                        if cda_comment_act_text:
                                            fhir_specimen_note = fhir4.Annotation()
                                            fhir_specimen.note.append(fhir_specimen_note)
                                            fhir_specimen_note.text = utils.ed2markdown(fhir4, cda_comment_act_text)

def CdaLaboratorySpecialtySectionToFhirSection(cda, cda_section, fhir_section, fhir_practitionerRole, fhir_patient, fhir_diagnosticReport, fhir_bundle):
    CdaSectionToFhirSection(cda_section, fhir_section, fhir_bundle)
    for cda_section_entry in cda_section.entry or []:
        cda_act = cda_section_entry.act
        if cda_act:
            for cda_entryRelationship in cda_act.entryRelationship or []:
                if utils.single([v5 for v5 in [v4 for v3 in [v2 for v1 in [cda_entryRelationship] for v2 in fhirpath_utils.get(v1,'organizer')] for v4 in fhirpath_utils.get(v3,'templateId')] if fhirpath_utils.equals(fhirpath_utils.get(v5,'root'), '==', ['1.3.6.1.4.1.19376.1.3.1.4']) == [True]]):
                    cda_laboratory_battery_organizer = cda_entryRelationship.organizer
                    if cda_laboratory_battery_organizer:
                        fhir_bundle_entry = fhir4.Bundle_Entry()
                        fhir_bundle.entry.append(fhir_bundle_entry)
                        fhir_observation = fhir4.Observation()
                        fhir_bundle_entry.resource = fhir4.ResourceContainer(Observation=fhir_observation)
                        fhir_observation_id = string(value=dummy_uuid())
                        fhir_observation.id = fhir_observation_id
                        fhir_observation.status = string(value='final')
                        fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_observation_id is None else fhir_observation_id if isinstance(fhir_observation_id, str) else fhir_observation_id.value)))
                        fhir_section_entry_reference = fhir4.Reference()
                        fhir_section.entry.append(fhir_section_entry_reference)
                        if not fhir_observation.id:
                            fhir_observation.id = string(value=dummy_uuid())
                        fhir_section_entry_reference.reference = string(value=type(fhir_observation).__name__ + '/' + fhir_observation.id.value)
                        fhir_section_entry_reference.type_ = uri(value='Observation')
                        fhir_observation_meta = fhir4.Meta()
                        if fhir_observation.meta is not None:
                            fhir_observation_meta = fhir_observation.meta
                        else:
                            fhir_observation.meta = fhir_observation_meta
                        fhir_observation_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/Observation-resultslab-eu-lab'))
                        fhir_category = fhir4.CodeableConcept()
                        fhir_observation.category.append(fhir_category)
                        fhir_category_coding = fhir4.Coding()
                        fhir_category.coding.append(fhir_category_coding)
                        fhir_category_coding.system = uri(value='http://terminology.hl7.org/CodeSystem/observation-category')
                        fhir_category_coding.code = string(value='laboratory')
                        fhir_observation_subject_reference = fhir4.Reference()
                        fhir_observation.subject = fhir_observation_subject_reference
                        if not fhir_patient.id:
                            fhir_patient.id = string(value=dummy_uuid())
                        fhir_observation_subject_reference.reference = string(value=type(fhir_patient).__name__ + '/' + fhir_patient.id.value)
                        if cda_laboratory_battery_organizer.code:
                            fhir_observation.category.append(fhir4.CodeableConcept())
                            CDCodeableConcept(cda_laboratory_battery_organizer.code, fhir_observation.category[-1])
                        if cda_laboratory_battery_organizer.code:
                            fhir_observation.code = fhir4.CodeableConcept()
                            CDCodeableConcept(cda_laboratory_battery_organizer.code, fhir_observation.code)
                        organizer_statusCode = cda_laboratory_battery_organizer.statusCode
                        if organizer_statusCode:
                            cda_code = organizer_statusCode.code
                            if cda_code:
                                trans_out = translate(url='act-status-2-observation-status', code=(cda_code if isinstance(cda_code, str) else cda_code.value), silent=False)
                                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                                match = matches[0]['code']
                                fhir_observation.status = string(value=match)
                        cda_effectiveTime = cda_laboratory_battery_organizer.effectiveTime
                        if cda_effectiveTime:
                            fhir_observation_effective = fhir4.dateTime()
                            fhir_observation.effectiveDateTime = fhir_observation_effective
                            fhir_observation_effective_extenstion = fhir4.Extension()
                            fhir_observation_effective.extension.append(fhir_observation_effective_extenstion)
                            fhir_observation_effective_extenstion.url = 'http://hl7.org/fhir/StructureDefinition/data-absent-reason'
                            fhir_observation_effective_extenstion_code = fhir4.code()
                            fhir_observation_effective_extenstion.valueCode = fhir_observation_effective_extenstion_code
                            fhir_observation_effective_extenstion_code.value = 'not-applicable'
                            TSDateTime(cda_effectiveTime, fhir_observation_effective)
                        if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda_laboratory_battery_organizer] for v2 in fhirpath_utils.get(v1,'effectiveTime')])])):
                            fhir_observation_effective = fhir4.dateTime()
                            fhir_observation.effectiveDateTime = fhir_observation_effective
                            fhir_observation_effective.value = '1900-01-01'
                            fhir_observation_effective_extenstion = fhir4.Extension()
                            fhir_observation_effective.extension.append(fhir_observation_effective_extenstion)
                            fhir_observation_effective_extenstion.url = 'http://hl7.org/fhir/StructureDefinition/data-absent-reason'
                            fhir_observation_effective_extenstion_code = fhir4.code()
                            fhir_observation_effective_extenstion.valueCode = fhir_observation_effective_extenstion_code
                            fhir_observation_effective_extenstion_code.value = 'not-applicable'
                        for cda_laboratory_battery_organizer_performer in cda_laboratory_battery_organizer.performer:
                            if utils.single([bool([v2 for v1 in [cda_laboratory_battery_organizer] for v2 in fhirpath_utils.get(v1,'performer')])]):
                                CdaPerformerToFhirObservationPerformer(cda_laboratory_battery_organizer_performer, fhir_observation, fhir_bundle)
                        if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda_laboratory_battery_organizer] for v2 in fhirpath_utils.get(v1,'performer')])])):
                            if utils.single([bool([v6 for v5 in [v4 for v3 in fhirpath_utils.at_index([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'documentationOf')], [0]) for v4 in fhirpath_utils.get(v3,'serviceEvent')] for v6 in fhirpath_utils.get(v5,'performer')])]):
                                if len(cda.documentationOf) > 0:
                                    cda_documentationOf = cda.documentationOf[0]
                                    cda_documentationOf_serviceEvent = cda_documentationOf.serviceEvent
                                    if cda_documentationOf_serviceEvent:
                                        for cda_documentationOf_serviceEvent_performer in cda_documentationOf_serviceEvent.performer or []:
                                            if cda_documentationOf_serviceEvent_performer.time:
                                                fhir_observation.issued = fhir4.instant()
                                                transform_default(cda_documentationOf_serviceEvent_performer.time, fhir_observation.issued)
                            fhir_observation_performer_reference = fhir4.Reference()
                            fhir_observation.performer.append(fhir_observation_performer_reference)
                            if not fhir_practitionerRole.id:
                                fhir_practitionerRole.id = string(value=dummy_uuid())
                            fhir_observation_performer_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
                            fhir_observation_performer_reference.type_ = uri(value='PractitionerRole')
                        for cda_component in cda_laboratory_battery_organizer.component or []:
                            if utils.single([v5 for v5 in [v4 for v3 in [v2 for v1 in [cda_component] for v2 in fhirpath_utils.get(v1,'observation')] for v4 in fhirpath_utils.get(v3,'templateId')] if fhirpath_utils.equals(fhirpath_utils.get(v5,'root'), '==', ['1.3.6.1.4.1.19376.1.3.1.6']) == [True]]):
                                cda_laboratory_observation = cda_component.observation
                                if cda_laboratory_observation:
                                    fhir_bundle_entry = fhir4.Bundle_Entry()
                                    fhir_bundle.entry.append(fhir_bundle_entry)
                                    fhir_laboratory_observation = fhir4.Observation()
                                    fhir_bundle_entry.resource = fhir4.ResourceContainer(Observation=fhir_laboratory_observation)
                                    fhir_laboratory_observation_id = string(value=dummy_uuid())
                                    fhir_laboratory_observation.id = fhir_laboratory_observation_id
                                    fhir_laboratory_observation.status = string(value='final')
                                    fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_laboratory_observation_id is None else fhir_laboratory_observation_id if isinstance(fhir_laboratory_observation_id, str) else fhir_laboratory_observation_id.value)))
                                    fhir_observation_hasMember_reference = fhir4.Reference()
                                    fhir_observation.hasMember.append(fhir_observation_hasMember_reference)
                                    if not fhir_laboratory_observation.id:
                                        fhir_laboratory_observation.id = string(value=dummy_uuid())
                                    fhir_observation_hasMember_reference.reference = string(value=type(fhir_laboratory_observation).__name__ + '/' + fhir_laboratory_observation.id.value)
                                    fhir_observation_hasMember_reference.type_ = uri(value='Observation')
                                    CdaLaboratoryObservationToFhirObservation(cda, cda_laboratory_observation, fhir_laboratory_observation, fhir_practitionerRole, fhir_patient, fhir_bundle)

def CdaObservationToFhirObservation(cda_observation, fhir_observation):
    for id_ in cda_observation.id or []:
        fhir_observation.identifier.append(fhir4.Identifier())
        II(id_, fhir_observation.identifier[-1])

def CdaLaboratoryObservationToFhirObservation(cda, cda_laboratory_observation, fhir_observation, fhir_practitionerRole, fhir_patient, fhir_bundle):
    fhir_observation_meta = fhir4.Meta()
    if fhir_observation.meta is not None:
        fhir_observation_meta = fhir_observation.meta
    else:
        fhir_observation.meta = fhir_observation_meta
    fhir_observation_meta.profile.append(string(value='http://hl7.eu/fhir/laboratory/StructureDefinition/Observation-resultslab-eu-lab'))
    fhir_observation.status = string(value='final')
    for id_ in cda_laboratory_observation.id or []:
        fhir_observation.identifier.append(fhir4.Identifier())
        II(id_, fhir_observation.identifier[-1])
    fhir_category = fhir4.CodeableConcept()
    fhir_observation.category.append(fhir_category)
    fhir_category_coding = fhir4.Coding()
    fhir_category.coding.append(fhir_category_coding)
    fhir_category_coding.system = uri(value='http://terminology.hl7.org/CodeSystem/observation-category')
    fhir_category_coding.code = string(value='laboratory')
    fhir_observation_subject_reference = fhir4.Reference()
    fhir_observation.subject = fhir_observation_subject_reference
    if not fhir_patient.id:
        fhir_patient.id = string(value=dummy_uuid())
    fhir_observation_subject_reference.reference = string(value=type(fhir_patient).__name__ + '/' + fhir_patient.id.value)
    if cda_laboratory_observation.code:
        fhir_observation.code = fhir4.CodeableConcept()
        CDCodeableConcept(cda_laboratory_observation.code, fhir_observation.code)
    observation_statusCode = cda_laboratory_observation.statusCode
    if observation_statusCode:
        cda_code = observation_statusCode.code
        if cda_code:
            trans_out = translate(url='act-status-2-observation-status', code=(cda_code if isinstance(cda_code, str) else cda_code.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            fhir_observation.status = string(value=match)
    cda_effectiveTime = cda_laboratory_observation.effectiveTime
    if cda_effectiveTime:
        fhir_observation_effective = fhir4.dateTime()
        fhir_observation.effectiveDateTime = fhir_observation_effective
        TSDateTime(cda_effectiveTime, fhir_observation_effective)
    for cda_observation_value in cda_laboratory_observation.value or []:
        if isinstance(cda_observation_value, cda_austrian_extension.PQ):
            fhir_observation_value = fhir4.Quantity()
            fhir_observation.valueQuantity = fhir_observation_value
            PQQuantity(cda_observation_value, fhir_observation_value)
    for interpretationCode in cda_laboratory_observation.interpretationCode or []:
        fhir_observation.interpretation.append(fhir4.CodeableConcept())
        transform_default(interpretationCode, fhir_observation.interpretation[-1])
    for cda_laboratory_observation_performer in cda_laboratory_observation.performer:
        if utils.single([bool([v2 for v1 in [cda_laboratory_observation] for v2 in fhirpath_utils.get(v1,'performer')])]):
            CdaPerformerToFhirObservationPerformer(cda_laboratory_observation_performer, fhir_observation, fhir_bundle)
    if utils.single(fhirpath_utils.bool_not([bool([v2 for v1 in [cda_laboratory_observation] for v2 in fhirpath_utils.get(v1,'performer')])])):
        if utils.single([bool([v6 for v5 in [v4 for v3 in fhirpath_utils.at_index([v2 for v1 in [cda] for v2 in fhirpath_utils.get(v1,'documentationOf')], [0]) for v4 in fhirpath_utils.get(v3,'serviceEvent')] for v6 in fhirpath_utils.get(v5,'performer')])]):
            if len(cda.documentationOf) > 0:
                cda_documentationOf = cda.documentationOf[0]
                cda_documentationOf_serviceEvent = cda_documentationOf.serviceEvent
                if cda_documentationOf_serviceEvent:
                    for cda_documentationOf_serviceEvent_performer in cda_documentationOf_serviceEvent.performer or []:
                        if cda_documentationOf_serviceEvent_performer.time:
                            fhir_observation.issued = fhir4.instant()
                            transform_default(cda_documentationOf_serviceEvent_performer.time, fhir_observation.issued)
        fhir_observation_performer_reference = fhir4.Reference()
        fhir_observation.performer.append(fhir_observation_performer_reference)
        if not fhir_practitionerRole.id:
            fhir_practitionerRole.id = string(value=dummy_uuid())
        fhir_observation_performer_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
        fhir_observation_performer_reference.type_ = uri(value='PractitionerRole')
    for cda_observation_referenceRange in cda_laboratory_observation.referenceRange or []:
        cda_referenceRange_observationRange = cda_observation_referenceRange.observationRange
        if cda_referenceRange_observationRange:
            fhir_observation_referenceRange = fhir4.Observation_ReferenceRange()
            fhir_observation.referenceRange.append(fhir_observation_referenceRange)
            if cda_referenceRange_observationRange.text:
                fhir_observation_referenceRange.text = fhir4.string()
                EDstring(cda_referenceRange_observationRange.text, fhir_observation_referenceRange.text)
            cda_observationRange_value = cda_referenceRange_observationRange.value
            if cda_observationRange_value:
                for low in (cda_observationRange_value.low if isinstance(cda_observationRange_value.low, list) else ([] if not cda_observationRange_value.low else [cda_observationRange_value.low])):
                    fhir_observation_referenceRange.low = fhir4.Quantity()
                    transform_default(low, fhir_observation_referenceRange.low)
                for high in (cda_observationRange_value.high if isinstance(cda_observationRange_value.high, list) else ([] if not cda_observationRange_value.high else [cda_observationRange_value.high])):
                    fhir_observation_referenceRange.high = fhir4.Quantity()
                    transform_default(high, fhir_observation_referenceRange.high)
            fhir_referenceRange_type = fhir4.CodeableConcept()
            if fhir_observation_referenceRange.type_ is not None:
                fhir_referenceRange_type = fhir_observation_referenceRange.type_
            else:
                fhir_observation_referenceRange.type_ = fhir_referenceRange_type
            fhir_type_coding = fhir4.Coding()
            fhir_referenceRange_type.coding.append(fhir_type_coding)
            fhir_type_coding.system = uri(value='http://terminology.hl7.org/CodeSystem/referencerange-meaning')
            fhir_type_coding.code = string(value='normal')

def CdaAssignedEntityToFhirPractitionerRole(cda_assignedEntity, fhir_practitionerRole, fhir_bundle):
    fhir_bundle_entry = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry)
    fhir_practitioner = fhir4.Practitioner()
    fhir_bundle_entry.resource = fhir4.ResourceContainer(Practitioner=fhir_practitioner)
    fhir_practitioner_id = string(value=dummy_uuid())
    fhir_practitioner.id = fhir_practitioner_id
    fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitioner_id is None else fhir_practitioner_id if isinstance(fhir_practitioner_id, str) else fhir_practitioner_id.value)))
    fhir_practitionerRole_practitioner_reference = fhir4.Reference()
    fhir_practitionerRole.practitioner = fhir_practitionerRole_practitioner_reference
    if not fhir_practitioner.id:
        fhir_practitioner.id = string(value=dummy_uuid())
    fhir_practitionerRole_practitioner_reference.reference = string(value=type(fhir_practitioner).__name__ + '/' + fhir_practitioner.id.value)
    fhir_practitionerRole_practitioner_reference.type_ = uri(value='Practitioner')
    for id_ in cda_assignedEntity.id or []:
        fhir_practitionerRole.identifier.append(fhir4.Identifier())
        II(id_, fhir_practitionerRole.identifier[-1])
    if cda_assignedEntity.code:
        fhir_practitionerRole.code.append(fhir4.CodeableConcept())
        transform_default(cda_assignedEntity.code, fhir_practitionerRole.code[-1])
    for addr in cda_assignedEntity.addr or []:
        fhir_practitioner.address.append(fhir4.Address())
        CdaAdressCompilationToFhirAustrianAddress(addr, fhir_practitioner.address[-1])
    for telecom in cda_assignedEntity.telecom or []:
        fhir_practitioner.telecom.append(fhir4.ContactPoint())
        TELContactPoint(telecom, fhir_practitioner.telecom[-1])
    cda_assignedPerson = cda_assignedEntity.assignedPerson
    if cda_assignedPerson:
        for name in cda_assignedPerson.name or []:
            fhir_practitioner.name.append(fhir4.HumanName())
            transform_default(name, fhir_practitioner.name[-1])
    cda_representedOrganization = cda_assignedEntity.representedOrganization
    if cda_representedOrganization:
        fhir_bundle_entry = fhir4.Bundle_Entry()
        fhir_bundle.entry.append(fhir_bundle_entry)
        fhir_organization = fhir4.Organization()
        fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_organization)
        fhir_organization_id = string(value=dummy_uuid())
        fhir_organization.id = fhir_organization_id
        fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_organization_id is None else fhir_organization_id if isinstance(fhir_organization_id, str) else fhir_organization_id.value)))
        fhir_practitionerRole_organization = fhir4.Reference()
        fhir_practitionerRole.organization = fhir_practitionerRole_organization
        if not fhir_organization.id:
            fhir_organization.id = string(value=dummy_uuid())
        fhir_practitionerRole_organization.reference = string(value=type(fhir_organization).__name__ + '/' + fhir_organization.id.value)
        fhir_practitionerRole_organization.type_ = uri(value='Organization')
        CdaOrganizationCompilationToFhirOrganization(cda_representedOrganization, fhir_organization)

def CdaAssociatedEntityToFhirPractitionerRole(cda_associatedEntity, fhir_practitionerRole, fhir_bundle):
    fhir_bundle_entry = fhir4.Bundle_Entry()
    fhir_bundle.entry.append(fhir_bundle_entry)
    fhir_practitioner = fhir4.Practitioner()
    fhir_bundle_entry.resource = fhir4.ResourceContainer(Practitioner=fhir_practitioner)
    fhir_practitioner_id = string(value=dummy_uuid())
    fhir_practitioner.id = fhir_practitioner_id
    fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitioner_id is None else fhir_practitioner_id if isinstance(fhir_practitioner_id, str) else fhir_practitioner_id.value)))
    fhir_practitionerRole_practitioner_reference = fhir4.Reference()
    fhir_practitionerRole.practitioner = fhir_practitionerRole_practitioner_reference
    if not fhir_practitioner.id:
        fhir_practitioner.id = string(value=dummy_uuid())
    fhir_practitionerRole_practitioner_reference.reference = string(value=type(fhir_practitioner).__name__ + '/' + fhir_practitioner.id.value)
    fhir_practitionerRole_practitioner_reference.type_ = uri(value='Practitioner')
    for id_ in cda_associatedEntity.id or []:
        fhir_practitionerRole.identifier.append(fhir4.Identifier())
        II(id_, fhir_practitionerRole.identifier[-1])
    for addr in cda_associatedEntity.addr or []:
        fhir_practitioner.address.append(fhir4.Address())
        CdaAdressCompilationToFhirAustrianAddress(addr, fhir_practitioner.address[-1])
    for telecom in cda_associatedEntity.telecom or []:
        fhir_practitioner.telecom.append(fhir4.ContactPoint())
        TELContactPoint(telecom, fhir_practitioner.telecom[-1])
    cda_associatedPerson = cda_associatedEntity.associatedPerson
    if cda_associatedPerson:
        for name in cda_associatedPerson.name or []:
            fhir_practitioner.name.append(fhir4.HumanName())
            transform_default(name, fhir_practitioner.name[-1])
    cda_scopingOrganization = cda_associatedEntity.scopingOrganization
    if cda_scopingOrganization:
        fhir_bundle_entry = fhir4.Bundle_Entry()
        fhir_bundle.entry.append(fhir_bundle_entry)
        fhir_organization = fhir4.Organization()
        fhir_bundle_entry.resource = fhir4.ResourceContainer(Organization=fhir_organization)
        fhir_organization_id = string(value=dummy_uuid())
        fhir_organization.id = fhir_organization_id
        fhir_bundle_entry.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_organization_id is None else fhir_organization_id if isinstance(fhir_organization_id, str) else fhir_organization_id.value)))
        fhir_practitionerRole_organization = fhir4.Reference()
        fhir_practitionerRole.organization = fhir_practitionerRole_organization
        if not fhir_organization.id:
            fhir_organization.id = string(value=dummy_uuid())
        fhir_practitionerRole_organization.reference = string(value=type(fhir_organization).__name__ + '/' + fhir_organization.id.value)
        fhir_practitionerRole_organization.type_ = uri(value='Organization')
        CdaOrganizationCompilationToFhirOrganization(cda_scopingOrganization, fhir_organization)

def CdaPerformerToFhirObservationPerformer(cda_performer, fhir_observation, fhir_bundle):
    if cda_performer.time:
        fhir_observation.issued = fhir4.instant()
        transform_default(cda_performer.time, fhir_observation.issued)
    cda_performer_assignedEntity = cda_performer.assignedEntity
    if cda_performer_assignedEntity:
        fhir_bundle_entry01 = fhir4.Bundle_Entry()
        fhir_bundle.entry.append(fhir_bundle_entry01)
        fhir_practitionerRole = fhir4.PractitionerRole()
        fhir_bundle_entry01.resource = fhir4.ResourceContainer(PractitionerRole=fhir_practitionerRole)
        fhir_practitionerRole_id = string(value=dummy_uuid())
        fhir_practitionerRole.id = fhir_practitionerRole_id
        fhir_bundle_entry01.fullUrl = uri(value=('urn:uuid:' + ('' if fhir_practitionerRole_id is None else fhir_practitionerRole_id if isinstance(fhir_practitionerRole_id, str) else fhir_practitionerRole_id.value)))
        fhir_observation_performer_reference = fhir4.Reference()
        fhir_observation.performer.append(fhir_observation_performer_reference)
        if not fhir_practitionerRole.id:
            fhir_practitionerRole.id = string(value=dummy_uuid())
        fhir_observation_performer_reference.reference = string(value=type(fhir_practitionerRole).__name__ + '/' + fhir_practitionerRole.id.value)
        fhir_observation_performer_reference.type_ = uri(value='PractitionerRole')
        CdaAssignedEntityToFhirPractitionerRole(cda_performer_assignedEntity, fhir_practitionerRole, fhir_bundle)

# output
# 1..1 result (boolean)
# 0..1 message with error details for human (string)
# 0..* match with (list)
#   0..1 equivalnce (string from https://hl7.org/fhir/R4B/valueset-concept-map-equivalence.html)
#   0..1 concept
#       0..1 system
#       0..1 version
#       0..1 code
#       0..1 display 
#       0..1 userSelected will always be false, because this is a translation
#   0..1 source (conceptMap url)
# TODO implement reverse
def translate(url=None, conceptMapVersion=None, code=None, system=None, version=None, source=None, coding=None, codeableConcept=None, target=None, targetsystem=None, reverse=None, silent=False)              -> dict [bool, str, list[dict[str, dict[str, str, str, str, bool], str]]]:
    start = time.time()
    
    # start validation and recall of translate in simple from
    if codeableConcept:
        if isinstance(codeableConcept, str): 
            codeableConcept = fhir4.parseString(codeableConcept, silent)
        elif isinstance(coding, fhir4.CodeableConcept):
            pass
        else:
            sys.exit("The codeableConcept parameter has to be a string or a CodeableConcept Object (called method as library)!")
        # the first fit will be returned, else the last unfitted value will be returned
        # TODO check translate params
        for one_coding in codeableConcept.get_coding:
            if (ret := translate(url=url, source=source, coding=one_coding, 
                                 target=target, targetsystem=targetsystem, 
                                 reverse=reverse, silent=True))[0]:
                return ret
        else: return ret
        
    elif coding:
        if isinstance(coding, str): 
            coding = fhir4.parseString(coding, silent)
        elif isinstance(coding, fhir4.Coding):
            pass
        else:
            sys.exit("The coding parameter has to be a string or a Coding Object (called method as library)!")
        # TODO check translate params
        return translate(url=url,  source=source, coding=one_coding, 
                         target=target, targetsystem=targetsystem, 
                         reverse=reverse, silent=True)
        
    elif code:
        if not isinstance(code,str): 
            sys.exit("The code parameter has to be a string!")
        
    elif target:
        if not isinstance(code,str): 
            sys.exit("The target parameter has to be a string!")
        
    elif targetsystem:
        if not isinstance(code,str): 
            sys.exit("The targetsystem parameter has to be a string!")
        
    else:
        sys.exit("At least codeableConcept, coding, code, target or targetSystem has to be given!")
    # end validation and recall of translate in simplier from

    # look for any information from the one ore more generated conceptMaps into con_map_7d
    match = []
    unmapped = []
    if url not in con_map_7d.keys():
        print('   #ERROR# ConceptMap with URL "'+ url +'" is not loaded to this compiled conceptMap #ERROR#')
    else:
        for url_lvl in con_map_7d:
            if url_lvl == "%" or url_lvl == str(url or ""):#+str(("/?version=" and conceptMapVersion) or ""):
                for source_lvl in con_map_7d[url_lvl]:
                    if source_lvl == "%" or not source or source_lvl == source:
                        for target_lvl in con_map_7d[url_lvl][source_lvl]:
                            if target_lvl == "%" or not target or target_lvl == target:
                                for system_lvl in con_map_7d[url_lvl][source_lvl][target_lvl]:
                                    if system_lvl == "%" or not system or system_lvl == system:#+str(("/?version=" and version) or ""):
                                        for targetsystem_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl]:
                                            if targetsystem_lvl == "%" or not targetsystem or targetsystem_lvl == targetsystem:
                                                for code_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl]:
                                                    if code_lvl == "|" or code_lvl == "~" or code_lvl == "#":
                                                        unmapped += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]
                                                    if code_lvl == "%" or not code or code_lvl == code:
                                                        match += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]                
                                                    
    if not match:
        for one_unmapped in unmapped:
            tmp_system = ""
            tmp_version = ""
            tmp_code = ""
            tmp_display = ""
            # replace all "|" values with to translated code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            if one_unmapped["concept"]["code"].startswith("|"):
                tmp_system = system
                tmp_version = version
                tmp_code = one_unmapped["concept"]["code"][1:] + code
            # replace all "~" values with fixed code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            elif one_unmapped["concept"]["code"].startswith("~"):
                tmp_code = one_unmapped["concept"]["code"][1:]
                tmp_display = one_unmapped["concept"]["display"]
            elif one_unmapped["concept"]["code"].startswith("#"):
                # TODO detect recursion like conceptMapA -> conceptMapB -> ConceptMapA -> ...
                return translate(one_unmapped["concept"]["code"][1:], None, code, system, version, source, 
                                 coding, codeableConcept, target, targetsystem, reverse, silent)
            match.append({"equivalence": one_unmapped["equivalence"], 
                          "concept":{
                            "system": tmp_system, 
                            "version": tmp_version, # TODO version of codesystem out of url?
                            "code": tmp_code,
                            "display": tmp_display,
                            "userSelected": False},
                          "source": one_unmapped["source"]})
                
            
    # see if any match is not "unmatched" or "disjoint"
    result = False
    message = ""
    for one_match in match:
        if one_match["equivalence"] != "unmatched" and one_match["equivalence"] != "disjoint":
            result = True 

    if not silent:
        print('Translation in '+str(round(time.time()-start,3))+' seconds for code "'+code+'" with ConceptMap "'+url+'"')
    return {"result": result, "message": message, "match": match}

# The con_map_7d is a seven dimensional dictionary, for quickly finding the fitting translation
# All dimensions except the last are optional, so a explicit NONE value will be used as key and 
# interpreted as the default key, that always will be fitting, no matter what other keys are fitting.
# If a version is included (purely optional), than the version will be added with a blank before to the key
#
# The 0th dimension is mandatory and stating the ConceptMap with its url (including the version).
#
# The 1st dimension is optional and stating the SOURCE valueset (including the version), as one conceptMap can only 
# have a maximum of one SOURCE, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 2nd dimension is optional and stating the TARGET valueset (including the version), as one conceptMap can only 
# have a maximum of one TARGET, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 3th dimension is optional and stating the SYSTEM (including the version) from the source valueset code, as one 
# code could be used in multiple SYSTEMs from the source valueset to translate. 
# Not stating a SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this SYSTEM element as stating the valueset, that should be
# stated in source, this case will still be supported by MaLaC-HD. Having a conceptMap with a source valueset 
# and a different SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#
# The 4th dimension is optional and stating the TARGET SYSTEM (including the version) from the target valueset code, as one 
# code could be used in multiple SYSTEMs from the target valueset to translate. 
# Not stating a TARGET SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this TARGET SYSTEM element as stating the target valueset, that should be
# stated in target, this case will still be supported by MaLaC-HD. Having a conceptMap with a target valueset 
# and a different TARGET SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#   
# The 5th dimension is optional and stating the CODE from the source valueset, as one conceptMap can have none or 
# multiple CODEs from the source to translate. 
#
# The 6th dimension is NOT optional and stating the TARGET CODE from the target valueset. As one source code could be translated 
# in multiple TARGET CODEs, the whole set have to be returend. 
# For a translation with explicitly no TARGET CODE, because of an quivalence of unmatched or disjoint, NONE will be returned. 
#   
# a minimal example, translating "hi" to "servus": 
# con_map_7d = {"myConMap": {None: {None: {"hi": {None: {None: ["equivalent", "<coding><code>servus</code></coding>", "https://my.concept.map/conceptMap/my"]}}}}}
#
# TODO add a dimension for a specific dependsOn property
# TODO add a solution for the unmapped element
con_map_7d = {}


con_map_7d["cda-sdtc-statuscode-2-fhir-composition-status"] = {
    "%": {
        "%": {
            "http://hl7.org/fhir/ValueSet/composition-status": {
                "http://hl7.org/fhir/ValueSet/composition-status": {
                    "active": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/ValueSet/composition-status",
                                "version": "",
                                "code": "preliminary",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "cda-sdtc-statuscode-2-fhir-composition-status"
                        }
                    ],
                    "nullified": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/ValueSet/composition-status",
                                "version": "",
                                "code": "entered-in-error",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "cda-sdtc-statuscode-2-fhir-composition-status"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["cm-v3-administrative-gender"] = {
    "%": {
        "%": {
            "http://terminology.hl7.org/ValueSet/v3-AdministrativeGender": {
                "http://hl7.org/fhir/ValueSet/administrative-gender": {
                    "M": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AdministrativeGender",
                                "version": "",
                                "code": "male",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "cm-v3-administrative-gender"
                        }
                    ],
                    "F": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AdministrativeGender",
                                "version": "",
                                "code": "female",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "cm-v3-administrative-gender"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["ELGAAdministrativeGenderFHIRGender"] = {
    "%": {
        "%": {
            "https://termgit.elga.gv.at/ValueSet-elga-administrativegender": {
                "http://hl7.org/fhir/ValueSet/administrative-gender": {
                    "F": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-administrativegender",
                                "version": "",
                                "code": "female",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAAdministrativeGenderFHIRGender"
                        }
                    ],
                    "M": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-administrativegender",
                                "version": "",
                                "code": "male",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAAdministrativeGenderFHIRGender"
                        }
                    ],
                    "UN": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-administrativegender",
                                "version": "",
                                "code": "other",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAAdministrativeGenderFHIRGender"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["act-status-2-observation-status"] = {
    "%": {
        "%": {
            "http://terminology.hl7.org/ValueSet/v3-ActStatus": {
                "http://hl7.org/fhir/ValueSet/observation-status": {
                    "completed": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-ActStatus",
                                "version": "",
                                "code": "final",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "act-status-2-observation-status"
                        }
                    ],
                    "active": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-ActStatus",
                                "version": "",
                                "code": "preliminary",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "act-status-2-observation-status"
                        }
                    ],
                    "aborted": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-ActStatus",
                                "version": "",
                                "code": "cancelled",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "act-status-2-observation-status"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["addressUse"] = {
    "%": {
        "%": {
            "http://terminology.hl7.org/ValueSet/v3-AddressUse": {
                "http://hl7.org/fhir/valueset-address-use.html": {
                    "H": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "HP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "HV": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "WP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "DIR": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "PUB": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "TMP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "temp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "OLD": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "old",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "BAD": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://terminology.hl7.org/ValueSet/v3-AddressUse",
                                "version": "",
                                "code": "old",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["ELGA2FHIRAddressUse"] = {
    "%": {
        "%": {
            "https://termgit.elga.gv.at/ValueSet/elga-addressuse": {
                "http://hl7.org/fhir/ValueSet/address-use": {
                    "H": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "HP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "HV": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "WP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "DIR": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "PUB": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "TMP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "temp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "PHYS": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ],
                    "PST": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGA2FHIRAddressUse"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["ELGATelecomAddressUseFHIRContactPointUse"] = {
    "%": {
        "%": {
            "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse": {
                "http://hl7.org/fhir/ValueSet/contact-point-use": {
                    "H": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "HP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "HV": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "WP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "AS": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "work",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "EC": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "home",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "MC": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "mobile",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "PG": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "mobile",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ],
                    "TMP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-telecomaddressuse",
                                "version": "",
                                "code": "temp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGATelecomAddressUseFHIRContactPointUse"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["ELGAEntityNameUseFHIRNameUse"] = {
    "%": {
        "%": {
            "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse": {
                "http://hl7.org/fhir/ValueSet/name-use": {
                    "ASGN": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "usual",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "C": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "usual",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "I": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "anonymous",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "L": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "official",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "OR": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "official",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "P": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "anonymous",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "A": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "anonymous",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "R": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "anonymous",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "SRCH": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "temp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "PHON": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "nickname",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "SNDX": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "nickname",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "ABC": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "nickname",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "IDE": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "nickname",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ],
                    "SYL": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynameuse",
                                "version": "",
                                "code": "nickname",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNameUseFHIRNameUse"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["ELGAEntityNamePartQualifierFHIRNamePartQualifier"] = {
    "%": {
        "%": {
            "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier": {
                "http://hl7.org/fhir/ValueSet/name-part-qualifier": {
                    "AC": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "AC",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "AD": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "AD",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "BR": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "BR",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "CL": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "CL",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "IN": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "IN",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "LS": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "LS",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "NB": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "NB",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "PR": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "PR",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "SP": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "SP",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ],
                    "VV": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "https://termgit.elga.gv.at/ValueSet-elga-entitynamepartqualifier",
                                "version": "",
                                "code": "VV",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "ELGAEntityNamePartQualifierFHIRNamePartQualifier"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["OIDtoURI"] = {
    "%": {
        "%": {
            "http://cda.oid": {
                "http://fhir.system": {
                    "2.16.840.1.113883.6.96": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://snomed.info/sct",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://loinc.org",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.8": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://unitsofmeasure.org",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://hl7.org/fhir/sid/icd-10",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.73": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://www.whocc.no/atc",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.2.16.1.4.9": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "urn:oid:2.16.840.1.113883.2.16.1.4.9",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.83": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-ObservationInterpretation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "0.4.0.127.0.16.1.1.2.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ehdsi-edqm-auszug",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.0.3166.1.2.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/iso-3166-alpha-2-code",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.0.3166.1.2.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/iso-3166-1-alpha-3",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.0.5218": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/geschlechtercodes-iso-iec-5218",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.0.639.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/iso-639-2",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.276.0.76.5.547": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/atc-deutsch-wido",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.10.1.4.3.4.3.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-medikationmengenartalternativ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.10.1.4.3.4.3.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationrezeptart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.10.1.4.3.4.3.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationartanwendung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.10.1.4.3.4.3.5": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationdarreichungsform",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.10.1.4.3.4.3.6": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationtherapieart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.10.1.4.3.4.3.7": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationrezeptpflichtstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.10": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-languagecode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.11": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-maritalstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.13": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-observationinterpretation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.14": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/langid-at",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.15": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-participationfunctioncode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.150": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditeventid",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.151": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditeventtype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.152": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditsourcetype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.153": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditroleid",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.154": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditparticipantobjecttype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.155": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditparticipantobjecttyperole",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.156": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditparticipantobjectidtype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.158": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dicom-sopclasses",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.159": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationabgabeart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.16": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-addressuse",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.160": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-actcode-abginfo",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.161": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-actcode-patinfo",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.162": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-wirkstoffe-ages",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.164": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-sectionspflegesitber",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.165": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-auditeventtype-a-arr",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.166": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-dosisparameter",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.167": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-problemstatuscode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.168": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-diagnosesicherheit",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.169": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-problemkataloge",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.17": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-personalrelationship",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.171": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-kulturerregernachweis",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.172": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-laendercodes",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.173": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-humanlanguage",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.174": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-proficiencylevelcode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.175": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-languageabilitymode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.176": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-seitenlokalisation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.177": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-allergyorintolerancetype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.178": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownallergies",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.179": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownproblems",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.18": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-religiousaffiliation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.180": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-allergyorintoleranceagent",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.181": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-allergyreaction",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.182": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-criticalityobservationvalue",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.183": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-allergystatuscode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.184": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-conditionverificationstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.186": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-nachweisergebnis",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.187": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-probenmaterial",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.188": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-mikroorganismen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.189": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-problemseverity",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.19": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-roleclass",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.191": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownmedication",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.192": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-absentorunknowndevices",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.193": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownprocedures",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.198": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-conditionstatuscode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-nullflavor",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.202": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-expecteddeliverydatemethod",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.203": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-pregnanciessummary",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.204": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-currentsmokingstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.205": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-typeofproblem",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.206": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-allergietyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.210": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/atcdabbr-noinformationqualifier",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.211": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/atcdabbr-lateralityqualifiercode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.22": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-serviceeventslabor",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.25": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-urlscheme",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.26": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-rollen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.27": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-entitynameuse",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.29": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-informationrecipienttype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-realmcode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.30": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationtherapieart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.32": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationmengenart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.33": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-noinformation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.34": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-vitalparameterarten",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.35": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-problemarten",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.36": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-telecomaddressuse",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.360": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-sectionsserviceevents",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.39": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-dokumentenklassen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-administrativegender",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.42": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medientyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.43": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-serviceeventperformer",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.44": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-laborparameter",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.46": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-specimentype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.47": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-laborstruktur",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.48": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-sectionsentlassungaerztl",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.49": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-sectionsentlassungpflege",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.5": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-actencountercode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.50": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-sectionsradiologie",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.52": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-humanactsite",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.53": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-entlassungsmanagementa",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.55": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-mammogramassessment",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.57": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-serviceeventsentlassbr",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.58": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-significantpathogens",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.59": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-einnahmezeitpunkte",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.6": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-authorspeciality",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.60": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-formatcodezusatz",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.61": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-formatcode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.62": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/appc-modalitaet",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.63": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/appc-lateralitaet",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.64": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/appc-prozeduren",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.65": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/appc-anatomie",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.66": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationartanwendung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.67": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationmengenartalternativ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.68": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationrezeptart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.69": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationfrequenz",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.7": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-confidentiality",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.70": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationdarreichungsform",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.71": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationpharmazeutischeempfehlungstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.72": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-healthcarefacilitytypecode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.74": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-medikationrezeptpflichtstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.75": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-practicesetting",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.86": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-xds-dokumentenklassen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.9": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-insuredassocentity",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.90": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-anti-hcv-immunoassay",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.91": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-anti-hcv-immunoblot-assay",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.92": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-hcv-core-ag-assay",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.10.93": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-hbv-status",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.3.1.10": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/oeaek-berechtigungen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.3.1.10.20": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/oeaek-fachrichtung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.3.1.10.30": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/oeaek-additivfach",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.3.1.10.40": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/oeaek-spezialdiplom",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.3.1.10.50": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/oeaek-zertifikate",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.3.1.10.60": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/oeaek-spezialisierung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.4.16": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/asp-liste",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.4.16.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/eimpf-impfstoffe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.101": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-parameter",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.102": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-methoden",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.103": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-actcode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.104": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/rast-klassen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.105": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.106": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-hcv-rna",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.109": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-klinischemanifestation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.11": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-laborparameterergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.110": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-wurdekrankheitimportiert",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.12": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-practicesetting",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.150": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditeventid",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.151": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditeventtype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.152": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.153": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.154": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttyperole",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.155": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjectidtype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.156": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikation-ages-wirkstoffe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.158": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.159": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-e-health-anwendungen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.160": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-fachaerzte",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.161": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-kontakttypen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.162": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-patienten-identifizierungsmethoden",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.165": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-auditeventtypea-arr",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.171": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/icd-10-bmg-2017",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.173": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-specimentype-ergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.175": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/icpc2",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.179": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-ergaenzungsliste",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.180": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-allergietyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.183": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.184": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/icd-10-bmg-2020",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.186": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/eimpf-historischeimpfstoffe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.187": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-ergaenzungmeldepflichtigekrankheiten",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.190": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-pflegestufen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.191": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/eimpf-zuordnungsmatrix",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.194": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/exnds-sections",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.195": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/exnds-concepts",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.196": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-betreuung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.197": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-listeschulen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.198": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/exnds-metadaten",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.199": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/exnds-formatcodes",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-gtelvogdarollen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.200": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/eimpf-schemamatrix",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.202": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/dgc-qr",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.203": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/dgc-antibody-test",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.205": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/dgc-ratnamemanufacturer",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.206": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-typ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.207": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-art",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.208": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-statauf",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.209": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/bmg-icd-10-2022",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.21": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-serviceeventsentlassbrief",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.211": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/lkf-ergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.215": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-lebensmittelallergene",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.216": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/bmg-lkf-2023",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.217": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.219": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationpackaging",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.221": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/medikationactiveingredient",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.222": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/bmg-icd-10-2024",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.223": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/bmg-lkf-2024",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.224": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.28": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-entlassungsmanagementart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.37": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-formatcode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.38.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/appc-modalitaet",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.38.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/appc-lateralitaet",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.38.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/appc-prozeduren",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.38.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/appc-anatomie",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/gda-attribute",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.40": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-sections",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.41": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-formatcodezusatz",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.45": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-significantpathogens",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.49": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-mammogramassessment",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.5": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/gda-org",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.55": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/elga-urlschemeergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.56": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/icd-10-bmg-2014",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.58": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-material",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.59": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-janein",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.60": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-analysedetails",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.61": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-antigenh",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.62": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-artmalaria",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.63": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-artquartier",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.64": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-befundart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.65": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-biotyp-biovar",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.66": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-antigeno",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.67": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-tbc-resistenzergaenzung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.68": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-quartiercode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.69": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-aviditaet",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.70": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-genogruppe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.71": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-genotyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.72": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-genotyppora-r1",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.73": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-genotyppora-r2",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.74": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-durchgefuehrt",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.75": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-tbc-resultat",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.76": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-ergebnis",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.77": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-wowurdekrankheiterworben",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.78": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-nachweisbar",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.79": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-testmethodemic-ipd",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.80": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-testmethodetypingipd",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.81": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-nachweis",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.82": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-multilocsequ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.83": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-monoclonalsub",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.84": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-organ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.85": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-yersinapathogen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.86": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-phagentyp-vtec",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.87": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-phagentyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.88": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-posneg",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.89": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-ribotype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.90": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-serogruppe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.91": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-serotyp-gene-feta",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.92": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-serotyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.93": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-gewinnung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.94": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-tbc-typ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.95": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-orth20probe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.96": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-reiseland",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.97": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-verotoxin-2-subtyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.98": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-verotoxin-1-subtyp",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.5.99": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ems-materialmethode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.10": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-historischeimpfstoffe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.11": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-impfrollen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.13": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-antikoerperbestimmung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.14": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-impfstoffe",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.15": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-specialsituationindication",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.16": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-materialmethode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.18": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-krankheitsmerkmale",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.19": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-meldepflichtige-krankheiten",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-impfrelevanteerkrankung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.21": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-taetigkeitsbereich",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.23": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-artderdiagnose",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.24": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-impfstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.25": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-klinischemanifestation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.26": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-serviceeventstelemonepi",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.27": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-actconsenttype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.29": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-lebensmittelbedingteintoxikationen",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-specialcasevaccination",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.30": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-alcoholconsumption",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.31": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-betreuung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-immunizationtarget",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.49": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-hospitalisierung",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.5": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-impfschema",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.51": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ems-reiseland",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.52": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-pregnancystatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.53": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-antibiogramm",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.54": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-typeoftest",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.55": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-diseaseoragent",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.56": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-vaccine",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.57": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-medicinalproduct",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.58": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-vaccinemarketingauthorizationholder",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.59": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-doses",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.6": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-impfdosis",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.60": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-resultofthetest",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.62": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-zusatzklassifikation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.63": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-country",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.65": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/dgc-ratnamemanufacturer",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.66": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-anamneselabormikrobiologie",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.67": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/lkf-diagnose-art",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.68": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/lkf-diagnose-statauf",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.69": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/lkf-diagnose-typ",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.7": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-impfgrund",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.74": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-mengenart",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.75": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-lebensmittelallergene",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.76": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/austrian-designation-use",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.77": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-patientidentifier",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.78": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-zusatzklassifikation-impfprogramm",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.79": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/eimpf-zusatzklassifikation-impfsetting",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.8": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/elga-entitynamepartqualifier",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.81": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-administrativegender-v2",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.83": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-administrativegender-fhir-extension",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.84": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/orphanet-rare-diseases",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.85": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/snomed-rare-diseases",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.86": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-organizationtype",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.40.0.34.6.0.10.87": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/hl7-at-practitionerrole",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.840.10003.5.109": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/iana-mime-type",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.840.10008.2.16.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/dcm",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.2.840.10008.2.6.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/dicom-sopclasses",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-healthcare-professional-role",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.12": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-route-of-administration",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.16": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-unit",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-doseform",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.24": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-activeingredient",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.3": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-package",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.31": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-confidentiality",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.34": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-administrative-gender",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-country",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.40": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-telecom-address",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.41": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-timing-event",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.56": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-quantity-unit",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.6": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-language",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.12559.11.10.1.3.1.42.61": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-substance",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1.3.6.1.4.1.19376.1.9.2.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ihe-pharmaceutical-advice-status-list",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "1dd183a6-6d2b-4a9d-8f5d-be09d6bb5a6e": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ehdsi-language",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.1.11.19708": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/ValueSet/ehdsi-actsubstanceadministrationcode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.12.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-administrative-sex",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.12.496": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-consent-type",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.12.497": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-consent-mode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.12.498": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-consent-status",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.12.548": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-signatorys-relationship-to-subject",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.18.108": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v2-0203",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.18.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v2-0001",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.18.320": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v2-0496",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.18.321": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v2-0497",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.18.322": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v2-0498",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.18.355": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v2-0548",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.2.16.1.4.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-at-religionaustria",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.2.16.1.4.10": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-at-formatcodes",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.2.9.6.2.7": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/isco",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.3.6905.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ehdsi-substance",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.3.7.1.7": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/sciphox-seitenlokalisation",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.3.7.1.8": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/sciphox-diagnosenzusatz",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.4.642.1.76": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://hl7.org/fhir/event-timing",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.4.642.3.115": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.4.642.3.155": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ips-conditionclinicalstatuscodes",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-AdministrativeGender",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.1008": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-NullFlavor",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.1052": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-ActSite",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.110": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-RoleClass",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.111": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-RoleCode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.1119": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-AddressUse",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.1150.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/ips-absentorunknowndata",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.129": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-SpecimenType",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.139": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-TimingEvent",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.14": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-ActStatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.143": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-URLScheme",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.2": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-MaritalStatus",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.25": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-Confidentiality",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.4": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.43": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-EntityNamePartQualifier",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.45": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-EntityNameUse",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.60": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityMode",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.61": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityProficiency",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.88": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-ParticipationFunction",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.5.90": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.121": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/hl7-language-identification",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.24": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/mdc-medicaldevicecommunications",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.254-2005": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/icf",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "2.16.840.1.113883.6.43.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://cda.oid",
                                "version": "",
                                "code": "https://termgit.elga.gv.at/CodeSystem/icd-o-3",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ],
                    "|": [
                        {
                            "equivalence": "equal",
                            "concept": {
                                "system": "",
                                "version": "",
                                "code": "|urn:oid:",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "OIDtoURI"
                        }
                    ]
                }
            }
        }
    }
}
def II(src, tgt):
    Any(src, tgt)
    r = src.root
    if r:
        if utils.single([bool([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'extension')])]):
            trans_out = translate(url='OIDtoURI', code=(r if isinstance(r, str) else r.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.system = string(value=match)
    r = src.root
    if r:
        if utils.single(fhirpath_utils.bool_and([not([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'extension')])], [v6 for v5 in [v4 for v3 in [src] for v4 in fhirpath_utils.get(v3,'root')] for v6 in fhirpath_utils.matches(v5, ['[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}'])])):
            tgt.system = uri(value='urn:ietf:rfc:3986')
            tgt.value = string(value=utils.single(fhirpath_utils.add(['urn:uuid:'], [v2 for v1 in [r] for v2 in fhirpath_utils.lower(v1)])))
    r = src.root
    if r:
        if utils.single(fhirpath_utils.bool_and([not([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'extension')])], [v6 for v5 in [v4 for v3 in [src] for v4 in fhirpath_utils.get(v3,'root')] for v6 in fhirpath_utils.contains(v5, ['.'])])):
            tgt.system = uri(value='urn:ietf:rfc:3986')
            tgt.value = string(value=('urn:oid:' + ('' if r is None else r if isinstance(r, str) else r.value)))
    e = src.extension
    if e:
        tgt.value = string(value=e)
    s = src.assigningAuthorityName
    if s:
        a = fhir4.Reference()
        if tgt.assigner is not None:
            a = tgt.assigner
        else:
            tgt.assigner = a
        a.display = string(value=s)
    displayable = src.displayable
    if displayable:
        ext = fhir4.Extension()
        tgt.extension.append(ext)
        ext.url = 'http://hl7.org/fhir/cdaStructureDefinition/extension-displayable'
        ext.valueString = string(value=str(v))

def INT(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = v

def BL(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = v

def TSInstant(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = dateutil.parser.parse(str(v))

def IVL_TSDateTime(src, tgt):
    TSInstant(src, tgt)

def TSDateTime(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = dateutil.parser.parse(str(v)).isoformat()

def IVXB_TSDateTime(src, tgt):
    TSDateTime(src, tgt)

def TSDate(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = utils.single([v2 for v1 in [str(dateutil.parser.parse(v).isoformat())] for v2 in fhirpath_utils.substring(v1,[0],[10])])

def IVLTSPeriod(src, tgt):
    Any(src, tgt)
    if src.low:
        tgt.start = fhir4.dateTime()
        transform_default(src.low, tgt.start)
    if src.high:
        tgt.end = fhir4.dateTime()
        transform_default(src.high, tgt.end)

def IVLTSDateTime(src, tgt):
    Any(src, tgt)
    s = src.low
    TSDateTime(s, tgt)

def STstring(src, tgt):
    Any(src, tgt)
    v = src
    if v:
        tgt.value = v

def EDstring(src, tgt):
    Any(src, tgt)
    v = src.valueOf_.strip()
    if v:
        tgt.value = v
    v = src
    if v:
        if utils.single([bool([v4 for v3 in [v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'reference')] for v4 in fhirpath_utils.get(v3,'value')])]):
            tgt.value = utils.single([v8 for v7 in [v6 for v5 in [v4 for v3 in [v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'reference')] for v4 in fhirpath_utils.get(v3,'value')] for v6 in fhirpath_utils.resolve(v5, [v])] for v8 in fhirpath_utils.get(v7,'valueOf_',strip=True)])

def ENstring(src, tgt):
    Any(src, tgt)
    v = src
    if v:
        tgt.value = utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)])

def ONstring(src, tgt):
    ENstring(src, tgt)

def CSCode(src, tgt):
    Any(src, tgt)
    c = src.code
    if c:
        tgt.value = c

def CECode(src, tgt):
    CSCode(src, tgt)

def CDCode(src, tgt):
    CSCode(src, tgt)

def CECoding(src, tgt):
    Any(src, tgt)
    code = src.code
    if code:
        tgt.code = string(value=str(code))
    system = src.codeSystem
    if system:
        trans_out = translate(url='OIDtoURI', code=(system if isinstance(system, str) else system.value), silent=False)
        matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
        # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
        if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
        match = matches[0]['code']
        tgt.system = string(value=match)
    display = src.displayName
    if display:
        tgt.display = string(value=str(display))

def CDCoding(src, tgt):
    CECoding(src, tgt)

def CECodeableConcept(src, tgt):
    Any(src, tgt)
    if src.originalText:
        tgt.text = fhir4.string()
        EDstring(src.originalText, tgt.text)
    coding = fhir4.Coding()
    tgt.coding.append(coding)
    CECoding(src, coding)
    for translation in src.translation or []:
        coding = fhir4.Coding()
        tgt.coding.append(coding)
        code = translation.code
        if code:
            coding.code = string(value=str(code))
        system = src.codeSystem
        if system:
            trans_out = translate(url='OIDtoURI', code=(system if isinstance(system, str) else system.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.systemCode = string(value=match)
        display = translation.displayName
        if display:
            coding.display = string(value=str(display))

def CSCodeableConcept(src, tgt):
    CECodeableConcept(src, tgt)

def CDCodeableConcept(src, tgt):
    CECodeableConcept(src, tgt)

def ENHumanName(src, tgt):
    Any(src, tgt)
    for v in src.family or []:
        tgt.family = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    for v in src.given or []:
        for v4 in [string(value=v3) for v3 in [v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]]:
            tgt.given.append(v4)
    for v in src.prefix or []:
        for v4 in [string(value=v3) for v3 in [v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]]:
            tgt.prefix.append(v4)
    for v in src.suffix or []:
        for v4 in [string(value=v3) for v3 in [v2 for v1 in [v] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]]:
            tgt.suffix.append(v4)
    if src.validTime:
        tgt.period = fhir4.Period()
        IVLTSPeriod(src.validTime, tgt.period)

def CdaPersonNameCompilationToFhirHumanName(cda_name, fhir_humanName):
    Any(cda_name, fhir_humanName)
    cda_name_use = cda_name.use
    if cda_name_use:
        trans_out = translate(url='ELGAEntityNameUseFHIRNameUse', code=(cda_name_use if isinstance(cda_name_use, str) else cda_name_use.value), silent=False)
        matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
        # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
        if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
        match = matches[0]['code']
        fhir_humanName.use = string(value=match)
    cda_name_text = cda_name.valueOf_.strip()
    if cda_name_text:
        fhir_humanName.text = string(value=utils.single([cda_name_text]))
    for cda_name_prefix in cda_name.prefix or []:
        fhir_humanName_prefix = [string(value=v3) for v3 in [v2 for v1 in [cda_name_prefix] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]]
        for v4 in fhir_humanName_prefix:
            fhir_humanName.prefix.append(v4)
        extension = []
        for _fhir_humanName_prefix in fhir_humanName_prefix:
            _extension = fhir4.Extension()
            _fhir_humanName_prefix.extension.append(_extension)
            extension.append(_extension)
        if utils.single([bool([v2 for v1 in [cda_name_prefix] for v2 in fhirpath_utils.get(v1,'qualifier')])]):
            for _extension in extension:
                _extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-EN-qualifier'
        qualifier = cda_name_prefix.qualifier
        if qualifier:
            for _extension in extension:
                trans_out = translate(url='ELGAEntityNamePartQualifierFHIRNamePartQualifier', code=(qualifier if isinstance(qualifier, str) else qualifier.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                _extension.valueCode = string(value=match)
    for cda_name_given in cda_name.given or []:
        fhir_humanName_given = [string(value=v3) for v3 in [v2 for v1 in [cda_name_given] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]]
        for v4 in fhir_humanName_given:
            fhir_humanName.given.append(v4)
        extension = []
        for _fhir_humanName_given in fhir_humanName_given:
            _extension = fhir4.Extension()
            _fhir_humanName_given.extension.append(_extension)
            extension.append(_extension)
        if utils.single([bool([v2 for v1 in [cda_name_given] for v2 in fhirpath_utils.get(v1,'qualifier')])]):
            for _extension in extension:
                _extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-EN-qualifier'
        qualifier = cda_name_given.qualifier
        if qualifier:
            for _extension in extension:
                trans_out = translate(url='ELGAEntityNamePartQualifierFHIRNamePartQualifier', code=(qualifier if isinstance(qualifier, str) else qualifier.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                _extension.valueCode = string(value=match)
    for cda_name_family in cda_name.family or []:
        fhir_humanName_family = string(value=utils.single([v2 for v1 in [cda_name_family] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
        fhir_humanName.family = fhir_humanName_family
        if fhir_humanName_family:
            extension = fhir4.Extension()
            fhir_humanName_family.extension.append(extension)
        else:
            extension = None
        if utils.single([bool([v2 for v1 in [cda_name_family] for v2 in fhirpath_utils.get(v1,'qualifier')])]):
            if extension:
                extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-EN-qualifier'
        qualifier = cda_name_family.qualifier
        if qualifier:
            if extension:
                trans_out = translate(url='ELGAEntityNamePartQualifierFHIRNamePartQualifier', code=(qualifier if isinstance(qualifier, str) else qualifier.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                extension.valueCode = string(value=match)
    for cda_name_suffix in cda_name.suffix or []:
        fhir_humanName_suffix = [string(value=v3) for v3 in [v2 for v1 in [cda_name_suffix] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]]
        for v4 in fhir_humanName_suffix:
            fhir_humanName.suffix.append(v4)
        extension = []
        for _fhir_humanName_suffix in fhir_humanName_suffix:
            _extension = fhir4.Extension()
            _fhir_humanName_suffix.extension.append(_extension)
            extension.append(_extension)
        if utils.single([bool([v2 for v1 in [cda_name_suffix] for v2 in fhirpath_utils.get(v1,'qualifier')])]):
            for _extension in extension:
                _extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-EN-qualifier'
        qualifier = cda_name_suffix.qualifier
        if qualifier:
            for _extension in extension:
                trans_out = translate(url='ELGAEntityNamePartQualifierFHIRNamePartQualifier', code=(qualifier if isinstance(qualifier, str) else qualifier.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                match = matches[0]['code']
                _extension.valueCode = string(value=match)

def CdaOrganizationCompilationToFhirOrganization(cda_organization, fhir_organization):
    for id_ in cda_organization.id or []:
        fhir_organization.identifier.append(fhir4.Identifier())
        II(id_, fhir_organization.identifier[-1])
    for name in cda_organization.name or []:
        fhir_organization.name = fhir4.string()
        transform_default(name, fhir_organization.name)
    for telecom in cda_organization.telecom or []:
        fhir_organization.telecom.append(fhir4.ContactPoint())
        TELContactPoint(telecom, fhir_organization.telecom[-1])
    for addr in cda_organization.addr or []:
        fhir_organization.address.append(fhir4.Address())
        CdaAdressCompilationToFhirAustrianAddress(addr, fhir_organization.address[-1])

def CdaAdressCompilationToFhirAustrianAddress(cda_address, fhir_address):
    Any(cda_address, fhir_address)
    cda_use = cda_address.use
    if cda_use:
        trans_out = translate(url='ELGA2FHIRAddressUse', code=(cda_use if isinstance(cda_use, str) else cda_use.value), silent=False)
        matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
        # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
        if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
        match = matches[0]['code']
        fhir_address.use = string(value=match)
        if utils.single((fhirpath_utils.bool_and((fhirpath_utils.equals([cda_use], '!=', ['PHYS'])), (fhirpath_utils.equals([cda_use], '!=', ['PST']))))):
            fhir_address.type_ = string(value='both')
        if utils.single((fhirpath_utils.equals([cda_use], '==', ['PHYS']))):
            fhir_address.type_ = string(value='physical')
        if utils.single((fhirpath_utils.equals([cda_use], '==', ['PST']))):
            fhir_address.type_ = string(value='postal')
    for cda_postalCode in cda_address.postalCode or []:
        fhir_address.postalCode = string(value=utils.single([v2 for v1 in [cda_postalCode] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    for cda_city in cda_address.city or []:
        fhir_address.city = string(value=utils.single([v2 for v1 in [cda_city] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    for cda_state in cda_address.state or []:
        fhir_address.state = string(value=utils.single([v2 for v1 in [cda_state] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    for cda_country in cda_address.country or []:
        fhir_address.country = string(value=utils.single([v2 for v1 in [cda_country] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    if utils.single((fhirpath_utils.bool_and([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'streetName')])], [bool([v4 for v3 in [cda_address] for v4 in fhirpath_utils.get(v3,'houseNumber')])]))):
        fhir_address_line = fhir4.string()
        fhir_address.line.append(fhir_address_line)
        if utils.single(([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'additionalLocator')])])):
            fhir_address_line.value = utils.single(fhirpath_utils.add(fhirpath_utils.add(fhirpath_utils.add(fhirpath_utils.add([v4 for v3 in [v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'streetName')] for v4 in fhirpath_utils.get(v3,'valueOf_',strip=True)], [' ']), [v8 for v7 in [v6 for v5 in [cda_address] for v6 in fhirpath_utils.get(v5,'houseNumber')] for v8 in fhirpath_utils.get(v7,'valueOf_',strip=True)]), [' ']), [v12 for v11 in [v10 for v9 in [cda_address] for v10 in fhirpath_utils.get(v9,'additionalLocator')] for v12 in fhirpath_utils.get(v11,'valueOf_',strip=True)]))
        if utils.single((fhirpath_utils.bool_not([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'additionalLocator')])]))):
            fhir_address_line.value = utils.single(fhirpath_utils.add(fhirpath_utils.add([v4 for v3 in [v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'streetName')] for v4 in fhirpath_utils.get(v3,'valueOf_',strip=True)], [' ']), [v8 for v7 in [v6 for v5 in [cda_address] for v6 in fhirpath_utils.get(v5,'houseNumber')] for v8 in fhirpath_utils.get(v7,'valueOf_',strip=True)]))
        for cda_address_streetName in cda_address.streetName or []:
            extension = fhir4.Extension()
            fhir_address_line.extension.append(extension)
            extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-ADXP-streetName'
            extension.valueString = string(value=utils.single([v2 for v1 in [cda_address_streetName] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
        for cda_address_houseNumber in cda_address.houseNumber or []:
            extension = fhir4.Extension()
            fhir_address_line.extension.append(extension)
            extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-ADXP-houseNumber'
            extension.valueString = string(value=utils.single([v2 for v1 in [cda_address_houseNumber] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
        for cda_address_additionalLocator in cda_address.additionalLocator or []:
            if utils.single(([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'additionalLocator')])])):
                extension = fhir4.Extension()
                fhir_address_line.extension.append(extension)
                extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-ADXP-additionalLocator'
                extension.valueString = string(value=utils.single([v2 for v1 in [cda_address_additionalLocator] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))
    if utils.single(([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'streetAddressLine')])])):
        fhir_address_line = fhir4.string()
        fhir_address.line.append(fhir_address_line)
        if utils.single(([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'additionalLocator')])])):
            fhir_address_line.value = utils.single(fhirpath_utils.add(fhirpath_utils.add([v4 for v3 in [v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'streetAddressLine')] for v4 in fhirpath_utils.get(v3,'valueOf_',strip=True)], [' ']), [v8 for v7 in [v6 for v5 in [cda_address] for v6 in fhirpath_utils.get(v5,'additionalLocator')] for v8 in fhirpath_utils.get(v7,'valueOf_',strip=True)]))
        if utils.single((fhirpath_utils.bool_not([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'additionalLocator')])]))):
            fhir_address_line.value = utils.single([v4 for v3 in [v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'streetAddressLine')] for v4 in fhirpath_utils.get(v3,'valueOf_',strip=True)])
        for cda_address_additionalLocator in cda_address.additionalLocator or []:
            if utils.single(([bool([v2 for v1 in [cda_address] for v2 in fhirpath_utils.get(v1,'additionalLocator')])])):
                extension = fhir4.Extension()
                fhir_address_line.extension.append(extension)
                extension.url = 'http://hl7.org/fhir/StructureDefinition/iso21090-ADXP-additionalLocator'
                extension.valueString = string(value=utils.single([v2 for v1 in [cda_address_additionalLocator] for v2 in fhirpath_utils.get(v1,'valueOf_',strip=True)]))

def TELContactPoint(src, tgt):
    Any(src, tgt)
    u = src.use
    if u:
        if utils.single((fhirpath_utils.bool_or([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'use')] for v4 in fhirpath_utils.startswith(v3, ['H'])], (fhirpath_utils.equals([v6 for v5 in [src] for v6 in fhirpath_utils.get(v5,'use')], '==', ['EC']))))):
            trans_out = translate(url='ELGATelecomAddressUseFHIRContactPointUse', code=(u if isinstance(u, str) else u.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.use = string(value=match)
    u = src.use
    if u:
        if utils.single((fhirpath_utils.bool_or((fhirpath_utils.equals([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'use')], '==', ['WP'])), (fhirpath_utils.equals([v4 for v3 in [src] for v4 in fhirpath_utils.get(v3,'use')], '==', ['AS']))))):
            trans_out = translate(url='ELGATelecomAddressUseFHIRContactPointUse', code=(u if isinstance(u, str) else u.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.use = string(value=match)
    u = src.use
    if u:
        if utils.single((fhirpath_utils.bool_or((fhirpath_utils.equals([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'use')], '==', ['MC'])), (fhirpath_utils.equals([v4 for v3 in [src] for v4 in fhirpath_utils.get(v3,'use')], '==', ['PG']))))):
            trans_out = translate(url='ELGATelecomAddressUseFHIRContactPointUse', code=(u if isinstance(u, str) else u.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.use = string(value=match)
    u = src.use
    if u:
        if utils.single(((fhirpath_utils.equals([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'use')], '==', ['TMP'])))):
            trans_out = translate(url='ELGATelecomAddressUseFHIRContactPointUse', code=(u if isinstance(u, str) else u.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.use = string(value=match)
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['fax:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[4],[])]))
            tgt.system = string(value='fax')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['file:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[5],[])]))
            tgt.system = string(value='other')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['ftp:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[4],[])]))
            tgt.system = string(value='url')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['http:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[7],[])]))
            tgt.system = string(value='url')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['mailto:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[7],[])]))
            tgt.system = string(value='email')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['mllp:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[5],[])]))
            tgt.system = string(value='url')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['modem:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[6],[])]))
            tgt.system = string(value='other')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['nfs:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[4],[])]))
            tgt.system = string(value='other')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['tel:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[4],[])]))
            tgt.system = string(value='phone')
    v = src.value
    if v:
        if utils.single(([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'value')] for v4 in fhirpath_utils.startswith(v3, ['telnet:'])])):
            tgt.value = string(value=utils.single([v2 for v1 in [v] for v2 in fhirpath_utils.substring(v1,[7],[])]))
            tgt.system = string(value='url')
    for useablePeriod in src.useablePeriod or []:
        tgt.period = fhir4.Period()
        transform_default(useablePeriod, tgt.period)

def PQQuantity(src, tgt):
    Any(src, tgt)
    tgt.system = uri(value='http://unitsofmeasure.org')
    unit = src.unit
    if unit:
        tgt.code = string(value=unit)
    value = src.value
    if value:
        tgt.value = decimal(value=value)

def IVXB_PQQuantity(src, tgt):
    PQQuantity(src, tgt)

def RTOPQPQRatio(src, tgt):
    Any(src, tgt)
    numerator = src.numerator
    if numerator:
        targetNumerator = fhir4.Quantity()
        if tgt.numerator is not None:
            targetNumerator = tgt.numerator
        else:
            tgt.numerator = targetNumerator
        PQQuantity(numerator, targetNumerator)
    denominator = src.denominator
    if denominator:
        targetDenominator = fhir4.Quantity()
        if tgt.denominator is not None:
            targetDenominator = tgt.denominator
        else:
            tgt.denominator = targetDenominator
        PQQuantity(denominator, targetDenominator)

def Any(src, tgt):
    pass

def unpack_container(resource_container):
    if resource_container.Account is not None:
        return resource_container.Account
    if resource_container.ActivityDefinition is not None:
        return resource_container.ActivityDefinition
    if resource_container.AdministrableProductDefinition is not None:
        return resource_container.AdministrableProductDefinition
    if resource_container.AdverseEvent is not None:
        return resource_container.AdverseEvent
    if resource_container.AllergyIntolerance is not None:
        return resource_container.AllergyIntolerance
    if resource_container.Appointment is not None:
        return resource_container.Appointment
    if resource_container.AppointmentResponse is not None:
        return resource_container.AppointmentResponse
    if resource_container.AuditEvent is not None:
        return resource_container.AuditEvent
    if resource_container.Basic is not None:
        return resource_container.Basic
    if resource_container.Binary is not None:
        return resource_container.Binary
    if resource_container.BiologicallyDerivedProduct is not None:
        return resource_container.BiologicallyDerivedProduct
    if resource_container.BodyStructure is not None:
        return resource_container.BodyStructure
    if resource_container.Bundle is not None:
        return resource_container.Bundle
    if resource_container.CapabilityStatement is not None:
        return resource_container.CapabilityStatement
    if resource_container.CarePlan is not None:
        return resource_container.CarePlan
    if resource_container.CareTeam is not None:
        return resource_container.CareTeam
    if resource_container.CatalogEntry is not None:
        return resource_container.CatalogEntry
    if resource_container.ChargeItem is not None:
        return resource_container.ChargeItem
    if resource_container.ChargeItemDefinition is not None:
        return resource_container.ChargeItemDefinition
    if resource_container.Citation is not None:
        return resource_container.Citation
    if resource_container.Claim is not None:
        return resource_container.Claim
    if resource_container.ClaimResponse is not None:
        return resource_container.ClaimResponse
    if resource_container.ClinicalImpression is not None:
        return resource_container.ClinicalImpression
    if resource_container.ClinicalUseDefinition is not None:
        return resource_container.ClinicalUseDefinition
    if resource_container.CodeSystem is not None:
        return resource_container.CodeSystem
    if resource_container.Communication is not None:
        return resource_container.Communication
    if resource_container.CommunicationRequest is not None:
        return resource_container.CommunicationRequest
    if resource_container.CompartmentDefinition is not None:
        return resource_container.CompartmentDefinition
    if resource_container.Composition is not None:
        return resource_container.Composition
    if resource_container.ConceptMap is not None:
        return resource_container.ConceptMap
    if resource_container.Condition is not None:
        return resource_container.Condition
    if resource_container.Consent is not None:
        return resource_container.Consent
    if resource_container.Contract is not None:
        return resource_container.Contract
    if resource_container.Coverage is not None:
        return resource_container.Coverage
    if resource_container.CoverageEligibilityRequest is not None:
        return resource_container.CoverageEligibilityRequest
    if resource_container.CoverageEligibilityResponse is not None:
        return resource_container.CoverageEligibilityResponse
    if resource_container.DetectedIssue is not None:
        return resource_container.DetectedIssue
    if resource_container.Device is not None:
        return resource_container.Device
    if resource_container.DeviceDefinition is not None:
        return resource_container.DeviceDefinition
    if resource_container.DeviceMetric is not None:
        return resource_container.DeviceMetric
    if resource_container.DeviceRequest is not None:
        return resource_container.DeviceRequest
    if resource_container.DeviceUseStatement is not None:
        return resource_container.DeviceUseStatement
    if resource_container.DiagnosticReport is not None:
        return resource_container.DiagnosticReport
    if resource_container.DocumentManifest is not None:
        return resource_container.DocumentManifest
    if resource_container.DocumentReference is not None:
        return resource_container.DocumentReference
    if resource_container.Encounter is not None:
        return resource_container.Encounter
    if resource_container.Endpoint is not None:
        return resource_container.Endpoint
    if resource_container.EnrollmentRequest is not None:
        return resource_container.EnrollmentRequest
    if resource_container.EnrollmentResponse is not None:
        return resource_container.EnrollmentResponse
    if resource_container.EpisodeOfCare is not None:
        return resource_container.EpisodeOfCare
    if resource_container.EventDefinition is not None:
        return resource_container.EventDefinition
    if resource_container.Evidence is not None:
        return resource_container.Evidence
    if resource_container.EvidenceReport is not None:
        return resource_container.EvidenceReport
    if resource_container.EvidenceVariable is not None:
        return resource_container.EvidenceVariable
    if resource_container.ExampleScenario is not None:
        return resource_container.ExampleScenario
    if resource_container.ExplanationOfBenefit is not None:
        return resource_container.ExplanationOfBenefit
    if resource_container.FamilyMemberHistory is not None:
        return resource_container.FamilyMemberHistory
    if resource_container.Flag is not None:
        return resource_container.Flag
    if resource_container.Goal is not None:
        return resource_container.Goal
    if resource_container.GraphDefinition is not None:
        return resource_container.GraphDefinition
    if resource_container.Group is not None:
        return resource_container.Group
    if resource_container.GuidanceResponse is not None:
        return resource_container.GuidanceResponse
    if resource_container.HealthcareService is not None:
        return resource_container.HealthcareService
    if resource_container.ImagingStudy is not None:
        return resource_container.ImagingStudy
    if resource_container.Immunization is not None:
        return resource_container.Immunization
    if resource_container.ImmunizationEvaluation is not None:
        return resource_container.ImmunizationEvaluation
    if resource_container.ImmunizationRecommendation is not None:
        return resource_container.ImmunizationRecommendation
    if resource_container.ImplementationGuide is not None:
        return resource_container.ImplementationGuide
    if resource_container.Ingredient is not None:
        return resource_container.Ingredient
    if resource_container.InsurancePlan is not None:
        return resource_container.InsurancePlan
    if resource_container.Invoice is not None:
        return resource_container.Invoice
    if resource_container.Library is not None:
        return resource_container.Library
    if resource_container.Linkage is not None:
        return resource_container.Linkage
    if resource_container.List is not None:
        return resource_container.List
    if resource_container.Location is not None:
        return resource_container.Location
    if resource_container.ManufacturedItemDefinition is not None:
        return resource_container.ManufacturedItemDefinition
    if resource_container.Measure is not None:
        return resource_container.Measure
    if resource_container.MeasureReport is not None:
        return resource_container.MeasureReport
    if resource_container.Media is not None:
        return resource_container.Media
    if resource_container.Medication is not None:
        return resource_container.Medication
    if resource_container.MedicationAdministration is not None:
        return resource_container.MedicationAdministration
    if resource_container.MedicationDispense is not None:
        return resource_container.MedicationDispense
    if resource_container.MedicationKnowledge is not None:
        return resource_container.MedicationKnowledge
    if resource_container.MedicationRequest is not None:
        return resource_container.MedicationRequest
    if resource_container.MedicationStatement is not None:
        return resource_container.MedicationStatement
    if resource_container.MedicinalProductDefinition is not None:
        return resource_container.MedicinalProductDefinition
    if resource_container.MessageDefinition is not None:
        return resource_container.MessageDefinition
    if resource_container.MessageHeader is not None:
        return resource_container.MessageHeader
    if resource_container.MolecularSequence is not None:
        return resource_container.MolecularSequence
    if resource_container.NamingSystem is not None:
        return resource_container.NamingSystem
    if resource_container.NutritionOrder is not None:
        return resource_container.NutritionOrder
    if resource_container.NutritionProduct is not None:
        return resource_container.NutritionProduct
    if resource_container.Observation is not None:
        return resource_container.Observation
    if resource_container.ObservationDefinition is not None:
        return resource_container.ObservationDefinition
    if resource_container.OperationDefinition is not None:
        return resource_container.OperationDefinition
    if resource_container.OperationOutcome is not None:
        return resource_container.OperationOutcome
    if resource_container.Organization is not None:
        return resource_container.Organization
    if resource_container.OrganizationAffiliation is not None:
        return resource_container.OrganizationAffiliation
    if resource_container.PackagedProductDefinition is not None:
        return resource_container.PackagedProductDefinition
    if resource_container.Patient is not None:
        return resource_container.Patient
    if resource_container.PaymentNotice is not None:
        return resource_container.PaymentNotice
    if resource_container.PaymentReconciliation is not None:
        return resource_container.PaymentReconciliation
    if resource_container.Person is not None:
        return resource_container.Person
    if resource_container.PlanDefinition is not None:
        return resource_container.PlanDefinition
    if resource_container.Practitioner is not None:
        return resource_container.Practitioner
    if resource_container.PractitionerRole is not None:
        return resource_container.PractitionerRole
    if resource_container.Procedure is not None:
        return resource_container.Procedure
    if resource_container.Provenance is not None:
        return resource_container.Provenance
    if resource_container.Questionnaire is not None:
        return resource_container.Questionnaire
    if resource_container.QuestionnaireResponse is not None:
        return resource_container.QuestionnaireResponse
    if resource_container.RegulatedAuthorization is not None:
        return resource_container.RegulatedAuthorization
    if resource_container.RelatedPerson is not None:
        return resource_container.RelatedPerson
    if resource_container.RequestGroup is not None:
        return resource_container.RequestGroup
    if resource_container.ResearchDefinition is not None:
        return resource_container.ResearchDefinition
    if resource_container.ResearchElementDefinition is not None:
        return resource_container.ResearchElementDefinition
    if resource_container.ResearchStudy is not None:
        return resource_container.ResearchStudy
    if resource_container.ResearchSubject is not None:
        return resource_container.ResearchSubject
    if resource_container.RiskAssessment is not None:
        return resource_container.RiskAssessment
    if resource_container.Schedule is not None:
        return resource_container.Schedule
    if resource_container.SearchParameter is not None:
        return resource_container.SearchParameter
    if resource_container.ServiceRequest is not None:
        return resource_container.ServiceRequest
    if resource_container.Slot is not None:
        return resource_container.Slot
    if resource_container.Specimen is not None:
        return resource_container.Specimen
    if resource_container.SpecimenDefinition is not None:
        return resource_container.SpecimenDefinition
    if resource_container.StructureDefinition is not None:
        return resource_container.StructureDefinition
    if resource_container.StructureMap is not None:
        return resource_container.StructureMap
    if resource_container.Subscription is not None:
        return resource_container.Subscription
    if resource_container.SubscriptionStatus is not None:
        return resource_container.SubscriptionStatus
    if resource_container.SubscriptionTopic is not None:
        return resource_container.SubscriptionTopic
    if resource_container.Substance is not None:
        return resource_container.Substance
    if resource_container.SubstanceDefinition is not None:
        return resource_container.SubstanceDefinition
    if resource_container.SupplyDelivery is not None:
        return resource_container.SupplyDelivery
    if resource_container.SupplyRequest is not None:
        return resource_container.SupplyRequest
    if resource_container.Task is not None:
        return resource_container.Task
    if resource_container.TerminologyCapabilities is not None:
        return resource_container.TerminologyCapabilities
    if resource_container.TestReport is not None:
        return resource_container.TestReport
    if resource_container.TestScript is not None:
        return resource_container.TestScript
    if resource_container.ValueSet is not None:
        return resource_container.ValueSet
    if resource_container.VerificationResult is not None:
        return resource_container.VerificationResult
    if resource_container.VisionPrescription is not None:
        return resource_container.VisionPrescription
    if resource_container.Parameters is not None:
        return resource_container.Parameters
    return None

default_types_maps = {
    (cda_austrian_extension.II, fhir4.Identifier): II,
    (cda_austrian_extension.INT, fhir4.integer): INT,
    (cda_austrian_extension.BL, fhir4.boolean): BL,
    (cda_austrian_extension.TS, fhir4.instant): TSInstant,
    (cda_austrian_extension.IVL_TS, fhir4.instant): IVL_TSDateTime,
    (cda_austrian_extension.TS, fhir4.dateTime): TSDateTime,
    (cda_austrian_extension.IVXB_TS, fhir4.dateTime): IVXB_TSDateTime,
    (cda_austrian_extension.TS, fhir4.date): TSDate,
    (cda_austrian_extension.IVL_TS, fhir4.Period): IVLTSPeriod,
    (cda_austrian_extension.IVL_TS, fhir4.dateTime): IVLTSDateTime,
    (cda_austrian_extension.ST, fhir4.string): STstring,
    (cda_austrian_extension.ED, fhir4.string): EDstring,
    (cda_austrian_extension.EN, fhir4.string): ENstring,
    (cda_austrian_extension.ON, fhir4.string): ONstring,
    (cda_austrian_extension.CS, fhir4.code): CSCode,
    (cda_austrian_extension.CE, fhir4.code): CECode,
    (cda_austrian_extension.CD, fhir4.code): CDCode,
    (cda_austrian_extension.CE, fhir4.Coding): CECoding,
    (cda_austrian_extension.CD, fhir4.Coding): CDCoding,
    (cda_austrian_extension.CE, fhir4.CodeableConcept): CECodeableConcept,
    (cda_austrian_extension.CS, fhir4.CodeableConcept): CSCodeableConcept,
    (cda_austrian_extension.CD, fhir4.CodeableConcept): CDCodeableConcept,
    (cda_austrian_extension.EN, fhir4.HumanName): ENHumanName,
    (cda_austrian_extension.PN, fhir4.HumanName): CdaPersonNameCompilationToFhirHumanName,
    (cda_austrian_extension.AD, fhir4.Address): CdaAdressCompilationToFhirAustrianAddress,
    (cda_austrian_extension.TEL, fhir4.ContactPoint): TELContactPoint,
    (cda_austrian_extension.PQ, fhir4.Quantity): PQQuantity,
    (cda_austrian_extension.IVXB_PQ, fhir4.Quantity): IVXB_PQQuantity,
    (cda_austrian_extension.RTO_PQ_PQ, fhir4.Ratio): RTOPQPQRatio,
}

def transform_default(source, target, target_type=None):
    target_type = target_type or type(target)
    default_map = default_types_maps.get((type(source), target_type))
    if default_map:
        default_map(source, target)
    else:
        raise BaseException('No default transform found for %s -> %s' % (type(source), target_type))

if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    transform(args.source, args.target)
