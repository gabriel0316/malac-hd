import sys
import argparse
import time
import uuid
import re
import dateutil.parser
from html import escape as html_escape
import fhir4
from fhir4 import string, base64Binary, markdown, code, dateTime, uri, boolean, decimal
from fhirpath import utils as fp_utils
from datetime import datetime

description_text = "This has been compiled by the MApping LAnguage Compiler for Health Data, short MaLaC-HD. See arguments for more details."
one_timestamp = datetime.now()
utils = fp_utils.FHIRPathUtils(fhir4)

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description_text)
    parser.add_argument(
       '-s', '--source', help='the source file path', required=True
    )
    parser.add_argument(
       '-t', '--target', help='the target file path the result will be written to', required=True
    )
    return parser

def transform(source_path, target_path):
    start = time.time()
    print('+++++++ Transformation from '+source_path+' to '+target_path+' started +++++++')

    practitioner = fhir4.parse(source_path, silence=True)
    patient = fhir4.Patient()
    PractitionerToPatient(practitioner, patient)
    with open(target_path, 'w', newline='', encoding='utf-8') as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        patient.export(f, 0, namespacedef_='xmlns="http://hl7.org/fhir" xmlns:v3="urn:hl7-org:v3"')

    print('altogether in '+str(round(time.time()-start,3))+' seconds.')
    print('+++++++ Transformation from '+source_path+' to '+target_path+' ended  +++++++')

def PractitionerToPatient(practitioner, patient):
    for identifier in practitioner.identifier or []:
        patient.identifier.append(identifier)
    for identifier in practitioner.identifier or []:
        r = identifier.system
        if r:
            if next(iter([bool([v2 for v1 in [practitioner] for v2 in utils.get(v1,'identifier')])]), None):
                trans_out = translate(url='multiple-returns', code=(r if isinstance(r, str) else r.value), silent=False)
                matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                match = fhir4.CodeableConcept(coding=[fhir4.Coding(system=fhir4.uri(value=match['system']), version=fhir4.string(value=match['version']), code=fhir4.string(value=match['code']), display=fhir4.string(value=match['display']), userSelected=fhir4.string(value=match['userSelected'])) for match in matches])
                patient.maritalStatus = match
        for identifier in practitioner.identifier or []:
            r = identifier.system
            if r:
                if next(iter([bool([v2 for v1 in [practitioner] for v2 in utils.get(v1,'identifier')])]), None):
                    maritalStatus = fhir4.CodeableConcept()
                    if patient.maritalStatus is not None:
                        maritalStatus = patient.maritalStatus
                    else:
                        patient.maritalStatus = maritalStatus
                    trans_out = translate(url='multiple-returns', code=(r if isinstance(r, str) else r.value), silent=False)
                    matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
                    # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
                    if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
                    match = fhir4.Coding(system=fhir4.uri(value=matches[0]['system']), version=fhir4.string(value=matches[0]['version']), code=fhir4.string(value=matches[0]['code']), display=fhir4.string(value=matches[0]['display']), userSelected=fhir4.string(value=matches[0]['userSelected']))
                    maritalStatus.coding.append(match)
            for name in practitioner.name or []:
                for given in name.given or []:
                    patient_name = fhir4.HumanName()
                    patient.name.append(patient_name)
                    patient_name.given.append(given)
                for telecom in practitioner.telecom or []:
                    patient.telecom.append(telecom)
                birthDate = practitioner.birthDate
                if birthDate:
                    patient.birthDate = birthDate

# output
# 1..1 result (boolean)
# 0..1 message with error details for human (string)
# 0..* match with (list)
#   0..1 equivalnce (string from https://hl7.org/fhir/R4B/valueset-concept-map-equivalence.html)
#   0..1 concept
#       0..1 system
#       0..1 version
#       0..1 code
#       0..1 display 
#       0..1 userSelected will always be false, because this is a translation
#   0..1 source (conceptMap url)
# TODO implement reverse
def translate(url=None, conceptMapVersion=None, code=None, system=None, version=None, source=None, coding=None, codeableConcept=None, target=None, targetsystem=None, reverse=None, silent=False)              -> dict [bool, str, list[dict[str, dict[str, str, str, str, bool], str]]]:
    start = time.time()
    
    # start validation and recall of translate in simple from
    if codeableConcept:
        if isinstance(codeableConcept, str): 
            codeableConcept = fhir4.parseString(codeableConcept, silent)
        elif isinstance(coding, fhir4.CodeableConcept):
            pass
        else:
            sys.exit("The codeableConcept parameter has to be a string or a CodeableConcept Object (called method as library)!")
        # the first fit will be returned, else the last unfitted value will be returned
        # TODO check translate params
        for one_coding in codeableConcept.get_coding:
            if (ret := translate(url=url, source=source, coding=one_coding, 
                                 target=target, targetsystem=targetsystem, 
                                 reverse=reverse, silent=True))[0]:
                return ret
        else: return ret
        
    elif coding:
        if isinstance(coding, str): 
            coding = fhir4.parseString(coding, silent)
        elif isinstance(coding, fhir4.Coding):
            pass
        else:
            sys.exit("The coding parameter has to be a string or a Coding Object (called method as library)!")
        # TODO check translate params
        return translate(url=url,  source=source, coding=one_coding, 
                         target=target, targetsystem=targetsystem, 
                         reverse=reverse, silent=True)
        
    elif code:
        if not isinstance(code,str): 
            sys.exit("The code parameter has to be a string!")
        
    elif target:
        if not isinstance(code,str): 
            sys.exit("The target parameter has to be a string!")
        
    elif targetsystem:
        if not isinstance(code,str): 
            sys.exit("The targetsystem parameter has to be a string!")
        
    else:
        sys.exit("At least codeableConcept, coding, code, target or targetSystem has to be given!")
    # end validation and recall of translate in simplier from

    # look for any information from the one ore more generated conceptMaps into con_map_7d
    match = []
    unmapped = []
    if url not in con_map_7d.keys():
        print('   #ERROR# ConceptMap with URL "'+ url +'" is not loaded to this compiled conceptMap #ERROR#')
    else:
        for url_lvl in con_map_7d:
            if url_lvl == "%" or url_lvl == str(url or ""):#+str(("/?version=" and conceptMapVersion) or ""):
                for source_lvl in con_map_7d[url_lvl]:
                    if source_lvl == "%" or not source or source_lvl == source:
                        for target_lvl in con_map_7d[url_lvl][source_lvl]:
                            if target_lvl == "%" or not target or target_lvl == target:
                                for system_lvl in con_map_7d[url_lvl][source_lvl][target_lvl]:
                                    if system_lvl == "%" or not system or system_lvl == system:#+str(("/?version=" and version) or ""):
                                        for targetsystem_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl]:
                                            if targetsystem_lvl == "%" or not targetsystem or targetsystem_lvl == targetsystem:
                                                for code_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl]:
                                                    if code_lvl == "|" or code_lvl == "~" or code_lvl == "#":
                                                        unmapped += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]
                                                    if code_lvl == "%" or not code or code_lvl == code:
                                                        match += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]                
                                                    
    if not match:
        for one_unmapped in unmapped:
            tmp_system = ""
            tmp_version = ""
            tmp_code = ""
            tmp_display = ""
            # replace all "|" values with to translated code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            if one_unmapped["concept"]["code"].startswith("|"):
                tmp_system = system
                tmp_version = version
                tmp_code = one_unmapped["concept"]["code"][1:] + code
            # replace all "~" values with fixed code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            elif one_unmapped["concept"]["code"].startswith("~"):
                tmp_code = one_unmapped["concept"]["code"][1:]
                tmp_display = one_unmapped["concept"]["display"]
            elif one_unmapped["concept"]["code"].startswith("#"):
                # TODO detect recursion like conceptMapA -> conceptMapB -> ConceptMapA -> ...
                return translate(one_unmapped["concept"]["code"][1:], None, code, system, version, source, 
                                 coding, codeableConcept, target, targetsystem, reverse, silent)
            match.append({"equivalence": one_unmapped["equivalence"], 
                          "concept":{
                            "system": tmp_system, 
                            "version": tmp_version, # TODO version of codesystem out of url?
                            "code": tmp_code,
                            "display": tmp_display,
                            "userSelected": False},
                          "source": one_unmapped["source"]})
                
            
    # see if any match is not "unmatched" or "disjoint"
    result = False
    message = ""
    for one_match in match:
        if one_match["equivalence"] != "unmatched" and one_match["equivalence"] != "disjoint":
            result = True 

    if not silent:
        print('Translation in '+str(round(time.time()-start,3))+' seconds for code "'+code+'" with ConceptMap "'+url+'"')
    return {"result": result, "message": message, "match": match}

# The con_map_7d is a seven dimensional dictionary, for quickly finding the fitting translation
# All dimensions except the last are optional, so a explicit NONE value will be used as key and 
# interpreted as the default key, that always will be fitting, no matter what other keys are fitting.
# If a version is included (purely optional), than the version will be added with a blank before to the key
#
# The 0th dimension is mandatory and stating the ConceptMap with its url (including the version).
#
# The 1st dimension is optional and stating the SOURCE valueset (including the version), as one conceptMap can only 
# have a maximum of one SOURCE, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 2nd dimension is optional and stating the TARGET valueset (including the version), as one conceptMap can only 
# have a maximum of one TARGET, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 3th dimension is optional and stating the SYSTEM (including the version) from the source valueset code, as one 
# code could be used in multiple SYSTEMs from the source valueset to translate. 
# Not stating a SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this SYSTEM element as stating the valueset, that should be
# stated in source, this case will still be supported by MaLaC-HD. Having a conceptMap with a source valueset 
# and a different SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#
# The 4th dimension is optional and stating the TARGET SYSTEM (including the version) from the target valueset code, as one 
# code could be used in multiple SYSTEMs from the target valueset to translate. 
# Not stating a TARGET SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this TARGET SYSTEM element as stating the target valueset, that should be
# stated in target, this case will still be supported by MaLaC-HD. Having a conceptMap with a target valueset 
# and a different TARGET SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#   
# The 5th dimension is optional and stating the CODE from the source valueset, as one conceptMap can have none or 
# multiple CODEs from the source to translate. 
#
# The 6th dimension is NOT optional and stating the TARGET CODE from the target valueset. As one source code could be translated 
# in multiple TARGET CODEs, the whole set have to be returend. 
# For a translation with explicitly no TARGET CODE, because of an quivalence of unmatched or disjoint, NONE will be returned. 
#   
# a minimal example, translating "hi" to "servus": 
# con_map_7d = {"myConMap": {None: {None: {"hi": {None: {None: ["equivalent", "<coding><code>servus</code></coding>", "https://my.concept.map/conceptMap/my"]}}}}}
#
# TODO add a dimension for a specific dependsOn property
# TODO add a solution for the unmapped element
con_map_7d = {}


con_map_7d["multiple-returns"] = {
    "%": {
        "%": {
            "http://hl7.org/fhir/ValueSet/multiple-returns": {
                "http://hl7.org/fhir/ValueSet/multiple-returns": {
                    "http://www.acme.org/practitioners": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/ValueSet/multiple-returns",
                                "version": "",
                                "code": "http://www.i.transformer/",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "multiple-returns"
                        }
                    ]
                }
            }
        }
    }
}
def unpack_container(resource_container):
    if resource_container.Account is not None:
        return resource_container.Account
    if resource_container.ActivityDefinition is not None:
        return resource_container.ActivityDefinition
    if resource_container.AdministrableProductDefinition is not None:
        return resource_container.AdministrableProductDefinition
    if resource_container.AdverseEvent is not None:
        return resource_container.AdverseEvent
    if resource_container.AllergyIntolerance is not None:
        return resource_container.AllergyIntolerance
    if resource_container.Appointment is not None:
        return resource_container.Appointment
    if resource_container.AppointmentResponse is not None:
        return resource_container.AppointmentResponse
    if resource_container.AuditEvent is not None:
        return resource_container.AuditEvent
    if resource_container.Basic is not None:
        return resource_container.Basic
    if resource_container.Binary is not None:
        return resource_container.Binary
    if resource_container.BiologicallyDerivedProduct is not None:
        return resource_container.BiologicallyDerivedProduct
    if resource_container.BodyStructure is not None:
        return resource_container.BodyStructure
    if resource_container.Bundle is not None:
        return resource_container.Bundle
    if resource_container.CapabilityStatement is not None:
        return resource_container.CapabilityStatement
    if resource_container.CarePlan is not None:
        return resource_container.CarePlan
    if resource_container.CareTeam is not None:
        return resource_container.CareTeam
    if resource_container.CatalogEntry is not None:
        return resource_container.CatalogEntry
    if resource_container.ChargeItem is not None:
        return resource_container.ChargeItem
    if resource_container.ChargeItemDefinition is not None:
        return resource_container.ChargeItemDefinition
    if resource_container.Citation is not None:
        return resource_container.Citation
    if resource_container.Claim is not None:
        return resource_container.Claim
    if resource_container.ClaimResponse is not None:
        return resource_container.ClaimResponse
    if resource_container.ClinicalImpression is not None:
        return resource_container.ClinicalImpression
    if resource_container.ClinicalUseDefinition is not None:
        return resource_container.ClinicalUseDefinition
    if resource_container.CodeSystem is not None:
        return resource_container.CodeSystem
    if resource_container.Communication is not None:
        return resource_container.Communication
    if resource_container.CommunicationRequest is not None:
        return resource_container.CommunicationRequest
    if resource_container.CompartmentDefinition is not None:
        return resource_container.CompartmentDefinition
    if resource_container.Composition is not None:
        return resource_container.Composition
    if resource_container.ConceptMap is not None:
        return resource_container.ConceptMap
    if resource_container.Condition is not None:
        return resource_container.Condition
    if resource_container.Consent is not None:
        return resource_container.Consent
    if resource_container.Contract is not None:
        return resource_container.Contract
    if resource_container.Coverage is not None:
        return resource_container.Coverage
    if resource_container.CoverageEligibilityRequest is not None:
        return resource_container.CoverageEligibilityRequest
    if resource_container.CoverageEligibilityResponse is not None:
        return resource_container.CoverageEligibilityResponse
    if resource_container.DetectedIssue is not None:
        return resource_container.DetectedIssue
    if resource_container.Device is not None:
        return resource_container.Device
    if resource_container.DeviceDefinition is not None:
        return resource_container.DeviceDefinition
    if resource_container.DeviceMetric is not None:
        return resource_container.DeviceMetric
    if resource_container.DeviceRequest is not None:
        return resource_container.DeviceRequest
    if resource_container.DeviceUseStatement is not None:
        return resource_container.DeviceUseStatement
    if resource_container.DiagnosticReport is not None:
        return resource_container.DiagnosticReport
    if resource_container.DocumentManifest is not None:
        return resource_container.DocumentManifest
    if resource_container.DocumentReference is not None:
        return resource_container.DocumentReference
    if resource_container.Encounter is not None:
        return resource_container.Encounter
    if resource_container.Endpoint is not None:
        return resource_container.Endpoint
    if resource_container.EnrollmentRequest is not None:
        return resource_container.EnrollmentRequest
    if resource_container.EnrollmentResponse is not None:
        return resource_container.EnrollmentResponse
    if resource_container.EpisodeOfCare is not None:
        return resource_container.EpisodeOfCare
    if resource_container.EventDefinition is not None:
        return resource_container.EventDefinition
    if resource_container.Evidence is not None:
        return resource_container.Evidence
    if resource_container.EvidenceReport is not None:
        return resource_container.EvidenceReport
    if resource_container.EvidenceVariable is not None:
        return resource_container.EvidenceVariable
    if resource_container.ExampleScenario is not None:
        return resource_container.ExampleScenario
    if resource_container.ExplanationOfBenefit is not None:
        return resource_container.ExplanationOfBenefit
    if resource_container.FamilyMemberHistory is not None:
        return resource_container.FamilyMemberHistory
    if resource_container.Flag is not None:
        return resource_container.Flag
    if resource_container.Goal is not None:
        return resource_container.Goal
    if resource_container.GraphDefinition is not None:
        return resource_container.GraphDefinition
    if resource_container.Group is not None:
        return resource_container.Group
    if resource_container.GuidanceResponse is not None:
        return resource_container.GuidanceResponse
    if resource_container.HealthcareService is not None:
        return resource_container.HealthcareService
    if resource_container.ImagingStudy is not None:
        return resource_container.ImagingStudy
    if resource_container.Immunization is not None:
        return resource_container.Immunization
    if resource_container.ImmunizationEvaluation is not None:
        return resource_container.ImmunizationEvaluation
    if resource_container.ImmunizationRecommendation is not None:
        return resource_container.ImmunizationRecommendation
    if resource_container.ImplementationGuide is not None:
        return resource_container.ImplementationGuide
    if resource_container.Ingredient is not None:
        return resource_container.Ingredient
    if resource_container.InsurancePlan is not None:
        return resource_container.InsurancePlan
    if resource_container.Invoice is not None:
        return resource_container.Invoice
    if resource_container.Library is not None:
        return resource_container.Library
    if resource_container.Linkage is not None:
        return resource_container.Linkage
    if resource_container.List is not None:
        return resource_container.List
    if resource_container.Location is not None:
        return resource_container.Location
    if resource_container.ManufacturedItemDefinition is not None:
        return resource_container.ManufacturedItemDefinition
    if resource_container.Measure is not None:
        return resource_container.Measure
    if resource_container.MeasureReport is not None:
        return resource_container.MeasureReport
    if resource_container.Media is not None:
        return resource_container.Media
    if resource_container.Medication is not None:
        return resource_container.Medication
    if resource_container.MedicationAdministration is not None:
        return resource_container.MedicationAdministration
    if resource_container.MedicationDispense is not None:
        return resource_container.MedicationDispense
    if resource_container.MedicationKnowledge is not None:
        return resource_container.MedicationKnowledge
    if resource_container.MedicationRequest is not None:
        return resource_container.MedicationRequest
    if resource_container.MedicationStatement is not None:
        return resource_container.MedicationStatement
    if resource_container.MedicinalProductDefinition is not None:
        return resource_container.MedicinalProductDefinition
    if resource_container.MessageDefinition is not None:
        return resource_container.MessageDefinition
    if resource_container.MessageHeader is not None:
        return resource_container.MessageHeader
    if resource_container.MolecularSequence is not None:
        return resource_container.MolecularSequence
    if resource_container.NamingSystem is not None:
        return resource_container.NamingSystem
    if resource_container.NutritionOrder is not None:
        return resource_container.NutritionOrder
    if resource_container.NutritionProduct is not None:
        return resource_container.NutritionProduct
    if resource_container.Observation is not None:
        return resource_container.Observation
    if resource_container.ObservationDefinition is not None:
        return resource_container.ObservationDefinition
    if resource_container.OperationDefinition is not None:
        return resource_container.OperationDefinition
    if resource_container.OperationOutcome is not None:
        return resource_container.OperationOutcome
    if resource_container.Organization is not None:
        return resource_container.Organization
    if resource_container.OrganizationAffiliation is not None:
        return resource_container.OrganizationAffiliation
    if resource_container.PackagedProductDefinition is not None:
        return resource_container.PackagedProductDefinition
    if resource_container.Patient is not None:
        return resource_container.Patient
    if resource_container.PaymentNotice is not None:
        return resource_container.PaymentNotice
    if resource_container.PaymentReconciliation is not None:
        return resource_container.PaymentReconciliation
    if resource_container.Person is not None:
        return resource_container.Person
    if resource_container.PlanDefinition is not None:
        return resource_container.PlanDefinition
    if resource_container.Practitioner is not None:
        return resource_container.Practitioner
    if resource_container.PractitionerRole is not None:
        return resource_container.PractitionerRole
    if resource_container.Procedure is not None:
        return resource_container.Procedure
    if resource_container.Provenance is not None:
        return resource_container.Provenance
    if resource_container.Questionnaire is not None:
        return resource_container.Questionnaire
    if resource_container.QuestionnaireResponse is not None:
        return resource_container.QuestionnaireResponse
    if resource_container.RegulatedAuthorization is not None:
        return resource_container.RegulatedAuthorization
    if resource_container.RelatedPerson is not None:
        return resource_container.RelatedPerson
    if resource_container.RequestGroup is not None:
        return resource_container.RequestGroup
    if resource_container.ResearchDefinition is not None:
        return resource_container.ResearchDefinition
    if resource_container.ResearchElementDefinition is not None:
        return resource_container.ResearchElementDefinition
    if resource_container.ResearchStudy is not None:
        return resource_container.ResearchStudy
    if resource_container.ResearchSubject is not None:
        return resource_container.ResearchSubject
    if resource_container.RiskAssessment is not None:
        return resource_container.RiskAssessment
    if resource_container.Schedule is not None:
        return resource_container.Schedule
    if resource_container.SearchParameter is not None:
        return resource_container.SearchParameter
    if resource_container.ServiceRequest is not None:
        return resource_container.ServiceRequest
    if resource_container.Slot is not None:
        return resource_container.Slot
    if resource_container.Specimen is not None:
        return resource_container.Specimen
    if resource_container.SpecimenDefinition is not None:
        return resource_container.SpecimenDefinition
    if resource_container.StructureDefinition is not None:
        return resource_container.StructureDefinition
    if resource_container.StructureMap is not None:
        return resource_container.StructureMap
    if resource_container.Subscription is not None:
        return resource_container.Subscription
    if resource_container.SubscriptionStatus is not None:
        return resource_container.SubscriptionStatus
    if resource_container.SubscriptionTopic is not None:
        return resource_container.SubscriptionTopic
    if resource_container.Substance is not None:
        return resource_container.Substance
    if resource_container.SubstanceDefinition is not None:
        return resource_container.SubstanceDefinition
    if resource_container.SupplyDelivery is not None:
        return resource_container.SupplyDelivery
    if resource_container.SupplyRequest is not None:
        return resource_container.SupplyRequest
    if resource_container.Task is not None:
        return resource_container.Task
    if resource_container.TerminologyCapabilities is not None:
        return resource_container.TerminologyCapabilities
    if resource_container.TestReport is not None:
        return resource_container.TestReport
    if resource_container.TestScript is not None:
        return resource_container.TestScript
    if resource_container.ValueSet is not None:
        return resource_container.ValueSet
    if resource_container.VerificationResult is not None:
        return resource_container.VerificationResult
    if resource_container.VisionPrescription is not None:
        return resource_container.VisionPrescription
    if resource_container.Parameters is not None:
        return resource_container.Parameters
    return None

def _handle_tr(tr):
    html = ""
    attrs = ""
    if tr.ID:
        attrs += ' id="%s"' % tr.ID
    if tr.styleCode:
        attrs += f' class="{tr.styleCode}"'
    html += "<tr %s>" % attrs
    for th in tr.th:
        html += _handle_tdh(th, "th")
    for td in tr.td:
        html += _handle_tdh(td, "td")
    html += "</tr>"
    return html

def _handle_tdh(tdh, tag):
    html = ""
    attrs = ""
    if tdh.ID:
        attrs += ' id="%s"' % tdh.ID
    if tdh.styleCode:
        attrs += f' class="{tdh.styleCode}"'
    html += f"<{tag}%s>" % attrs
    html += _handle_content(tdh)
    html += f"</{tag}>"
    return html

def _handle_content(elem):
    html = ""
    for content in elem.content_:
        if content.category == 1:
            html += html_escape(content.value)
        elif content.category == 2:
            if content.name == "br":
                html += f'<br />'
            else:
                html += f"[[unhandled simple content {content.name}]]"
        elif content.category == 3:
            attrs = ""
            if getattr(content.value, "ID", None):
                attrs += ' id="%s"' % content.value.ID 
            if content.name == "table":
                html += "<table%s><thead>" % attrs
                for tr in content.value.thead.tr:
                    html += _handle_tr(tr)
                html += "</thead>"
                if content.value.tfoot:
                    html += "<tfoot>"
                    for tr in content.value.tfoot.tr:
                        html += _handle_tr(tr)
                    html += "</tfoot>"
                html += "<tbody>"
                for tr in content.value.tbody[0].tr:
                    html += _handle_tr(tr)
                html += "</tbody></table>"
            elif content.name == "content":
                if content.value.styleCode == "Italics":
                    attrs += ' class="italics"'
                elif content.value.styleCode:
                    attrs += f' class="{content.value.styleCode}"'
                html += "<span%s>" % attrs
                html += _handle_content(content.value)
                html += "</span>"
            elif content.name == "paragraph":
                if content.value.styleCode:
                    attrs += f' class="{content.value.styleCode}"'
                html += "<p%s>" % attrs
                html += _handle_content(content.value)
                html += "</p>"
            elif content.name == "sup":
                html += "<sup%s>" % attrs
                html += _handle_content(content.value)
                html += "</sup>"
            elif content.name == "footnote":
                html += '<span class="footnote"%s>' % attrs
                html += _handle_content(content.value)
                html += "</span>"
            elif content.name == "renderMultiMedia":
                html += f'<img src="{content.value.referencedObject}"%s />' % attrs
            else:
                html += f"[[unhandled content {content.name}]]"
        else:
            html += f"[[unhandled category {content.category}]]"
    return html

def strucdoctext2html(strucdoctext):
    html = _handle_content(strucdoctext)
    div = fhir4.Flow(content_=[fhir4.MixedContainer(1, 0, "", html)])
    div.original_tagname_ = "div"
    div.valueOf_ = ""
    return div

def ed2html(ed):
    # TODO: improve
    div = fhir4.Flow(content_=[fhir4.MixedContainer(1, 0, "", f"<v3:reference>{ed.reference.value}</v3:reference>")])
    div.original_tagname_ = "div"
    div.valueOf_ = ""
    return div

def ed2markdown(ed):
    # TODO: improve
    return fhir4.markdown(value=f"<v3:reference>{ed.reference.value}</v3:reference>")

def builddiv(content):
    div = fhir4.Flow(content_=[fhir4.MixedContainer(1, 0, "", content)])
    div.original_tagname_ = "div"
    div.valueOf_ = ""
    return div

if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    transform(args.source, args.target)
