
import sys
import argparse
import time
import uuid
import re
import io
import json
from datetime import datetime
import dateutil.parser
from html import escape as html_escape
import utils
import fhir4
import cda_austrian_extension
from fhir4 import string, base64Binary, markdown, code, dateTime, uri, boolean, decimal

fhir4.Resource.exportJsonAttributes = utils.exportJsonAttributesResource
fhir4.ResourceContainer.exportJsonResult = utils.exportJsonResultResourceContainer
fhir4.Narrative.exportJsonResult = utils.exportJsonResultNarrative
fhir4.Element.exportJsonResult = utils.exportJsonResultElement

description_text = "This has been compiled by the MApping LAnguage Compiler for Health Data, short MaLaC-HD. See arguments for more details."
one_timestamp = datetime(2000,1,1)
fhirpath_utils = utils.FHIRPathUtils(fhir4)
shared_vars = {}

curr_uuid = 0
def dummy_uuid():
  global curr_uuid
  curr_uuid += 1
  return str(uuid.uuid3(uuid.NAMESPACE_URL, str(curr_uuid)))

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description_text)
    parser.add_argument(
       '-s', '--source', help='the source file path', required=True
    )
    parser.add_argument(
       '-t', '--target', help='the target file path the result will be written to', required=True
    )
    return parser

def transform(source_path, target_path):
    start = time.time()
    print('+++++++ Transformation from '+source_path+' to '+target_path+' started +++++++')

    if source_path.endswith('.xml'):
        bundle = fhir4.parse(source_path, silence=True)
    elif source_path.endswith('.json'):
        with open(source_path, 'r', newline='', encoding='utf-8') as f:
            bundle = utils.parse_json(fhir4, json.load(f))
    else:
        raise BaseException('Unknown source file ending')
    cda = cda_austrian_extension.POCD_MT000040_ClinicalDocument()
    BundleToCda(bundle, cda)
    with open(target_path, 'w', newline='', encoding='utf-8') as f:
        if target_path.endswith('.xml'):
            f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            cda.export(f, 0, namespacedef_='xmlns="http://hl7.org/fhir" xmlns:v3="urn:hl7-org:v3"')
        else:
            raise BaseException('Unknown target file ending')

    print('altogether in '+str(round(time.time()-start,3))+' seconds.')
    print('+++++++ Transformation from '+source_path+' to '+target_path+' ended  +++++++')

def BundleToCda(bundle, cda):
    recordTarget = cda_austrian_extension.POCD_MT000040_RecordTarget()
    cda.recordTarget.append(recordTarget)
    author = cda_austrian_extension.POCD_MT000040_Author()
    cda.author.append(author)
    BundleToClinicalDocument(bundle, recordTarget, author, cda)

def BundleToClinicalDocument(bundle, recordTarget, author, cda):
    typeId = cda_austrian_extension.POCD_MT000040_InfrastructureRoot_typeId()
    if cda.typeId is not None:
        typeId = cda.typeId
    else:
        cda.typeId = typeId
    typeId.root = '2.16.840.1.113883.1.3'
    typeId.extension = 'POCD_HD000040'
    identifier = bundle.identifier
    if identifier:
        id = cda_austrian_extension.II()
        if cda.id is not None:
            id = cda.id
        else:
            cda.id = id
        IdentifierII(identifier, id)
    timestamp = bundle.timestamp
    if timestamp:
        effectiveTime = cda_austrian_extension.TS()
        if cda.effectiveTime is not None:
            effectiveTime = cda.effectiveTime
        else:
            cda.effectiveTime = effectiveTime
        InstantTS(timestamp, effectiveTime)
    for entry in bundle.entry or []:
        resource = unpack_container(entry.resource)
        if utils.single([v3 for v3 in [resource] if fhirpath_utils.is_type(v3, 'FHIR.Composition') == [True]]):
            CompositionClinicalDocument(bundle, resource, recordTarget, author, cda)

def SectionCdaSection(bundle, section, cdasection):
    for extension in section.extension or []:
        if utils.single(fhirpath_utils.equals([v2 for v1 in [extension] for v2 in fhirpath_utils.get(v1,'url')], '==', ['http://fhir.ch/ig/ch-core/StructureDefinition/ch-ext-epr-sectionid'])):
            id = cda_austrian_extension.II()
            if cdasection.id is not None:
                id = cdasection.id
            else:
                cdasection.id = id
            valueIdentifier = extension.valueIdentifier
            IdentifierII(valueIdentifier, id)
    section = section
    if section:
        if utils.single(fhirpath_utils.equals([bool([v3 for v3 in [v2 for v1 in [section] for v2 in fhirpath_utils.get(v1,'extension')] if fhirpath_utils.equals(fhirpath_utils.get(v3,'url'), '==', ['http://fhir.ch/ig/ch-core/StructureDefinition/ch-ext-epr-sectionid']) == [True]])], '==', [False])):
            id = cda_austrian_extension.II()
            if cdasection.id is not None:
                id = cdasection.id
            else:
                cdasection.id = id
            identifier = bundle.identifier
            if identifier:
                id = cda_austrian_extension.II()
                if cdasection.id is not None:
                    id = cdasection.id
                else:
                    cdasection.id = id
                IdentifierII(identifier, id)
    for code in (section.code if isinstance(section.code, list) else ([] if not section.code else [section.code])):
        for coding in (code.coding if isinstance(code.coding, list) else ([] if not code.coding else [code.coding])):
            if utils.single(fhirpath_utils.equals([v2 for v1 in [coding] for v2 in fhirpath_utils.get(v1,'system')], '==', ['http://loinc.org'])):
                cdacode = cda_austrian_extension.CE()
                if cdasection.code is not None:
                    cdacode = cdasection.code
                else:
                    cdasection.code = cdacode
                CodingCE(coding, cdacode)
    for title in (section.title if isinstance(section.title, list) else ([] if not section.title else [section.title])):
        cdatitle = cda_austrian_extension.ST()
        if cdasection.title is not None:
            cdatitle = cdasection.title
        else:
            cdasection.title = cdatitle
        cdatitle.valueOf_ = title # unknown source type for string
    for text in (section.text if isinstance(section.text, list) else ([] if not section.text else [section.text])):
        for div in (text.div if isinstance(text.div, list) else ([] if not text.div else [text.div])):
            cdasection.text = div

def CompositionClinicalDocument(bundle, composition, recordTarget, author, cda):
    type = composition.type_
    if type:
        for coding in type.coding or []:
            if utils.single(fhirpath_utils.equals([v2 for v1 in [coding] for v2 in fhirpath_utils.get(v1,'system')], '==', ['http://loinc.org'])):
                if 'docCode' in shared_vars:
                    code = shared_vars['docCode']
                else:
                    code = cda_austrian_extension.CE()
                    if cda.code is not None:
                        code = cda.code
                    else:
                        cda.code = code
                    shared_vars['docCode'] = code
                CodingCE(coding, code)
    title = composition.title
    if title:
        t = cda_austrian_extension.ST()
        if cda.title is not None:
            t = cda.title
        else:
            cda.title = t
        t.valueOf_ = title.value
    conf = composition.confidentiality
    if conf:
        cdaconf = cda_austrian_extension.CE()
        if cda.confidentialityCode is not None:
            cdaconf = cda.confidentialityCode
        else:
            cda.confidentialityCode = cdaconf
        cdaconf.code = conf.value
        cdaconf.codeSystem = '2.16.840.1.113883.5.25'
    language = composition.language
    if language:
        languageCode = cda_austrian_extension.CS()
        if cda.languageCode is not None:
            languageCode = cda.languageCode
        else:
            cda.languageCode = languageCode
        languageCode.code = language.value
    for entry in bundle.entry or []:
        if utils.single(fhirpath_utils.bool_and((fhirpath_utils.membership([entry.fullUrl], [v4 for v3 in [v2 for v1 in [composition] for v2 in fhirpath_utils.get(v1,'subject')] for v4 in fhirpath_utils.get(v3,'reference')])), [v6 for v5 in [entry.fullUrl] for v6 in fhirpath_utils.startswith(v5, ['urn:uuid'])])):
            patient = unpack_container(entry.resource)
            if isinstance(patient, fhir4.Patient):
                PatientRecordTarget(patient, bundle, recordTarget)
        patient = unpack_container(entry.resource)
        if utils.single(fhirpath_utils.membership((fhirpath_utils.add(fhirpath_utils.add(['Patient'], ['/']), [v2 for v1 in [patient] for v2 in fhirpath_utils.get(v1,'id')])), [v6 for v5 in [v4 for v3 in [composition] for v4 in fhirpath_utils.get(v3,'subject')] for v6 in fhirpath_utils.get(v5,'reference')])):
            PatientRecordTarget(patient, bundle, recordTarget)
        if utils.single(fhirpath_utils.bool_and((fhirpath_utils.membership([entry.fullUrl], [v4 for v3 in [v2 for v1 in [composition] for v2 in fhirpath_utils.get(v1,'custodian')] for v4 in fhirpath_utils.get(v3,'reference')])), [v6 for v5 in [entry.fullUrl] for v6 in fhirpath_utils.startswith(v5, ['urn:uuid'])])):
            organization = entry.resource
            if organization:
                if isinstance(organization, fhir4.Organization):
                    custodian = cda_austrian_extension.POCD_MT000040_Custodian()
                    if cda.custodian is not None:
                        custodian = cda.custodian
                    else:
                        cda.custodian = custodian
                    OrganizationCustodian(organization, custodian)
        organization = entry.resource
        if organization:
            if utils.single(fhirpath_utils.membership((fhirpath_utils.add(fhirpath_utils.add(['Organization'], ['/']), [v2 for v1 in [organization] for v2 in fhirpath_utils.get(v1,'id')])), [v6 for v5 in [v4 for v3 in [composition] for v4 in fhirpath_utils.get(v3,'custodian')] for v6 in fhirpath_utils.get(v5,'reference')])):
                custodian = cda_austrian_extension.POCD_MT000040_Custodian()
                if cda.custodian is not None:
                    custodian = cda.custodian
                else:
                    cda.custodian = custodian
                OrganizationCustodian(organization, custodian)
        if utils.single(fhirpath_utils.bool_and((fhirpath_utils.membership([entry.fullUrl], [v4 for v3 in [v2 for v1 in [composition] for v2 in fhirpath_utils.get(v1,'author')] for v4 in fhirpath_utils.get(v3,'reference')])), [v6 for v5 in [entry.fullUrl] for v6 in fhirpath_utils.startswith(v5, ['urn:uuid'])])):
            practitioner = unpack_container(entry.resource)
            if isinstance(practitioner, fhir4.Practitioner):
                PractitionerAuthor(bundle, composition, practitioner, author)
        practitioner = unpack_container(entry.resource)
        if utils.single(fhirpath_utils.membership((fhirpath_utils.add(fhirpath_utils.add(['Practitioner'], ['/']), [v2 for v1 in [practitioner] for v2 in fhirpath_utils.get(v1,'id')])), [v6 for v5 in [v4 for v3 in [composition] for v4 in fhirpath_utils.get(v3,'author')] for v6 in fhirpath_utils.get(v5,'reference')])):
            PractitionerAuthor(bundle, composition, practitioner, author)
        if utils.single(fhirpath_utils.bool_and((fhirpath_utils.membership([entry.fullUrl], [v6 for v5 in [v4 for v3 in [v2 for v1 in [composition] for v2 in fhirpath_utils.get(v1,'attester')] for v4 in fhirpath_utils.get(v3,'party')] for v6 in fhirpath_utils.get(v5,'reference')])), [v8 for v7 in [entry.fullUrl] for v8 in fhirpath_utils.startswith(v7, ['urn:uuid'])])):
            practitioner = entry.resource
            if practitioner:
                if isinstance(practitioner, fhir4.Practitioner):
                    legalAuthenticator = cda_austrian_extension.POCD_MT000040_LegalAuthenticator()
                    cda.legalAuthenticator.append(legalAuthenticator)
                    PractitionerLegalAuthenticator(bundle, composition, practitioner, legalAuthenticator)
        practitioner = entry.resource
        if practitioner:
            if utils.single(fhirpath_utils.membership((fhirpath_utils.add(fhirpath_utils.add(['Practitioner'], ['/']), [v2 for v1 in [practitioner] for v2 in fhirpath_utils.get(v1,'id')])), [v8 for v7 in [v6 for v5 in [v4 for v3 in [composition] for v4 in fhirpath_utils.get(v3,'attester')] for v6 in fhirpath_utils.get(v5,'party')] for v8 in fhirpath_utils.get(v7,'reference')])):
                legalAuthenticator = cda_austrian_extension.POCD_MT000040_LegalAuthenticator()
                cda.legalAuthenticator.append(legalAuthenticator)
                PractitionerLegalAuthenticator(bundle, composition, practitioner, legalAuthenticator)
    for section in composition.section or []:
        if utils.single(fhirpath_utils.equals([bool([v2 for v1 in [section] for v2 in fhirpath_utils.get(v1,'code')])], '==', [False])):
            component = cda_austrian_extension.POCD_MT000040_Component2()
            if cda.component is not None:
                component = cda.component
            else:
                cda.component = component
            component.contextConductionInd = True
            structuredBody = cda_austrian_extension.POCD_MT000040_StructuredBody()
            if component.structuredBody is not None:
                structuredBody = component.structuredBody
            else:
                component.structuredBody = structuredBody
            component = cda_austrian_extension.POCD_MT000040_Component3()
            structuredBody.component.append(component)
            cdasection = cda_austrian_extension.POCD_MT000040_Section()
            if component.section is not None:
                cdasection = component.section
            else:
                component.section = cdasection
            SectionCdaSection(bundle, section, cdasection)

def PatientRecordTarget(src, bundle, tgt):
    patientRole = cda_austrian_extension.POCD_MT000040_PatientRole()
    if tgt.patientRole is not None:
        patientRole = tgt.patientRole
    else:
        tgt.patientRole = patientRole
    for identifier in src.identifier or []:
        id = cda_austrian_extension.II()
        patientRole.id.append(id)
        IdentifierII(identifier, id)
    for address in src.address or []:
        ad = cda_austrian_extension.AD()
        patientRole.addr.append(ad)
        AddressAD(address, ad)
    patient = cda_austrian_extension.POCD_MT000040_Patient()
    if patientRole.patient is not None:
        patient = patientRole.patient
    else:
        patientRole.patient = patient
    for humanname in src.name or []:
        en = cda_austrian_extension.PN()
        patient.name.append(en)
        HumanNameEN(humanname, en)
    birthDate = src.birthDate
    if birthDate:
        birthTime = cda_austrian_extension.TS()
        if patient.birthTime is not None:
            birthTime = patient.birthTime
        else:
            patient.birthTime = birthTime
        DateTS(birthDate, birthTime)
    v = src.gender
    if v:
        adminGender = cda_austrian_extension.CE()
        if patient.administrativeGenderCode is not None:
            adminGender = patient.administrativeGenderCode
        else:
            patient.administrativeGenderCode = adminGender
        trans_out = translate(url='ttp://hl7.org/fhir/ConceptMap/cm-administrative-gender-v3', code=(v if isinstance(v, str) else v.value), silent=False)
        matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
        # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
        if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
        match = matches[0]['code']
        adminGender.code = match
        adminGender.codeSystem = '2.16.840.1.113883.5.1'
        adminGender.codeSystemName = 'HL7 AdministrativeGender'
        v = v
        if v:
            if utils.single(fhirpath_utils.equals([v], '==', ['male'])):
                adminGender.displayName = 'Male'
        v = v
        if v:
            if utils.single(fhirpath_utils.equals([v], '==', ['female'])):
                adminGender.displayName = 'Female'
        v = v
        if v:
            if utils.single(fhirpath_utils.equals([v], '==', ['other'])):
                adminGender.displayName = 'Undifferentiated'
    for telecom in src.telecom or []:
        tel = cda_austrian_extension.TEL()
        patientRole.telecom.append(tel)
        ContactPointTEL(telecom, tel)
    managingOrganization = src.managingOrganization
    if managingOrganization:
        for entry in bundle.entry or []:
            if utils.single(fhirpath_utils.bool_and((fhirpath_utils.membership([entry.fullUrl], [v2 for v1 in [managingOrganization] for v2 in fhirpath_utils.get(v1,'reference')])), [v4 for v3 in [entry.fullUrl] for v4 in fhirpath_utils.startswith(v3, ['urn:uuid'])])):
                providerOrganization = cda_austrian_extension.POCD_MT000040_Organization()
                if patientRole.providerOrganization is not None:
                    providerOrganization = patientRole.providerOrganization
                else:
                    patientRole.providerOrganization = providerOrganization
                Organization2CdaOrganization(managingOrganization, providerOrganization)
            organization = entry.resource
            if organization:
                if utils.single(fhirpath_utils.membership((fhirpath_utils.add(fhirpath_utils.add(['Organization'], ['/']), [v2 for v1 in [organization] for v2 in fhirpath_utils.get(v1,'id')])), [v4 for v3 in [managingOrganization] for v4 in fhirpath_utils.get(v3,'reference')])):
                    providerOrganization = cda_austrian_extension.POCD_MT000040_Organization()
                    if patientRole.providerOrganization is not None:
                        providerOrganization = patientRole.providerOrganization
                    else:
                        patientRole.providerOrganization = providerOrganization
                    Organization2CdaOrganization(managingOrganization, providerOrganization)

def PractitionerAuthor(bundle, composition, src, tgt):
    for srcauthor in composition.author or []:
        for extensionTime in srcauthor.extension or []:
            if utils.single(fhirpath_utils.equals([v2 for v1 in [extensionTime] for v2 in fhirpath_utils.get(v1,'url')], '==', ['http://fhir.ch/ig/ch-core/StructureDefinition/ch-ext-epr-time'])):
                valueDateTime = extensionTime.valueDateTime
                if valueDateTime:
                    time = cda_austrian_extension.TS()
                    if tgt.time is not None:
                        time = tgt.time
                    else:
                        tgt.time = time
                    DateTimeTS(valueDateTime, time)
    valueDateTime = bundle.timestamp
    if valueDateTime:
        if utils.single([not([v5 for v5 in [v4 for v3 in [v2 for v1 in [composition] for v2 in fhirpath_utils.get(v1,'author')] for v4 in fhirpath_utils.get(v3,'extension')] if fhirpath_utils.equals(fhirpath_utils.get(v5,'url'), '==', ['http://fhir.ch/ig/ch-core/StructureDefinition/ch-ext-epr-time']) == [True]])]):
            time = cda_austrian_extension.TS()
            if tgt.time is not None:
                time = tgt.time
            else:
                tgt.time = time
            DateTimeTS(valueDateTime, time)
    assignedAuthor = cda_austrian_extension.POCD_MT000040_AssignedAuthor()
    if tgt.assignedAuthor is not None:
        assignedAuthor = tgt.assignedAuthor
    else:
        tgt.assignedAuthor = assignedAuthor
    for identifier in src.identifier or []:
        id = cda_austrian_extension.II()
        assignedAuthor.id.append(id)
        IdentifierII(identifier, id)
    if len(src.telecom) > 0:
        telecom = src.telecom[0]
        tel = cda_austrian_extension.TEL()
        assignedAuthor.telecom.append(tel)
        ContactPointTEL(telecom, tel)
    for address in src.address or []:
        ad = cda_austrian_extension.AD()
        assignedAuthor.addr.append(ad)
        AddressAD(address, ad)
    for name in src.name or []:
        assignedPerson = cda_austrian_extension.POCD_MT000040_Person()
        if assignedAuthor.assignedPerson is not None:
            assignedPerson = assignedAuthor.assignedPerson
        else:
            assignedAuthor.assignedPerson = assignedPerson
        en = cda_austrian_extension.PN()
        assignedPerson.name.append(en)
        HumanNameEN(name, en)
    for entry in bundle.entry or []:
        if utils.single(fhirpath_utils.bool_and((fhirpath_utils.membership([entry.fullUrl], [v4 for v3 in [v2 for v1 in [composition] for v2 in fhirpath_utils.get(v1,'author')] for v4 in fhirpath_utils.get(v3,'reference')])), [v6 for v5 in [entry.fullUrl] for v6 in fhirpath_utils.startswith(v5, ['urn:uuid'])])):
            organization = entry.resource
            if organization:
                if isinstance(organization, fhir4.Organization):
                    cdaorganization = cda_austrian_extension.POCD_MT000040_Organization()
                    if assignedAuthor.representedOrganization is not None:
                        cdaorganization = assignedAuthor.representedOrganization
                    else:
                        assignedAuthor.representedOrganization = cdaorganization
                    Organization2CdaOrganization(organization, cdaorganization)
        organization = entry.resource
        if organization:
            if utils.single(fhirpath_utils.membership((fhirpath_utils.add(fhirpath_utils.add(['Organization'], ['/']), [v2 for v1 in [organization] for v2 in fhirpath_utils.get(v1,'id')])), [v6 for v5 in [v4 for v3 in [composition] for v4 in fhirpath_utils.get(v3,'author')] for v6 in fhirpath_utils.get(v5,'reference')])):
                cdaorganization = cda_austrian_extension.POCD_MT000040_Organization()
                if assignedAuthor.representedOrganization is not None:
                    cdaorganization = assignedAuthor.representedOrganization
                else:
                    assignedAuthor.representedOrganization = cdaorganization
                Organization2CdaOrganization(organization, cdaorganization)

def OrganizationCustodian(src, tgt):
    assignedCustodian = cda_austrian_extension.POCD_MT000040_AssignedCustodian()
    if tgt.assignedCustodian is not None:
        assignedCustodian = tgt.assignedCustodian
    else:
        tgt.assignedCustodian = assignedCustodian
    representedCustodianOrganization = cda_austrian_extension.POCD_MT000040_CustodianOrganization()
    if assignedCustodian.representedCustodianOrganization is not None:
        representedCustodianOrganization = assignedCustodian.representedCustodianOrganization
    else:
        assignedCustodian.representedCustodianOrganization = representedCustodianOrganization
    Organization2CdaOrganizationCustodian(src, representedCustodianOrganization)

def Organization2CdaOrganizationCustodian(src, tgt):
    for identifier in src.identifier or []:
        id = cda_austrian_extension.II()
        tgt.id.append(id)
        IdentifierII(identifier, id)
    name = src.name
    if name:
        orgname = cda_austrian_extension.ON()
        tgt.name.append(orgname)
        orgname.other = name
    for address in src.address or []:
        ad = cda_austrian_extension.AD()
        tgt.addr.append(ad)
        AddressAD(address, ad)
    if len(src.telecom) > 0:
        telecom = src.telecom[0]
        tel = cda_austrian_extension.TEL()
        tgt.telecom.append(tel)
        ContactPointTEL(telecom, tel)

def Organization2CdaOrganization(src, tgt):
    for identifier in src.identifier or []:
        id = cda_austrian_extension.II()
        tgt.id.append(id)
        IdentifierII(identifier, id)
    name = src.name
    if name:
        orgname = cda_austrian_extension.ON()
        tgt.name.append(orgname)
        orgname.other = name
    for address in src.address or []:
        ad = cda_austrian_extension.AD()
        tgt.addr.append(ad)
        AddressAD(address, ad)
    for telecom in src.telecom or []:
        tel = cda_austrian_extension.TEL()
        tgt.telecom.append(tel)
        ContactPointTEL(telecom, tel)

def PractitionerLegalAuthenticator(bundle, composition, src, tgt):
    templateId = cda_austrian_extension.II()
    tgt.templateId.append(templateId)
    templateId.root = '2.16.756.5.30.1.1.10.2.5'
    for attester in composition.attester or []:
        srcTime = attester.time
        if srcTime:
            time = cda_austrian_extension.TS()
            if tgt.time is not None:
                time = tgt.time
            else:
                tgt.time = time
            InstantTS(srcTime, time)
    signatureCode = cda_austrian_extension.CS()
    if tgt.signatureCode is not None:
        signatureCode = tgt.signatureCode
    else:
        tgt.signatureCode = signatureCode
    signatureCode.code = 'S'
    assignedEntity = cda_austrian_extension.POCD_MT000040_AssignedEntity()
    if tgt.assignedEntity is not None:
        assignedEntity = tgt.assignedEntity
    else:
        tgt.assignedEntity = assignedEntity
    for identifier in src.identifier or []:
        id = cda_austrian_extension.II()
        assignedEntity.id.append(id)
        IdentifierII(identifier, id)
    for name in src.name or []:
        assignedPerson = cda_austrian_extension.POCD_MT000040_Person()
        if assignedEntity.assignedPerson is not None:
            assignedPerson = assignedEntity.assignedPerson
        else:
            assignedEntity.assignedPerson = assignedPerson
        en = cda_austrian_extension.PN()
        assignedPerson.name.append(en)
        HumanNameEN(name, en)

# output
# 1..1 result (boolean)
# 0..1 message with error details for human (string)
# 0..* match with (list)
#   0..1 equivalnce (string from https://hl7.org/fhir/R4B/valueset-concept-map-equivalence.html)
#   0..1 concept
#       0..1 system
#       0..1 version
#       0..1 code
#       0..1 display 
#       0..1 userSelected will always be false, because this is a translation
#   0..1 source (conceptMap url)
# TODO implement reverse
def translate(url=None, conceptMapVersion=None, code=None, system=None, version=None, source=None, coding=None, codeableConcept=None, target=None, targetsystem=None, reverse=None, silent=False)              -> dict [bool, str, list[dict[str, dict[str, str, str, str, bool], str]]]:
    start = time.time()
    
    # start validation and recall of translate in simple from
    if codeableConcept:
        if isinstance(codeableConcept, str): 
            codeableConcept = fhir4.parseString(codeableConcept, silent)
        elif isinstance(coding, fhir4.CodeableConcept):
            pass
        else:
            sys.exit("The codeableConcept parameter has to be a string or a CodeableConcept Object (called method as library)!")
        # the first fit will be returned, else the last unfitted value will be returned
        # TODO check translate params
        for one_coding in codeableConcept.get_coding:
            if (ret := translate(url=url, source=source, coding=one_coding, 
                                 target=target, targetsystem=targetsystem, 
                                 reverse=reverse, silent=True))[0]:
                return ret
        else: return ret
        
    elif coding:
        if isinstance(coding, str): 
            coding = fhir4.parseString(coding, silent)
        elif isinstance(coding, fhir4.Coding):
            pass
        else:
            sys.exit("The coding parameter has to be a string or a Coding Object (called method as library)!")
        # TODO check translate params
        return translate(url=url,  source=source, coding=one_coding, 
                         target=target, targetsystem=targetsystem, 
                         reverse=reverse, silent=True)
        
    elif code:
        if not isinstance(code,str): 
            sys.exit("The code parameter has to be a string!")
        
    elif target:
        if not isinstance(code,str): 
            sys.exit("The target parameter has to be a string!")
        
    elif targetsystem:
        if not isinstance(code,str): 
            sys.exit("The targetsystem parameter has to be a string!")
        
    else:
        sys.exit("At least codeableConcept, coding, code, target or targetSystem has to be given!")
    # end validation and recall of translate in simplier from

    # look for any information from the one ore more generated conceptMaps into con_map_7d
    match = []
    unmapped = []
    if url not in con_map_7d.keys():
        print('   #ERROR# ConceptMap with URL "'+ url +'" is not loaded to this compiled conceptMap #ERROR#')
    else:
        for url_lvl in con_map_7d:
            if url_lvl == "%" or url_lvl == str(url or ""):#+str(("/?version=" and conceptMapVersion) or ""):
                for source_lvl in con_map_7d[url_lvl]:
                    if source_lvl == "%" or not source or source_lvl == source:
                        for target_lvl in con_map_7d[url_lvl][source_lvl]:
                            if target_lvl == "%" or not target or target_lvl == target:
                                for system_lvl in con_map_7d[url_lvl][source_lvl][target_lvl]:
                                    if system_lvl == "%" or not system or system_lvl == system:#+str(("/?version=" and version) or ""):
                                        for targetsystem_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl]:
                                            if targetsystem_lvl == "%" or not targetsystem or targetsystem_lvl == targetsystem:
                                                for code_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl]:
                                                    if code_lvl == "|" or code_lvl == "~" or code_lvl == "#":
                                                        unmapped += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]
                                                    if code_lvl == "%" or not code or code_lvl == code:
                                                        match += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]                
                                                    
    if not match:
        for one_unmapped in unmapped:
            tmp_system = ""
            tmp_version = ""
            tmp_code = ""
            tmp_display = ""
            # replace all "|" values with to translated code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            if one_unmapped["concept"]["code"].startswith("|"):
                tmp_system = system
                tmp_version = version
                tmp_code = one_unmapped["concept"]["code"][1:] + code
            # replace all "~" values with fixed code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            elif one_unmapped["concept"]["code"].startswith("~"):
                tmp_code = one_unmapped["concept"]["code"][1:]
                tmp_display = one_unmapped["concept"]["display"]
            elif one_unmapped["concept"]["code"].startswith("#"):
                # TODO detect recursion like conceptMapA -> conceptMapB -> ConceptMapA -> ...
                return translate(one_unmapped["concept"]["code"][1:], None, code, system, version, source, 
                                 coding, codeableConcept, target, targetsystem, reverse, silent)
            match.append({"equivalence": one_unmapped["equivalence"], 
                          "concept":{
                            "system": tmp_system, 
                            "version": tmp_version, # TODO version of codesystem out of url?
                            "code": tmp_code,
                            "display": tmp_display,
                            "userSelected": False},
                          "source": one_unmapped["source"]})
                
            
    # see if any match is not "unmatched" or "disjoint"
    result = False
    message = ""
    for one_match in match:
        if one_match["equivalence"] != "unmatched" and one_match["equivalence"] != "disjoint":
            result = True 

    if not silent:
        print('Translation in '+str(round(time.time()-start,3))+' seconds for code "'+code+'" with ConceptMap "'+url+'"')
    return {"result": result, "message": message, "match": match}

# The con_map_7d is a seven dimensional dictionary, for quickly finding the fitting translation
# All dimensions except the last are optional, so a explicit NONE value will be used as key and 
# interpreted as the default key, that always will be fitting, no matter what other keys are fitting.
# If a version is included (purely optional), than the version will be added with a blank before to the key
#
# The 0th dimension is mandatory and stating the ConceptMap with its url (including the version).
#
# The 1st dimension is optional and stating the SOURCE valueset (including the version), as one conceptMap can only 
# have a maximum of one SOURCE, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 2nd dimension is optional and stating the TARGET valueset (including the version), as one conceptMap can only 
# have a maximum of one TARGET, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 3th dimension is optional and stating the SYSTEM (including the version) from the source valueset code, as one 
# code could be used in multiple SYSTEMs from the source valueset to translate. 
# Not stating a SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this SYSTEM element as stating the valueset, that should be
# stated in source, this case will still be supported by MaLaC-HD. Having a conceptMap with a source valueset 
# and a different SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#
# The 4th dimension is optional and stating the TARGET SYSTEM (including the version) from the target valueset code, as one 
# code could be used in multiple SYSTEMs from the target valueset to translate. 
# Not stating a TARGET SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this TARGET SYSTEM element as stating the target valueset, that should be
# stated in target, this case will still be supported by MaLaC-HD. Having a conceptMap with a target valueset 
# and a different TARGET SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#   
# The 5th dimension is optional and stating the CODE from the source valueset, as one conceptMap can have none or 
# multiple CODEs from the source to translate. 
#
# The 6th dimension is NOT optional and stating the TARGET CODE from the target valueset. As one source code could be translated 
# in multiple TARGET CODEs, the whole set have to be returend. 
# For a translation with explicitly no TARGET CODE, because of an quivalence of unmatched or disjoint, NONE will be returned. 
#   
# a minimal example, translating "hi" to "servus": 
# con_map_7d = {"myConMap": {None: {None: {"hi": {None: {None: ["equivalent", "<coding><code>servus</code></coding>", "https://my.concept.map/conceptMap/my"]}}}}}
#
# TODO add a dimension for a specific dependsOn property
# TODO add a solution for the unmapped element
con_map_7d = {}


con_map_7d["uri2oid"] = {
    "%": {
        "%": {
            "http://hl7.org/fhir": {
                "http://hl7.org/cda": {
                    "http://snomed.info/sct": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "2.16.840.1.113883.6.96",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2oid"
                        }
                    ],
                    "http://loinc.org": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "2.16.840.1.113883.6.1",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2oid"
                        }
                    ],
                    "http://hl7.org/fhir/sid/atc": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "2.16.840.1.113883.6.73",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2oid"
                        }
                    ],
                    "http://www.whocc.no/atc": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "2.16.840.1.113883.6.73",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2oid"
                        }
                    ],
                    "http://terminology.hl7.org/CodeSystem/v3-substanceAdminSubstitution": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "2.16.840.1.113883.5.1070",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2oid"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["uri2system"] = {
    "%": {
        "%": {
            "http://hl7.org/fhir": {
                "http://hl7.org/cda": {
                    "http://snomed.info/sct": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "SNOMED CT",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2system"
                        }
                    ],
                    "http://loinc.org": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "LOINC",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2system"
                        }
                    ],
                    "http://hl7.org/fhir/sid/atc": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "ATC WHO",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2system"
                        }
                    ],
                    "http://www.whocc.no/atc": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "ATC WHO",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2system"
                        }
                    ],
                    "http://terminology.hl7.org/CodeSystem/v3-substanceAdminSubstitution": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "HL7 Substance Admin Substitution",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "uri2system"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["oid2system"] = {
    "%": {
        "%": {
            "http://hl7.org/fhir": {
                "http://hl7.org/cda": {
                    "urn:oid:2.51.1.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "GTIN",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "oid2system"
                        }
                    ],
                    "urn:oid:1.3.6.1.4.1.19376.1.9.2.1": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir",
                                "version": "",
                                "code": "IHE Pharmaceutical Advice Status List",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "oid2system"
                        }
                    ]
                }
            }
        }
    }
}

con_map_7d["addressUse"] = {
    "%": {
        "%": {
            "http://hl7.org/fhir/valueset-address-use.html": {
                "http://terminology.hl7.org/ValueSet/v3-AddressUse": {
                    "home": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/valueset-address-use.html",
                                "version": "",
                                "code": "H",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "work": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/valueset-address-use.html",
                                "version": "",
                                "code": "WP",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "temp": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/valueset-address-use.html",
                                "version": "",
                                "code": "TMP",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ],
                    "old": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/valueset-address-use.html",
                                "version": "",
                                "code": "OLD",
                                "display": "",
                                "userSelected": False
                            },
                            "source": "addressUse"
                        }
                    ]
                }
            }
        }
    }
}
def IdentifierII(src, tgt):
    Any(src, tgt)
    value = src.value
    if value:
        if utils.single(fhirpath_utils.bool_and([not([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'system')])], [v6 for v5 in [v4 for v3 in [src] for v4 in fhirpath_utils.get(v3,'value')] for v6 in fhirpath_utils.startswith(v5, ['urn:uuid:'])])):
            tgt.root = utils.single([v4 for v3 in [v2 for v1 in [value] for v2 in fhirpath_utils.substring(v1,[9],[])] for v4 in fhirpath_utils.upper(v3)])
    value = src.value
    if value:
        if utils.single(fhirpath_utils.equals([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'system')], '==', ['urn:ietf:rfc:3986'])):
            tgt.root = utils.single([v4 for v3 in [v2 for v1 in [value] for v2 in fhirpath_utils.substring(v1,[9],[])] for v4 in fhirpath_utils.upper(v3)])
    value = src.value
    if value:
        if utils.single([v4 for v3 in [v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'system')] for v4 in fhirpath_utils.startswith(v3, ['urn:oid:'])]):
            system = src.system
            if system:
                tgt.root = utils.single([v2 for v1 in [system] for v2 in fhirpath_utils.substring(v1,[8],[])])
                tgt.extension = value.value

def IntegerINT(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = v

def InstantTS(src, tgt):
    Any(src, tgt)
    v = src.value
    if v:
        tgt.value = v

def DateTimeTS(src, tgt):
    InstantTS(src, tgt)

def DateTS(src, tgt):
    InstantTS(src, tgt)

def CodeCS(src, tgt):
    Any(src, tgt)
    c = src.value
    if c:
        tgt.code = c

def CodeCE(src, tgt):
    CodeCS(src, tgt)

def CodeCD(src, tgt):
    CodeCS(src, tgt)

def CodingCE(src, tgt):
    Any(src, tgt)
    code = src.code
    if code:
        tgt.code = code.value
    system = src.system
    if system:
        if utils.single([v2 for v1 in [system] for v2 in fhirpath_utils.startswith(v1, ['http:'])]):
            trans_out = translate(url='uri2oid', code=(system if isinstance(system, str) else system.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.codeSystem = match
    system = src.system
    if system:
        if utils.single([v2 for v1 in [system] for v2 in fhirpath_utils.startswith(v1, ['urn:oid:'])]):
            system = src.system
            if system:
                tgt.codeSystem = utils.single([v2 for v1 in [system] for v2 in fhirpath_utils.substring(v1,[8],[])])
    system = src.system
    if system:
        if utils.single([v2 for v1 in [system] for v2 in fhirpath_utils.startswith(v1, ['http:'])]):
            trans_out = translate(url='uri2system', code=(system if isinstance(system, str) else system.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.codeSystemName = match
    system = src.system
    if system:
        if utils.single([v2 for v1 in [system] for v2 in fhirpath_utils.startswith(v1, ['urn:oid:'])]):
            trans_out = translate(url='oid2system', code=(system if isinstance(system, str) else system.value), silent=False)
            matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
            # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
            if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
            match = matches[0]['code']
            tgt.codeSystemName = match
    display = src.display
    if display:
        tgt.displayName = display.value

def CodeableConceptCE(src, tgt):
    Any(src, tgt)
    text = src.text
    if text:
        originalText = tgt.orginialText # unknown type
        originalText.data = text
    for coding in src.coding:
        CodingCE(coding, tgt)
    for coding in src.coding[1:]:
        translation = cda_austrian_extension.CD()
        tgt.translation.append(translation)
        CodingCE(coding, translation)

def AddressAD(src, tgt):
    Any(src, tgt)
    for v in src.line or []:
        line = cda_austrian_extension.adxp_streetAddressLine()
        tgt.streetAddressLine.append(line)
        line.valueOf_ = v.value
    v = src.postalCode
    if v:
        postalCode = cda_austrian_extension.adxp_postalCode()
        tgt.postalCode.append(postalCode)
        postalCode.valueOf_ = v.value
    if utils.single(fhirpath_utils.equals([bool([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'postalCode')])], '==', [False])):
        postalCode = cda_austrian_extension.adxp_postalCode()
        tgt.postalCode.append(postalCode)
        postalCode.nullFlavor = 'NI'
    v = src.city
    if v:
        city = cda_austrian_extension.adxp_city()
        tgt.city.append(city)
        city.valueOf_ = v.value
    if utils.single(fhirpath_utils.equals([bool([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'city')])], '==', [False])):
        city = cda_austrian_extension.adxp_city()
        tgt.city.append(city)
        city.nullFlavor = 'NI'
    v = src.state
    if v:
        state = cda_austrian_extension.adxp_state()
        tgt.state.append(state)
        state.valueOf_ = v.value
    v = src.district
    if v:
        county = cda_austrian_extension.adxp_county()
        tgt.county.append(county)
        county.valueOf_ = v.value
    v = src.country
    if v:
        country = cda_austrian_extension.adxp_country()
        tgt.country.append(country)
        country.valueOf_ = v.value
    if utils.single(fhirpath_utils.equals([bool([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'country')])], '==', [False])):
        country = cda_austrian_extension.adxp_country()
        tgt.country.append(country)
        country.nullFlavor = 'NI'
    c = src.use
    if c:
        trans_out = translate(url='addressUse', code=(c if isinstance(c, str) else c.value), silent=False)
        matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']
        # if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error
        if len(matches) != 1: sys.exit("There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!")
        match = matches[0]['code']
        tgt.use = match

def ContactPointTEL(src, tgt):
    Any(src, tgt)
    value = src.value
    if value:
        tgt.value = str(utils.single(fhirpath_utils.add(fhirpath_utils.add(fhirpath_utils.add(fhirpath_utils.add((['tel:'] if fhirpath_utils.equals([v2 for v1 in [src] for v2 in fhirpath_utils.get(v1,'system')], '==', ['phone']) == [True] else ['']), (['fax:'] if fhirpath_utils.equals([v4 for v3 in [src] for v4 in fhirpath_utils.get(v3,'system')], '==', ['fax']) == [True] else [''])), (['mailto:'] if fhirpath_utils.equals([v6 for v5 in [src] for v6 in fhirpath_utils.get(v5,'system')], '==', ['email']) == [True] else [''])), (['http:'] if fhirpath_utils.equals([v8 for v7 in [src] for v8 in fhirpath_utils.get(v7,'system')], '==', ['url']) == [True] else [''])), [value])))
    use = src.use
    if use:
        if utils.single(fhirpath_utils.equals([use], '==', ['home'])):
            tgt.use = 'HP'
    use = src.use
    if use:
        if utils.single(fhirpath_utils.equals([use], '==', ['work'])):
            tgt.use = 'WP'
    use = src.use
    if use:
        if utils.single(fhirpath_utils.equals([use], '==', ['old'])):
            tgt.use = 'BAD'
    use = src.use
    if use:
        if utils.single(fhirpath_utils.equals([use], '==', ['temp'])):
            tgt.use = 'TMP'
    use = src.use
    if use:
        if utils.single(fhirpath_utils.equals([use], '==', ['mobile'])):
            tgt.use = 'MC'

def HumanNameEN(src, tgt):
    Any(src, tgt)
    v = src.family
    if v:
        family = cda_austrian_extension.en_family()
        tgt.family.append(family)
        family.valueOf_ = v.value
    for v in src.given or []:
        given = cda_austrian_extension.en_given()
        tgt.given.append(given)
        given.valueOf_ = v.value
    for v in src.prefix or []:
        prefix = cda_austrian_extension.en_prefix()
        tgt.prefix.append(prefix)
        prefix.valueOf_ = v.value
    for v in src.suffix or []:
        suffix = cda_austrian_extension.en_suffix()
        tgt.suffix.append(suffix)
        suffix.valueOf_ = v.value

def QuantityPQ(src, tgt):
    Any(src, tgt)
    code = src.code
    if code:
        tgt.unit = code.value
    value = src.value
    if value:
        tgt.value = value

def RatioRTOPQPQ(src, tgt):
    Any(src, tgt)
    numerator = src.numerator
    if numerator:
        targetNumerator = cda_austrian_extension.PQ()
        if tgt.numerator is not None:
            targetNumerator = tgt.numerator
        else:
            tgt.numerator = targetNumerator
        QuantityPQ(numerator, targetNumerator)
    denominator = src.denominator
    if denominator:
        targetDenominator = cda_austrian_extension.PQ()
        if tgt.denominator is not None:
            targetDenominator = tgt.denominator
        else:
            tgt.denominator = targetDenominator
        QuantityPQ(denominator, targetDenominator)

def Any(src, tgt):
    for extension in src.extension or []:
        if utils.single(fhirpath_utils.equals([v2 for v1 in [extension] for v2 in fhirpath_utils.get(v1,'url')], '==', ['http://hl7.org/fhir/StructureDefinition/iso21090-nullFlavor'])):
            nullFlavor = builtins.str()
            if tgt.nullFlavor is not None:
                nullFlavor = tgt.nullFlavor
            else:
                tgt.nullFlavor = nullFlavor
            valueCode = extension.valueCode
            if valueCode:
                v = valueCode.value
                if v:
                    nullFlavor.value = v # unknown cast None -> string

def unpack_container(resource_container):
    if resource_container.Account is not None:
        return resource_container.Account
    if resource_container.ActivityDefinition is not None:
        return resource_container.ActivityDefinition
    if resource_container.AdministrableProductDefinition is not None:
        return resource_container.AdministrableProductDefinition
    if resource_container.AdverseEvent is not None:
        return resource_container.AdverseEvent
    if resource_container.AllergyIntolerance is not None:
        return resource_container.AllergyIntolerance
    if resource_container.Appointment is not None:
        return resource_container.Appointment
    if resource_container.AppointmentResponse is not None:
        return resource_container.AppointmentResponse
    if resource_container.AuditEvent is not None:
        return resource_container.AuditEvent
    if resource_container.Basic is not None:
        return resource_container.Basic
    if resource_container.Binary is not None:
        return resource_container.Binary
    if resource_container.BiologicallyDerivedProduct is not None:
        return resource_container.BiologicallyDerivedProduct
    if resource_container.BodyStructure is not None:
        return resource_container.BodyStructure
    if resource_container.Bundle is not None:
        return resource_container.Bundle
    if resource_container.CapabilityStatement is not None:
        return resource_container.CapabilityStatement
    if resource_container.CarePlan is not None:
        return resource_container.CarePlan
    if resource_container.CareTeam is not None:
        return resource_container.CareTeam
    if resource_container.CatalogEntry is not None:
        return resource_container.CatalogEntry
    if resource_container.ChargeItem is not None:
        return resource_container.ChargeItem
    if resource_container.ChargeItemDefinition is not None:
        return resource_container.ChargeItemDefinition
    if resource_container.Citation is not None:
        return resource_container.Citation
    if resource_container.Claim is not None:
        return resource_container.Claim
    if resource_container.ClaimResponse is not None:
        return resource_container.ClaimResponse
    if resource_container.ClinicalImpression is not None:
        return resource_container.ClinicalImpression
    if resource_container.ClinicalUseDefinition is not None:
        return resource_container.ClinicalUseDefinition
    if resource_container.CodeSystem is not None:
        return resource_container.CodeSystem
    if resource_container.Communication is not None:
        return resource_container.Communication
    if resource_container.CommunicationRequest is not None:
        return resource_container.CommunicationRequest
    if resource_container.CompartmentDefinition is not None:
        return resource_container.CompartmentDefinition
    if resource_container.Composition is not None:
        return resource_container.Composition
    if resource_container.ConceptMap is not None:
        return resource_container.ConceptMap
    if resource_container.Condition is not None:
        return resource_container.Condition
    if resource_container.Consent is not None:
        return resource_container.Consent
    if resource_container.Contract is not None:
        return resource_container.Contract
    if resource_container.Coverage is not None:
        return resource_container.Coverage
    if resource_container.CoverageEligibilityRequest is not None:
        return resource_container.CoverageEligibilityRequest
    if resource_container.CoverageEligibilityResponse is not None:
        return resource_container.CoverageEligibilityResponse
    if resource_container.DetectedIssue is not None:
        return resource_container.DetectedIssue
    if resource_container.Device is not None:
        return resource_container.Device
    if resource_container.DeviceDefinition is not None:
        return resource_container.DeviceDefinition
    if resource_container.DeviceMetric is not None:
        return resource_container.DeviceMetric
    if resource_container.DeviceRequest is not None:
        return resource_container.DeviceRequest
    if resource_container.DeviceUseStatement is not None:
        return resource_container.DeviceUseStatement
    if resource_container.DiagnosticReport is not None:
        return resource_container.DiagnosticReport
    if resource_container.DocumentManifest is not None:
        return resource_container.DocumentManifest
    if resource_container.DocumentReference is not None:
        return resource_container.DocumentReference
    if resource_container.Encounter is not None:
        return resource_container.Encounter
    if resource_container.Endpoint is not None:
        return resource_container.Endpoint
    if resource_container.EnrollmentRequest is not None:
        return resource_container.EnrollmentRequest
    if resource_container.EnrollmentResponse is not None:
        return resource_container.EnrollmentResponse
    if resource_container.EpisodeOfCare is not None:
        return resource_container.EpisodeOfCare
    if resource_container.EventDefinition is not None:
        return resource_container.EventDefinition
    if resource_container.Evidence is not None:
        return resource_container.Evidence
    if resource_container.EvidenceReport is not None:
        return resource_container.EvidenceReport
    if resource_container.EvidenceVariable is not None:
        return resource_container.EvidenceVariable
    if resource_container.ExampleScenario is not None:
        return resource_container.ExampleScenario
    if resource_container.ExplanationOfBenefit is not None:
        return resource_container.ExplanationOfBenefit
    if resource_container.FamilyMemberHistory is not None:
        return resource_container.FamilyMemberHistory
    if resource_container.Flag is not None:
        return resource_container.Flag
    if resource_container.Goal is not None:
        return resource_container.Goal
    if resource_container.GraphDefinition is not None:
        return resource_container.GraphDefinition
    if resource_container.Group is not None:
        return resource_container.Group
    if resource_container.GuidanceResponse is not None:
        return resource_container.GuidanceResponse
    if resource_container.HealthcareService is not None:
        return resource_container.HealthcareService
    if resource_container.ImagingStudy is not None:
        return resource_container.ImagingStudy
    if resource_container.Immunization is not None:
        return resource_container.Immunization
    if resource_container.ImmunizationEvaluation is not None:
        return resource_container.ImmunizationEvaluation
    if resource_container.ImmunizationRecommendation is not None:
        return resource_container.ImmunizationRecommendation
    if resource_container.ImplementationGuide is not None:
        return resource_container.ImplementationGuide
    if resource_container.Ingredient is not None:
        return resource_container.Ingredient
    if resource_container.InsurancePlan is not None:
        return resource_container.InsurancePlan
    if resource_container.Invoice is not None:
        return resource_container.Invoice
    if resource_container.Library is not None:
        return resource_container.Library
    if resource_container.Linkage is not None:
        return resource_container.Linkage
    if resource_container.List is not None:
        return resource_container.List
    if resource_container.Location is not None:
        return resource_container.Location
    if resource_container.ManufacturedItemDefinition is not None:
        return resource_container.ManufacturedItemDefinition
    if resource_container.Measure is not None:
        return resource_container.Measure
    if resource_container.MeasureReport is not None:
        return resource_container.MeasureReport
    if resource_container.Media is not None:
        return resource_container.Media
    if resource_container.Medication is not None:
        return resource_container.Medication
    if resource_container.MedicationAdministration is not None:
        return resource_container.MedicationAdministration
    if resource_container.MedicationDispense is not None:
        return resource_container.MedicationDispense
    if resource_container.MedicationKnowledge is not None:
        return resource_container.MedicationKnowledge
    if resource_container.MedicationRequest is not None:
        return resource_container.MedicationRequest
    if resource_container.MedicationStatement is not None:
        return resource_container.MedicationStatement
    if resource_container.MedicinalProductDefinition is not None:
        return resource_container.MedicinalProductDefinition
    if resource_container.MessageDefinition is not None:
        return resource_container.MessageDefinition
    if resource_container.MessageHeader is not None:
        return resource_container.MessageHeader
    if resource_container.MolecularSequence is not None:
        return resource_container.MolecularSequence
    if resource_container.NamingSystem is not None:
        return resource_container.NamingSystem
    if resource_container.NutritionOrder is not None:
        return resource_container.NutritionOrder
    if resource_container.NutritionProduct is not None:
        return resource_container.NutritionProduct
    if resource_container.Observation is not None:
        return resource_container.Observation
    if resource_container.ObservationDefinition is not None:
        return resource_container.ObservationDefinition
    if resource_container.OperationDefinition is not None:
        return resource_container.OperationDefinition
    if resource_container.OperationOutcome is not None:
        return resource_container.OperationOutcome
    if resource_container.Organization is not None:
        return resource_container.Organization
    if resource_container.OrganizationAffiliation is not None:
        return resource_container.OrganizationAffiliation
    if resource_container.PackagedProductDefinition is not None:
        return resource_container.PackagedProductDefinition
    if resource_container.Patient is not None:
        return resource_container.Patient
    if resource_container.PaymentNotice is not None:
        return resource_container.PaymentNotice
    if resource_container.PaymentReconciliation is not None:
        return resource_container.PaymentReconciliation
    if resource_container.Person is not None:
        return resource_container.Person
    if resource_container.PlanDefinition is not None:
        return resource_container.PlanDefinition
    if resource_container.Practitioner is not None:
        return resource_container.Practitioner
    if resource_container.PractitionerRole is not None:
        return resource_container.PractitionerRole
    if resource_container.Procedure is not None:
        return resource_container.Procedure
    if resource_container.Provenance is not None:
        return resource_container.Provenance
    if resource_container.Questionnaire is not None:
        return resource_container.Questionnaire
    if resource_container.QuestionnaireResponse is not None:
        return resource_container.QuestionnaireResponse
    if resource_container.RegulatedAuthorization is not None:
        return resource_container.RegulatedAuthorization
    if resource_container.RelatedPerson is not None:
        return resource_container.RelatedPerson
    if resource_container.RequestGroup is not None:
        return resource_container.RequestGroup
    if resource_container.ResearchDefinition is not None:
        return resource_container.ResearchDefinition
    if resource_container.ResearchElementDefinition is not None:
        return resource_container.ResearchElementDefinition
    if resource_container.ResearchStudy is not None:
        return resource_container.ResearchStudy
    if resource_container.ResearchSubject is not None:
        return resource_container.ResearchSubject
    if resource_container.RiskAssessment is not None:
        return resource_container.RiskAssessment
    if resource_container.Schedule is not None:
        return resource_container.Schedule
    if resource_container.SearchParameter is not None:
        return resource_container.SearchParameter
    if resource_container.ServiceRequest is not None:
        return resource_container.ServiceRequest
    if resource_container.Slot is not None:
        return resource_container.Slot
    if resource_container.Specimen is not None:
        return resource_container.Specimen
    if resource_container.SpecimenDefinition is not None:
        return resource_container.SpecimenDefinition
    if resource_container.StructureDefinition is not None:
        return resource_container.StructureDefinition
    if resource_container.StructureMap is not None:
        return resource_container.StructureMap
    if resource_container.Subscription is not None:
        return resource_container.Subscription
    if resource_container.SubscriptionStatus is not None:
        return resource_container.SubscriptionStatus
    if resource_container.SubscriptionTopic is not None:
        return resource_container.SubscriptionTopic
    if resource_container.Substance is not None:
        return resource_container.Substance
    if resource_container.SubstanceDefinition is not None:
        return resource_container.SubstanceDefinition
    if resource_container.SupplyDelivery is not None:
        return resource_container.SupplyDelivery
    if resource_container.SupplyRequest is not None:
        return resource_container.SupplyRequest
    if resource_container.Task is not None:
        return resource_container.Task
    if resource_container.TerminologyCapabilities is not None:
        return resource_container.TerminologyCapabilities
    if resource_container.TestReport is not None:
        return resource_container.TestReport
    if resource_container.TestScript is not None:
        return resource_container.TestScript
    if resource_container.ValueSet is not None:
        return resource_container.ValueSet
    if resource_container.VerificationResult is not None:
        return resource_container.VerificationResult
    if resource_container.VisionPrescription is not None:
        return resource_container.VisionPrescription
    if resource_container.Parameters is not None:
        return resource_container.Parameters
    return None

if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    transform(args.source, args.target)
