import argparse
import time
import fhir4
import sys

description_text = "This has been compiled by the MApping LAnguage Compiler for Health Data, short MaLaC-HD. See arguments for more details."

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description_text)
    parser.add_argument(
       '-c', '--code', help="""code --	
The code that is to be translated. If a code is provided, a system must be provided""",
        required=True
    )
    parser.add_argument(
       '-sys', '--system', help="""uri --	
The system for the code that is to be translated""",
        required=True
    )
    parser.add_argument(
       '-ver', '--version', help="""string --	
The version of the system, if one was provided in the source data""",
        required=True
    )
    parser.add_argument(
       '-s', '--source', help="""uri --	
Identifies the value set used when the concept (system/code pair) was chosen. 
May be a logical id, or an absolute or relative location. 
The source value set is an optional parameter because in some cases, the client cannot know what the source value set is. 
However, without a source value set, the server may be unable to safely identify an applicable concept map, 
and would return an error. For this reason, a source value set SHOULD always be provided. 
Note that servers may be able to identify an appropriate concept map without a source value set 
if there is a full mapping for the entire code system in the concept map, or by manual intervention""",
        required=True
    )
    parser.add_argument(
       '-cg', '--coding', help="""Coding --	
A coding to translate""",
        required=True
    )
    parser.add_argument(
       '-cc', '--codeableConcept', help="""CodeableConcept --	
A full codeableConcept to validate. The server can translate any of the coding values (e.g. existing translations) 
as it chooses""",
        required=True
    )
    parser.add_argument(
       '-t', '--target', help="""uri -- 
Identifies the value set in which a translation is sought. May be a logical id, or an absolute or relative location. 
If there's no target specified, the server should return all known translations, along with their source""", 
        required=True
    )
    parser.add_argument(
       '-ts', '--targetsystem', help="""uri -- 
Identifies a target code system in which a mapping is sought. This parameter is an alternative to the target parameter - 
only one is required. Searching for any translation to a target code system irrespective of the context (e.g. target valueset) 
may lead to unsafe results, and it is at the discretion of the server to decide when to support this operation""", 
        required=True
    )
    parser.add_argument(
       '-r', '--reverse', action="store_true", help="""boolean -- True if stated, else False -- 
If this is true, then the operation should return all the codes that might be mapped to this code. 
This parameter reverses the meaning of the source and target parameters""", 
        required=True
    )
    parser.add_argument(
       '-st', '--silent', action="store_true", help="""boolean -- True if stated, else False --
Do not print the converted python mapping to console""", 
        required=True
    )
    return parser

# output
# 1..1 result (boolean)
# 0..1 message with error details for human (string)
# 0..* match with (list)
#   0..1 equivalnce (string from https://hl7.org/fhir/R4B/valueset-concept-map-equivalence.html)
#   0..1 concept
#       0..1 system
#       0..1 version
#       0..1 code
#       0..1 display 
#       0..1 userSelected will always be false, because this is a translation
#   0..1 source (conceptMap url)
# TODO implement reverse
def translate(url=None, conceptMapVersion=None, code=None, system=None, version=None, source=None, coding=None, codeableConcept=None, target=None, targetsystem=None, reverse=None, silent=False)              -> dict [bool, str, list[dict[str, dict[str, str, str, str, bool], str]]]:
    start = time.time()
    
    # start validation and recall of translate in simple from
    if codeableConcept:
        if isinstance(codeableConcept, str): 
            codeableConcept = fhir4.parseString(codeableConcept, silent)
        elif isinstance(coding, fhir4.CodeableConcept):
            pass
        else:
            sys.exit("The codeableConcept parameter has to be a string or a CodeableConcept Object (called method as library)!")
        # the first fit will be returned, else the last unfitted value will be returned
        # TODO check translate params
        for one_coding in codeableConcept.get_coding:
            if (ret := translate(url=url, source=source, coding=one_coding, 
                                 target=target, targetsystem=targetsystem, 
                                 reverse=reverse, silent=True))[0]:
                return ret
        else: return ret
        
    elif coding:
        if isinstance(coding, str): 
            coding = fhir4.parseString(coding, silent)
        elif isinstance(coding, fhir4.Coding):
            pass
        else:
            sys.exit("The coding parameter has to be a string or a Coding Object (called method as library)!")
        # TODO check translate params
        return translate(url=url,  source=source, coding=one_coding, 
                         target=target, targetsystem=targetsystem, 
                         reverse=reverse, silent=True)
        
    elif code:
        if not isinstance(code,str): 
            sys.exit("The code parameter has to be a string!")
        
    elif target:
        if not isinstance(code,str): 
            sys.exit("The target parameter has to be a string!")
        
    elif targetsystem:
        if not isinstance(code,str): 
            sys.exit("The targetsystem parameter has to be a string!")
        
    else:
        sys.exit("At least codeableConcept, coding, code, target or targetSystem has to be given!")
    # end validation and recall of translate in simplier from

    # look for any information from the one ore more generated conceptMaps into con_map_7d
    match = []
    unmapped = []
    if url not in con_map_7d.keys():
        print('   #ERROR# ConceptMap with URL "'+ url +'" is not loaded to this compiled conceptMap #ERROR#')
    else:
        for url_lvl in con_map_7d:
            if url_lvl == "%" or url_lvl == str(url or ""):#+str(("/?version=" and conceptMapVersion) or ""):
                for source_lvl in con_map_7d[url_lvl]:
                    if source_lvl == "%" or not source or source_lvl == source:
                        for target_lvl in con_map_7d[url_lvl][source_lvl]:
                            if target_lvl == "%" or not target or target_lvl == target:
                                for system_lvl in con_map_7d[url_lvl][source_lvl][target_lvl]:
                                    if system_lvl == "%" or not system or system_lvl == system:#+str(("/?version=" and version) or ""):
                                        for targetsystem_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl]:
                                            if targetsystem_lvl == "%" or not targetsystem or targetsystem_lvl == targetsystem:
                                                for code_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl]:
                                                    if code_lvl == "|" or code_lvl == "~" or code_lvl == "#":
                                                        unmapped += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]
                                                    if code_lvl == "%" or not code or code_lvl == code:
                                                        match += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]                
                                                    
    if not match:
        for one_unmapped in unmapped:
            tmp_system = ""
            tmp_version = ""
            tmp_code = ""
            tmp_display = ""
            # replace all "|" values with to translated code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            if one_unmapped["concept"]["code"].startswith("|"):
                tmp_system = system
                tmp_version = version
                tmp_code = one_unmapped["concept"]["code"][1:] + code
            # replace all "~" values with fixed code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            elif one_unmapped["concept"]["code"].startswith("~"):
                tmp_code = one_unmapped["concept"]["code"][1:]
                tmp_display = one_unmapped["concept"]["display"]
            elif one_unmapped["concept"]["code"].startswith("#"):
                # TODO detect recursion like conceptMapA -> conceptMapB -> ConceptMapA -> ...
                return translate(one_unmapped["concept"]["code"][1:], None, code, system, version, source, 
                                 coding, codeableConcept, target, targetsystem, reverse, silent)
            match.append({"equivalence": one_unmapped["equivalence"], 
                          "concept":{
                            "system": tmp_system, 
                            "version": tmp_version, # TODO version of codesystem out of url?
                            "code": tmp_code,
                            "display": tmp_display,
                            "userSelected": False},
                          "source": one_unmapped["source"]})
                
            
    # see if any match is not "unmatched" or "disjoint"
    result = False
    message = ""
    for one_match in match:
        if one_match["equivalence"] != "unmatched" and one_match["equivalence"] != "disjoint":
            result = True 

    if not silent:
        print('Translation in '+str(round(time.time()-start,3))+' seconds for code "'+code+'" with ConceptMap "'+url+'"')
    return {"result": result, "message": message, "match": match}

# The con_map_7d is a seven dimensional dictionary, for quickly finding the fitting translation
# All dimensions except the last are optional, so a explicit NONE value will be used as key and 
# interpreted as the default key, that always will be fitting, no matter what other keys are fitting.
# If a version is included (purely optional), than the version will be added with a blank before to the key
#
# The 0th dimension is mandatory and stating the ConceptMap with its url (including the version).
#
# The 1st dimension is optional and stating the SOURCE valueset (including the version), as one conceptMap can only 
# have a maximum of one SOURCE, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 2nd dimension is optional and stating the TARGET valueset (including the version), as one conceptMap can only 
# have a maximum of one TARGET, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 3th dimension is optional and stating the SYSTEM (including the version) from the source valueset code, as one 
# code could be used in multiple SYSTEMs from the source valueset to translate. 
# Not stating a SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this SYSTEM element as stating the valueset, that should be
# stated in source, this case will still be supported by MaLaC-HD. Having a conceptMap with a source valueset 
# and a different SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#
# The 4th dimension is optional and stating the TARGET SYSTEM (including the version) from the target valueset code, as one 
# code could be used in multiple SYSTEMs from the target valueset to translate. 
# Not stating a TARGET SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this TARGET SYSTEM element as stating the target valueset, that should be
# stated in target, this case will still be supported by MaLaC-HD. Having a conceptMap with a target valueset 
# and a different TARGET SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#   
# The 5th dimension is optional and stating the CODE from the source valueset, as one conceptMap can have none or 
# multiple CODEs from the source to translate. 
#
# The 6th dimension is NOT optional and stating the TARGET CODE from the target valueset. As one source code could be translated 
# in multiple TARGET CODEs, the whole set have to be returend. 
# For a translation with explicitly no TARGET CODE, because of an quivalence of unmatched or disjoint, NONE will be returned. 
#   
# a minimal example, translating "hi" to "servus": 
# con_map_7d = {"myConMap": {None: {None: {"hi": {None: {None: ["equivalent", "<coding><code>servus</code></coding>", "https://my.concept.map/conceptMap/my"]}}}}}
#
# TODO add a dimension for a specific dependsOn property
# TODO add a solution for the unmapped element
con_map_7d = {}

con_map_7d["http://hl7.org/fhir/ConceptMap/101"] = {
    "http://hl7.org/fhir/ValueSet/address-use": {
        "http://terminology.hl7.org/ValueSet/v3-AddressUse": {
            "http://hl7.org/fhir/address-use": {
                "http://terminology.hl7.org/CodeSystem/v3-AddressUse": {
                    "home": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/address-use",
                                "version": "",
                                "code": "H",
                                "display": "home",
                                "userSelected": False
                            },
                            "source": "http://hl7.org/fhir/ConceptMap/101"
                        }
                    ],
                    "work": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/address-use",
                                "version": "",
                                "code": "WP",
                                "display": "work place",
                                "userSelected": False
                            },
                            "source": "http://hl7.org/fhir/ConceptMap/101"
                        }
                    ],
                    "temp": [
                        {
                            "equivalence": "equivalent",
                            "concept": {
                                "system": "http://hl7.org/fhir/address-use",
                                "version": "",
                                "code": "TMP",
                                "display": "temporary address",
                                "userSelected": False
                            },
                            "source": "http://hl7.org/fhir/ConceptMap/101"
                        }
                    ],
                    "old": [
                        {
                            "equivalence": "disjoint",
                            "concept": {
                                "system": "http://hl7.org/fhir/address-use",
                                "version": "",
                                "code": "BAD",
                                "display": "bad address",
                                "userSelected": False
                            },
                            "source": "http://hl7.org/fhir/ConceptMap/101"
                        }
                    ],
                    "~": [
                        {
                            "equivalence": "inexact",
                            "concept": {
                                "system": "",
                                "version": "",
                                "code": "~temp",
                                "display": "temp",
                                "userSelected": False
                            },
                            "source": "http://hl7.org/fhir/ConceptMap/101"
                        }
                    ]
                }
            }
        }
    }
}
        
if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    ret = translate(args.code, args.system, args.version, args.source, args.coding, 
    args.codeableConcept, args.target, args.targetsystem, args.reverse, args.silent)