import json
import pytest
import os
import inspect
import typing
import dateutil
import datetime
import decimal
import uuid

from antlr4 import *
from antlr4.tree.Tree import ParseTreeWalker
from antlr4.error.ErrorListener import ErrorListener
from antlr4.error.Errors import LexerNoViableAltException
from lxml import etree

import fhir4_map_converter
import fhir4
import utils
from fhirpath.r4.listener import PythonListener
from fhirpath.r4.generated.fhirpathLexer import fhirpathLexer as FHIRPathLexer
from fhirpath.r4.generated.fhirpathParser import fhirpathParser as FHIRPathParser

from . import fhirpathtest

def pytest_collect_file(parent, path):
    if path.basename == "tests-fhir-r4.xml":
        return FHIRPathTestFile.from_parent(parent, fspath=path)
    elif path.basename == "test-structuremap.xml":
        return StructureMapTestFile.from_parent(parent, fspath=path)

class FHIRPathTestFile(pytest.File):
    def collect(self):
        raw = fhirpathtest.parse(self.fspath.strpath)
        for group in raw.group:
            group_name = group.name
            for test in group.test:
                if not test.predicate and not test.expression.invalid:
                    yield FHIRPathTestItem.from_parent(
                        self,
                        name=group_name + ": " + test.name,
                        test=test,
                        path = self.fspath.strpath
                    )

class StructureMapTestFile(pytest.File):
    def collect(self):
        tree = etree.parse(self.path)
        test = tree.getroot().attrib
        children = tree.getroot().getchildren()
        transforms = []
        if children:
            for child in children:
                transforms.append(child.attrib)
        elif test.get("transform-input", None) and test.get("transform-output", None):
            transforms.append({"input": test["transform-input"], "output": test["transform-output"]})

        yield StructureMapTestItem.from_parent(self, name=test["name"], test=test, transforms=transforms, path=self.fspath.strpath)

class StructureMapTestItem(pytest.Item):

    def __init__(self, name, parent, test, transforms, path):
        super().__init__(name, parent)
        self.path = os.path.dirname(path) + "/"
        self.map_input = self.path + test["map"]
        self.convert_output = self.path + test["convert-output"]
        self.transforms = transforms

    def runtest(self):
        fhir4_map_converter.StructureMapSub.additional_header = "curr_uuid = 0\ndef dummy_uuid():\n  global curr_uuid\n  curr_uuid += 1\n  return str(uuid.uuid3(uuid.NAMESPACE_URL, str(curr_uuid)))\n\n"
        fhir4_map_converter.StructureMapSub.uuid_method = "dummy_uuid()"
        fhir4_map_converter.StructureMapSub.now_method = "datetime(2000,1,1)"

        map = fhir4_map_converter.parse(self.map_input, silence=True)

        mod_name = "mapping" + str(uuid.uuid4())
        try:
            with open(mod_name + ".py", 'w', newline='', encoding='utf-8') as f:
                f.write(map.convert(silent=True, source=os.path.abspath(self.map_input)))

            # as similar as possible to https://www.hl7.org/fhir/structuremap-operation-transform.html
            # if args.output is full file path, then do the transform itself we just created
            mapping = __import__(mod_name)
            for transform in self.transforms:
                print("")
                transform_input = self.path + transform["input"]
                transform_output = self.path + transform["output"]
                mapping.transform(transform_input, transform_output)
                print("")

            # because of the maybe needed import, move the convert output at the end to the right location
            if self.convert_output:
                if os.path.isfile(self.convert_output):
                    os.remove(self.convert_output)
                os.rename(mod_name + ".py", self.convert_output)
        except BaseException as ex:
            try:
                os.remove(mod_name + ".py")
            except:
                pass
            raise ex

        # TODO add some checks


class FHIRPathTestItem(pytest.Item):
    def __init__(self, name, parent, test, path):
        super().__init__(name, parent)
        self.test = test
        self.path = path

    def runtest(self):
        resource = self.test.inputfile
        expression = self.test.expression.valueOf_

        result = self.evaluate(os.path.dirname(self.path) + "/input/" + resource, expression, fhir4)
        expected_result = []
        for o in self.test.output:
            if o.type_ == "boolean":
                item = o.valueOf_ == "true"
            elif o.type_ == "integer":
                item = int(o.valueOf_)
            else:
                item = o.valueOf_
            expected_result.append(item)
        compare(result, expected_result)

    def evaluate(self, resource, expression, module):
        def recover(e):
            raise e

        mod_res = module.parse(resource, silence=True)
        resource_type = type(mod_res).__name__
        var_types = {resource_type: type(mod_res), "$this": type(mod_res)}
        src = resource_type
        this_elem = {} # TODO add all elems of resource here
        src_type = type(mod_res)

        for elem, type_str in inspect.get_annotations(src_type.__init__).items():
            this_elem[elem] = utils._get_type(src_type, elem)

        pythonListener = PythonListener(var_types, set(), module, src, this_elem)
        errorListener = ErrorListener()

        textStream = InputStream(expression)

        lexer = FHIRPathLexer(textStream)
        lexer.recover = recover
        lexer.removeErrorListeners()
        lexer.addErrorListener(errorListener)

        parser = FHIRPathParser(CommonTokenStream(lexer))
        parser.buildParseTrees = True
        parser.removeErrorListeners()
        parser.addErrorListener(errorListener)

        walker = ParseTreeWalker()
        try:
            walker.walk(pythonListener, parser.expression())
        except NotImplementedError as ex:
            raise ex
        except BaseException as ex:
            raise ex

        py_code = pythonListener.py_code()
        result = py_code.code

        context = {
            module.__name__: module, 
            "dateutil": dateutil, 
            "datetime": datetime, 
            "decimal": decimal,
            "fhirpath_utils": utils.FHIRPathUtils(module),
            "one_timestamp": datetime.datetime.now()
        }
        if resource_type:
            context[resource_type] = mod_res
        else:
            context.update(resource)
        try:
            exec("__myresult__=" + result, context) # TODO: pass locals
        except BaseException as ex:
            raise ex

        base_result = context["__myresult__"]
        if base_result is None:
            base_result = []
        elif not isinstance(base_result, list):
            base_result = [base_result]
        result = []
        for r in base_result:
            value_annotation = inspect.get_annotations(type(r).__init__).get("value", "")
            if value_annotation == type(r).__name__ + "Enum" or value_annotation == type(r).__name__ + "-primitive":
                r = r.value
            elif isinstance(r, getattr(module, "Quantity")):
                if r.unit:
                    r = "%d %s" % (r.value.value, r.unit.value)
                else:
                    r = "%d '%s'" % (r.value.value, r.code.value)
            elif isinstance(r, getattr(module, "GeneratedsSuper")):
                f = io.StringIO()
                r.export(f, 0, "", 'xmlns="http://hl7.org/fhir"')
                xml_str = f.getvalue()
                clazz = get_fhir_model_class(type(r).__name__)
                res = clazz.parse_raw(f.getvalue(), content_type="text/xml")
                r = json.loads(res.json())
            result.append(r)
        return result

def compare(l1, l2):
    # TODO REFACTOR
    if l1 == l2:
        assert True
    elif isinstance(l1, list) and isinstance(l2, list):
        assert sorted(l1) == sorted(l2)
    # elif len(l1) == len(l2) == 1:
    #     e1 = l1[0]
    #     e2 = evaluate({}, l2[0])[0] if isinstance(l2[0], str) else l2[0]
    #     if isinstance(e1, FP_Quantity) and isinstance(e2, FP_Quantity):
    #         assert e1 == e2
    #     else:
    #         assert str(e1) == str(e2)
    else:
        assert False, f"{l1} != {l2}"