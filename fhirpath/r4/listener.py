import inspect
import typing
import datetime
import decimal
from collections import OrderedDict

from antlr4.tree.Tree import TerminalNodeImpl
from .generated.fhirpathListener import fhirpathListener as FHIRPathListener
from .generated.fhirpathParser import fhirpathParser

import utils

class PythonCode(object):

    def __init__(self, code, out_type, list_func=False, single_val=False):
        self.code = code
        self.out_type = out_type
        self.list_func = list_func
        self.single_val = single_val

    def __str__(self) -> str:
        return self.code


class PythonListener(FHIRPathListener):

    def __init__(self, var_types, list_var, o_module, this, this_elem):
        self.o_module = o_module
        self.utils = utils.FHIRPathUtils(o_module)
        self.counter = 0
        self.parentStack = [{"py_code": {}}]
        self.var_types = var_types
        self.list_var = list_var
        self.this = this
        self.this_elem = this_elem
        self.where_vars = []
        self.resolve_ctx = set()
        if this:
            self.resolve_ctx.add(this)

    def py_code(self):
        py_code = next(iter(self.parentStack[0]["py_code"].values()))
        py_code.code = py_code.code.replace("#", "[%s]" % self.this)
        return py_code

    # Enter a parse tree produced by fhirpathParser#indexerExpression.
    def enterIndexerExpression(self, ctx: fhirpathParser.IndexerExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#indexerExpression.
    def exitIndexerExpression(self, ctx: fhirpathParser.IndexerExpressionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.at_index({code[0].code}, {code[1].code})", code[0].out_type)

    # Enter a parse tree produced by fhirpathParser#polarityExpression.
    def enterPolarityExpression(self, ctx: fhirpathParser.PolarityExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#polarityExpression.
    def exitPolarityExpression(self, ctx: fhirpathParser.PolarityExpressionContext):
        node, parentNode, code = self._exit(ctx)
        if ctx.children[0].getText() == "-":
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.negate(%s)" % code[0].code, int)
        else:
            parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#additiveExpression.
    def enterAdditiveExpression(self, ctx: fhirpathParser.AdditiveExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#additiveExpression.
    def exitAdditiveExpression(self, ctx: fhirpathParser.AdditiveExpressionContext):
        node, parentNode, code = self._exit(ctx)
        operator = ctx.children[1].getText()
        if operator == "+":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.add({code[0].code}, {code[1].code})", code[0].out_type)
        elif operator == "-":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.subtract({code[0].code}, {code[1].code})", code[0].out_type)
        elif operator == "&":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.concat({code[0].code}, {code[1].code})", code[0].out_type)

    # Enter a parse tree produced by fhirpathParser#multiplicativeExpression.
    def enterMultiplicativeExpression(self, ctx: fhirpathParser.MultiplicativeExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#multiplicativeExpression.
    def exitMultiplicativeExpression(self, ctx: fhirpathParser.MultiplicativeExpressionContext):
        node, parentNode, code = self._exit(ctx)
        operator = ctx.children[1].getText()
        if operator == "*":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.multiply({code[0].code}, {code[1].code})", code[0].out_type)
        elif operator == "/":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.divide({code[0].code}, {code[1].code})", code[0].out_type)
        elif operator == "div":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.div({code[0].code}, {code[1].code})", int)
        elif operator == "mod":
            parentNode["py_code"][ctx] = PythonCode(f"fhirpath_utils.mod({code[0].code}, {code[1].code})", int)

    # Enter a parse tree produced by fhirpathParser#unionExpression.
    def enterUnionExpression(self, ctx: fhirpathParser.UnionExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#unionExpression.
    def exitUnionExpression(self, ctx: fhirpathParser.UnionExpressionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.union(%s, %s)" % (code[0].code, code[1].code), code[0].out_type)

    # Enter a parse tree produced by fhirpathParser#orExpression.
    def enterOrExpression(self, ctx: fhirpathParser.OrExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#orExpression.
    def exitOrExpression(self, ctx: fhirpathParser.OrExpressionContext):
        node, parentNode, code = self._exit(ctx)
        op = ctx.children[1].getText()
        if op == "or":
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.bool_or(%s, %s)" % (code[0].code, code[1].code), bool)
        else:
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.bool_xor(%s, %s)" % (code[0].code, code[1].code), bool)

    # Enter a parse tree produced by fhirpathParser#andExpression.
    def enterAndExpression(self, ctx: fhirpathParser.AndExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#andExpression.
    def exitAndExpression(self, ctx: fhirpathParser.AndExpressionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.bool_and(%s, %s)" % (code[0].code, code[1].code), bool)

    # Enter a parse tree produced by fhirpathParser#membershipExpression.
    def enterMembershipExpression(self, ctx: fhirpathParser.MembershipExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#membershipExpression.
    def exitMembershipExpression(self, ctx: fhirpathParser.MembershipExpressionContext):
        node, parentNode, code = self._exit(ctx)
        op = ctx.children[1].getText()
        if op == "in":
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.membership(%s, %s)" % (code[0].code, code[1].code), None)
        elif op == "contains":
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.containership(%s, %s)" % (code[0].code, code[1].code), None)
        else:
            raise BaseException("Invalid membership operation %s" % op)

    # Enter a parse tree produced by fhirpathParser#inequalityExpression.
    def enterInequalityExpression(self, ctx: fhirpathParser.InequalityExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#inequalityExpression.
    def exitInequalityExpression(self, ctx: fhirpathParser.InequalityExpressionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.compare(%s, '%s', %s)" % (code[0].code, ctx.children[1].getText(), code[1].code), bool)

    # Enter a parse tree produced by fhirpathParser#invocationExpression.
    def enterInvocationExpression(self, ctx: fhirpathParser.InvocationExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#invocationExpression.
    def exitInvocationExpression(self, ctx: fhirpathParser.InvocationExpressionContext):
        node, parentNode, code = self._exit(ctx)

        if isinstance(ctx.children[-1], fhirpathParser.MemberInvocationContext):
            context = code[0]
            element = code[1]
            out_type, is_list = utils._get_type(context.out_type, element.code)
            add_args = ""
            if not out_type and element.code in ["dataString", "other", "dataBase64Binary", "xmlText"]:
                element.code = "valueOf_"
                add_args = ",strip=True"
                out_type = str
            elif out_type and context.out_type.__name__ == "ResourceContainer":
                raise BaseException("Cannot access contained without specifiying type (use 'as' or ofType)")
            v = self._get_new_varname()
            v2 = self._get_new_varname()
            code = f"[{v2} for {v} in {context.code} for {v2} in fhirpath_utils.get({v},'{element.code}'{add_args})]"
            py_code = PythonCode(code, out_type)
        elif isinstance(ctx.children[-1], fhirpathParser.FunctionInvocationContext):
            params = code[0]
            func = code[1]
            if func.out_type == "#":
                func.out_type = params.out_type
            if func.list_func:
                new_code = func.code.replace("#", params.code)
            else:
                v1 = self._get_new_varname()
                v2 = self._get_new_varname()
                proj = func.code.replace("#", v1)
                new_code = f"[{v2} for {v1} in {params.code} for {v2} in {proj}]"
            py_code = PythonCode(new_code, func.out_type)
        else:
            raise NotImplementedError("Not implemented: invocation other")
        parentNode["py_code"][ctx] = py_code

    # Enter a parse tree produced by fhirpathParser#equalityExpression.
    def enterEqualityExpression(self, ctx: fhirpathParser.EqualityExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#equalityExpression.
    def exitEqualityExpression(self, ctx: fhirpathParser.EqualityExpressionContext):
        node, parentNode, code = self._exit(ctx)
        op = ctx.children[1].getText()
        if op == "=":
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.equals(%s, '%s', %s)" % (code[0].code, "==", code[1].code), bool)
        elif op == "!=":
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.equals(%s, '%s', %s)" % (code[0].code, op, code[1].code), bool)
        elif op in  ["~", "!~"]:
            parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.equivalent(%s, '%s', %s)" % (code[0].code, op, code[1].code), bool)
        else:
            raise NotImplementedError("Not implemented: equality expr")

    # Enter a parse tree produced by fhirpathParser#impliesExpression.
    def enterImpliesExpression(self, ctx: fhirpathParser.ImpliesExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#impliesExpression.
    def exitImpliesExpression(self, ctx: fhirpathParser.ImpliesExpressionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.bool_implies(%s, %s)" % (code[0].code, code[1].code), bool)

    # Enter a parse tree produced by fhirpathParser#termExpression.
    def enterTermExpression(self, ctx: fhirpathParser.TermExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#termExpression.
    def exitTermExpression(self, ctx: fhirpathParser.TermExpressionContext):
        node, parentNode, code, = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#typeExpression.
    def enterTypeExpression(self, ctx: fhirpathParser.TypeExpressionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#typeExpression.
    def exitTypeExpression(self, ctx: fhirpathParser.TypeExpressionContext):
        node, parentNode, code, = self._exit(ctx)
        op = ctx.children[1].getText()
        if op == "is":
            out_type = bool
        else:
            out_type = self.utils.gettype_fromspec(code[1].code)
        parentNode["py_code"][ctx] = PythonCode("fhirpath_utils.%s_type(%s, '%s')" % (op, code[0].code, code[1].code), out_type)

    # Enter a parse tree produced by fhirpathParser#invocationTerm.
    def enterInvocationTerm(self, ctx: fhirpathParser.InvocationTermContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#invocationTerm.
    def exitInvocationTerm(self, ctx: fhirpathParser.InvocationTermContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#literalTerm.
    def enterLiteralTerm(self, ctx: fhirpathParser.LiteralTermContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#literalTerm.
    def exitLiteralTerm(self, ctx: fhirpathParser.LiteralTermContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#externalConstantTerm.
    def enterExternalConstantTerm(self, ctx: fhirpathParser.ExternalConstantTermContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#externalConstantTerm.
    def exitExternalConstantTerm(self, ctx: fhirpathParser.ExternalConstantTermContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#parenthesizedTerm.
    def enterParenthesizedTerm(self, ctx: fhirpathParser.ParenthesizedTermContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#parenthesizedTerm.
    def exitParenthesizedTerm(self, ctx: fhirpathParser.ParenthesizedTermContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("(%s)" % code[0].code, code[0].out_type)

    # Enter a parse tree produced by fhirpathParser#nullLiteral.
    def enterNullLiteral(self, ctx: fhirpathParser.NullLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#nullLiteral.
    def exitNullLiteral(self, ctx: fhirpathParser.NullLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("[]", None)

    # Enter a parse tree produced by fhirpathParser#booleanLiteral.
    def enterBooleanLiteral(self, ctx: fhirpathParser.BooleanLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#booleanLiteral.
    def exitBooleanLiteral(self, ctx: fhirpathParser.BooleanLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("[%s]" % ctx.getText().title(), None)

    # Enter a parse tree produced by fhirpathParser#stringLiteral.
    def enterStringLiteral(self, ctx: fhirpathParser.StringLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#stringLiteral.
    def exitStringLiteral(self, ctx: fhirpathParser.StringLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("[%s]" % ctx.getText().replace("\\`", "`"), str)

    # Enter a parse tree produced by fhirpathParser#numberLiteral.
    def enterNumberLiteral(self, ctx: fhirpathParser.NumberLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#numberLiteral.
    def exitNumberLiteral(self, ctx: fhirpathParser.NumberLiteralContext):
        node, parentNode, code = self._exit(ctx)
        number = ctx.getText()
        if "." in number:
            parentNode["py_code"][ctx] = PythonCode("[decimal.Decimal('%s')]" % number, decimal.Decimal)
        else:
            parentNode["py_code"][ctx] = PythonCode("[%s]" % number, int)

    # Enter a parse tree produced by fhirpathParser#dateLiteral.
    def enterDateLiteral(self, ctx: fhirpathParser.DateLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#dateLiteral.
    def exitDateLiteral(self, ctx: fhirpathParser.DateLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("[fhirpath_utils.SystemDate(value='%s')]" % (ctx.getText()[1:]), getattr(self.o_module, "date"))

    # Enter a parse tree produced by fhirpathParser#dateTimeLiteral.
    def enterDateTimeLiteral(self, ctx: fhirpathParser.DateTimeLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#dateTimeLiteral.
    def exitDateTimeLiteral(self, ctx: fhirpathParser.DateTimeLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("[fhirpath_utils.SystemDateTime(value='%s')]" % (ctx.getText()[1:]), getattr(self.o_module, "dateTime"))

    # Enter a parse tree produced by fhirpathParser#timeLiteral.
    def enterTimeLiteral(self, ctx: fhirpathParser.TimeLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#timeLiteral.
    def exitTimeLiteral(self, ctx: fhirpathParser.TimeLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("[fhirpath_utils.SystemTime(value='%s')]" % (ctx.getText()[2:]), getattr(self.o_module, "time"))

    # Enter a parse tree produced by fhirpathParser#quantityLiteral.
    def enterQuantityLiteral(self, ctx: fhirpathParser.QuantityLiteralContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#quantityLiteral.
    def exitQuantityLiteral(self, ctx: fhirpathParser.QuantityLiteralContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#externalConstant.
    def enterExternalConstant(self, ctx: fhirpathParser.ExternalConstantContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#externalConstant.
    def exitExternalConstant(self, ctx: fhirpathParser.ExternalConstantContext):
        node, parentNode, code = self._exit(ctx)
        constant = ctx.getText()[1:].replace('"', "").replace("`", "")
        if constant == "resource" or constant == "context":
            py_code = self.this
            out_type = self.var_types["$this"]
        elif constant == "ucum":
            py_code = "['http://unitsofmeasure.org']"
            out_type = str
        elif constant == "sct":
            py_code = "['http://snomed.info/sct']"
            out_type = str
        elif constant == "loinc":
            py_code = "['http://loinc.org']"
            out_type = str
        elif constant.startswith("vs-"):
            py_code = "['http://hl7.org/fhir/ValueSet/%s']" % constant[3:]
            out_type = str
        elif constant.startswith("ext-"):
            py_code = "['http://hl7.org/fhir/StructureDefinition/%s']" % constant[4:]
            out_type = str
        elif constant in self.var_types:
            py_code = self._handle_var(constant)
            out_type = self.var_types[constant]
        else:
            raise BaseException("Unkonwn external constant %s" % constant)
        parentNode["py_code"][ctx] = PythonCode(py_code, out_type)

    # patient.name.given.exists() -> bool([given for name in patient.name for given in name.given])

    # Enter a parse tree produced by fhirpathParser#memberInvocation.
    def enterMemberInvocation(self, ctx: fhirpathParser.MemberInvocationContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#memberInvocation.
    def exitMemberInvocation(self, ctx: fhirpathParser.MemberInvocationContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#functionInvocation.
    def enterFunctionInvocation(self, ctx: fhirpathParser.FunctionInvocationContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#functionInvocation.
    def exitFunctionInvocation(self, ctx: fhirpathParser.FunctionInvocationContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#thisInvocation.
    def enterThisInvocation(self, ctx: fhirpathParser.ThisInvocationContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#thisInvocation.
    def exitThisInvocation(self, ctx: fhirpathParser.ThisInvocationContext):
        node, parentNode, code = self._exit(ctx)
        if self.where_vars:
            where_var = self.where_vars[-1]
            code = where_var.code
            out_type = where_var.out_type
            single_val = where_var.single_val
        else:
            code = self.this
            out_type = self.var_types["$this"]
            single_val = True
        if single_val:
            parentNode["py_code"][ctx] = PythonCode("[%s]" % code, out_type)
        else:
            parentNode["py_code"][ctx] = PythonCode("%s" % code, out_type)

    # Enter a parse tree produced by fhirpathParser#indexInvocation.
    def enterIndexInvocation(self, ctx: fhirpathParser.IndexInvocationContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#indexInvocation.
    def exitIndexInvocation(self, ctx: fhirpathParser.IndexInvocationContext):
        node, parentNode, code = self._exit(ctx)
        where_var = self.where_vars[-3]
        code = where_var.code
        out_type = where_var.out_type
        parentNode["py_code"][ctx] = PythonCode("%s" % code, out_type)

    # Enter a parse tree produced by fhirpathParser#totalInvocation.
    def enterTotalInvocation(self, ctx: fhirpathParser.TotalInvocationContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#totalInvocation.
    def exitTotalInvocation(self, ctx: fhirpathParser.TotalInvocationContext):
        node, parentNode, code = self._exit(ctx)
        where_var = self.where_vars[-2]
        code = where_var.code
        out_type = where_var.out_type
        parentNode["py_code"][ctx] = PythonCode("%s" % code, out_type)

    # Enter a parse tree produced by fhirpathParser#function.
    def enterFunction(self, ctx: fhirpathParser.FunctionContext):
        self._enter(ctx)
        name = ctx.children[0].getText()
        if name in ["where", "select", "trace", "all", "repeat", "aggregate"]:
            context = next(iter(self.parentStack[-3]["py_code"].items()))[1]
            varname = self._get_new_varname()
            single_val = True
            if name == "aggregate":
                single_val = False
                varname_total = self._get_new_varname()
                varname_index = self._get_new_varname()
                self.where_vars.append(PythonCode(varname_total, context.out_type))
                self.where_vars.append(PythonCode(varname_index, context.out_type))
            self.where_vars.append(PythonCode(varname, context.out_type, single_val=single_val))

    # Exit a parse tree produced by fhirpathParser#function.
    def exitFunction(self, ctx: fhirpathParser.FunctionContext):
        node, parentNode, code = self._exit(ctx)
        function_name = code[0].code
        params = code[1] if len(code) > 1 else []
        if function_name == "lower":
            py_code = PythonCode("fhirpath_utils.lower(#)", str)
        elif function_name == "upper":
            py_code = PythonCode("fhirpath_utils.upper(#)", str)
        elif function_name == "substring":
            py_code = PythonCode("fhirpath_utils.substring(#,%s,%s)" % (params[0].code, params[1].code if len(params) > 1 else "[]"), str)
        elif function_name == "now":
            py_code = PythonCode("[one_timestamp]", datetime.datetime)
        elif function_name == "today":
            py_code = PythonCode("[one_timestamp.date()]", datetime.date)
        elif function_name == "timeOfDay":
            py_code = PythonCode("[one_timestamp.time()]", datetime.time)
        elif function_name == "exists":
            py_code = PythonCode("[bool(#)]", bool, list_func=True)
        elif function_name == "length":
            py_code = PythonCode("fhirpath_utils.strlength(#)", int)
        elif function_name == "toChars":
            py_code = PythonCode("fhirpath_utils.toChars(#)", str)
        elif function_name == "empty":
            py_code = PythonCode("[not(#)]", bool, list_func=True)
        elif function_name == "round":
            if len(params) > 0:
                py_code = PythonCode("fhirpath_utils.decimal_round(#, %s)" % params[0].code, decimal.Decimal, list_func=True)
            else:
                py_code = PythonCode("fhirpath_utils.decimal_round(#, [0])", decimal.Decimal, list_func=True)
        elif function_name == "truncate":
            py_code = PythonCode("fhirpath_utils.decimal_truncate(#)", int, list_func=True)
        elif function_name == "sqrt":
            py_code = PythonCode("fhirpath_utils.decimal_sqrt(#)", decimal.Decimal, list_func=True)
        elif function_name == "abs":
            py_code = PythonCode("fhirpath_utils.decimal_abs(#)", decimal.Decimal, list_func=True)
        elif function_name == "ceiling":
            py_code = PythonCode("fhirpath_utils.decimal_ceiling(#)", int, list_func=True)
        elif function_name == "floor":
            py_code = PythonCode("fhirpath_utils.decimal_floor(#)", int, list_func=True)
        elif function_name == "exp":
            py_code = PythonCode("fhirpath_utils.decimal_exp(#)", decimal.Decimal, list_func=True)
        elif function_name == "ln":
            py_code = PythonCode("fhirpath_utils.decimal_ln(#)", decimal.Decimal, list_func=True)
        elif function_name == "log":
            py_code = PythonCode("fhirpath_utils.decimal_log(#, %s)" % params[0].code, decimal.Decimal, list_func=True)
        elif function_name == "power":
            py_code = PythonCode("fhirpath_utils.decimal_power(#, %s)" % params[0].code, decimal.Decimal, list_func=True)
        elif function_name == "select":
            var = self.where_vars.pop()
            v1 = self._get_new_varname()
            where_code = params[0].code.replace("#", var.code)
            invocation = f"[{v1} for {var.code} in # for {v1} in {where_code}]"
            py_code = PythonCode(invocation, params[0].out_type, list_func=True)
        elif function_name == "where":
            var = self.where_vars.pop()
            where_code = params[0].code.replace("#", var.code)
            invocation = "[%s for %s in # if %s == [True]]" % (var.code, var.code, where_code)
            py_code = PythonCode(invocation, var.out_type, list_func=True)
        elif function_name == "all":
            var = self.where_vars.pop()
            where_code = params[0].code.replace("#", var.code)
            invocation = "[all(%s == [True] for %s in #)]" % (where_code, var.code)
            py_code = PythonCode(invocation, bool, list_func=True)
        elif function_name == "repeat":
            var = self.where_vars.pop()
            where_code = params[0].code
            py_code = PythonCode("fhirpath_utils.repeat(#, lambda %s: %s)" % (var.code, where_code), params[0].out_type, list_func=True)
        elif function_name == "aggregate":
            var = self.where_vars.pop()
            var_total = self.where_vars.pop()
            var_index = self.where_vars.pop()
            where_code = params[0].code
            init_code = params[1].code if len(params) > 1 else "{}"
            py_code = PythonCode("fhirpath_utils.aggregate(#, lambda %s, %s, %s: %s, %s)" % (var.code, var_index.code, var_total.code, where_code, init_code), params[0].out_type, list_func=True)
        elif function_name == "matches":
            py_code = PythonCode("fhirpath_utils.matches(#, %s)" % params[0].code, bool)
        elif function_name == "contains":
            py_code = PythonCode("fhirpath_utils.contains(#, %s)" % params[0].code, bool)
        elif function_name == "startsWith":
            py_code = PythonCode("fhirpath_utils.startswith(#, %s)" % params[0].code, bool)
        elif function_name == "endsWith":
            py_code = PythonCode("fhirpath_utils.endswith(#, %s)" % params[0].code, bool)
        elif function_name == "not":
            py_code = PythonCode("fhirpath_utils.bool_not(#)", bool, list_func=True)
        elif function_name == "skip":
            py_code = PythonCode("fhirpath_utils.skip(#, %s)" % params[0].code, "#", list_func=True)
        elif function_name == "take":
            py_code = PythonCode("fhirpath_utils.take(#, %s)" % params[0].code, "#", list_func=True)
        elif function_name == "first":
            py_code = PythonCode("fhirpath_utils.first(#)", "#", list_func=True)
        elif function_name == "last":
            py_code = PythonCode("fhirpath_utils.last(#)", "#", list_func=True)
        elif function_name == "tail":
            py_code = PythonCode("fhirpath_utils.tail(#)", "#", list_func=True)
        elif function_name == "children":
            py_code = PythonCode("fhirpath_utils.children(#)", None, list_func=True)
        elif function_name == "descendants":
            py_code = PythonCode("fhirpath_utils.descendants(#)", None, list_func=True)
        elif function_name == "count":
            py_code = PythonCode("[len(#)]", int, list_func=True)
        elif function_name == "allTrue":
            py_code = PythonCode("fhirpath_utils.allTrue(#)", bool, list_func=True)
        elif function_name == "allFalse":
            py_code = PythonCode("fhirpath_utils.allFalse(#)", bool, list_func=True)
        elif function_name == "anyTrue":
            py_code = PythonCode("fhirpath_utils.anyTrue(#)", bool, list_func=True)
        elif function_name == "anyFalse":
            py_code = PythonCode("fhirpath_utils.anyFalse(#)", bool, list_func=True)
        elif function_name == "iif":
            py_code = PythonCode("(%s if %s == [True] else %s)" % (params[1].code, params[0].code, params[2].code), "#", list_func=True)
        elif function_name == "union":
            py_code = PythonCode("fhirpath_utils.union(#, %s)" % params[0].code, params[0].out_type, list_func=True)
        elif function_name == "combine":
            py_code = PythonCode("# + %s" % params[0].code, params[0].out_type, list_func=True)
        elif function_name == "exclude":
            py_code = PythonCode("fhirpath_utils.exclude(#, %s)" % params[0].code, params[0].out_type, list_func=True)
        elif function_name == "single":
            py_code = PythonCode("fhirpath_utils.single(#)", "#", list_func=True)
        elif function_name == "trace":
            var = self.where_vars.pop()
            if len(params) > 0:
                v1 = self._get_new_varname()
                invocation = f"[{v1} for {var.code} in # for {v1} in {params[0].code}]"
                py_code = PythonCode("fhirpath_utils.trace(#, %s, %s)" % (params[0].code, invocation), "#", list_func=True)
            else:
                py_code = PythonCode("fhirpath_utils.trace(#, %s)" % (params[0].code), "#", list_func=True)
        elif function_name == "subsetOf":
            py_code = PythonCode("fhirpath_utils.subset_of(#, %s)" % params[0].code, bool, list_func=True)
        elif function_name == "supersetOf":
            py_code = PythonCode("fhirpath_utils.superset_of(#, %s)" % params[0].code, bool, list_func=True)
        elif function_name == "intersect":
            py_code = PythonCode("fhirpath_utils.intersect(#, %s)" % params[0].code, params[0].out_type, list_func=True)
        elif function_name == "distinct":
            py_code = PythonCode("fhirpath_utils.distinct(#)", "#", list_func=True)
        elif function_name == "isDistinct":
            py_code = PythonCode("fhirpath_utils.is_distinct(#)", bool, list_func=True)
        elif function_name == "toString":
            py_code = PythonCode("fhirpath_utils.toString(#)", str, list_func=True)
        elif function_name == "convertsToString":
            py_code = PythonCode("fhirpath_utils.convertsToString(#)", bool, list_func=True)
        elif function_name == "toBoolean":
            py_code = PythonCode("fhirpath_utils.toBoolean(#)", bool, list_func=True)
        elif function_name == "convertsToBoolean":
            py_code = PythonCode("fhirpath_utils.convertsToBoolean(#)", bool, list_func=True)
        elif function_name == "toInteger":
            py_code = PythonCode("fhirpath_utils.toInteger(#)", int, list_func=True)
        elif function_name == "convertsToInteger":
            py_code = PythonCode("fhirpath_utils.convertsToInteger(#)", bool, list_func=True)
        elif function_name == "toDecimal":
            py_code = PythonCode("fhirpath_utils.toDecimal(#)", decimal.Decimal, list_func=True)
        elif function_name == "convertsToDecimal":
            py_code = PythonCode("fhirpath_utils.convertsToDecimal(#)", bool, list_func=True)
        elif function_name == "toQuantity":
            py_code = PythonCode("fhirpath_utils.toQuantity(#, %s)" % (params[0].code if params else "[]"), getattr(self.o_module, "Quantity"), list_func=True)
        elif function_name == "convertsToQuantity":
            py_code = PythonCode("fhirpath_utils.convertsToQuantity(#, %s)" % (params[0].code if params else "[]"), bool, list_func=True)
        elif function_name == "toDate":
            py_code = PythonCode("fhirpath_utils.toDate(#)", getattr(self.o_module, "date"), list_func=True)
        elif function_name == "convertsToDate":
            py_code = PythonCode("fhirpath_utils.convertsToDate(#)", bool, list_func=True)
        elif function_name == "toDateTime":
            py_code = PythonCode("fhirpath_utils.toDateTime(#)", getattr(self.o_module, "dateTime"), list_func=True)
        elif function_name == "convertsToDateTime":
            py_code = PythonCode("fhirpath_utils.convertsToDateTime(#)", bool, list_func=True)
        elif function_name == "toTime":
            py_code = PythonCode("fhirpath_utils.toTime(#)", getattr(self.o_module, "time"), list_func=True)
        elif function_name == "convertsToTime":
            py_code = PythonCode("fhirpath_utils.convertsToTime(#)", bool, list_func=True)
        elif function_name == "type":
            py_code = PythonCode("fhirpath_utils.gettype(#)", None)
        elif function_name == "is":
            type_ = ctx.children[-2].getText().replace("`", "")
            py_code = PythonCode("fhirpath_utils.is_type(#, '%s')" % type_, bool, list_func=True)
        elif function_name == "as":
            type_ = ctx.children[-2].getText().replace("`", "")
            out_type = self.utils.gettype_fromspec(type_)
            py_code = PythonCode("fhirpath_utils.as_type(#, '%s')" % type_, out_type, list_func=True)
        elif function_name == "ofType":
            type_ = ctx.children[-2].getText().replace("`", "")
            out_type = self.utils.gettype_fromspec(type_)
            v1 = self._get_new_varname()
            py_code = PythonCode("[%s for %s in # if fhirpath_utils.is_type(%s, '%s') == [True]]" % (v1, v1, v1, type_), out_type, list_func=True)
        elif function_name == "conformsTo":
            py_code = PythonCode("fhirpath_utils.conformsTo(#, %s)" % params[0].code, bool, list_func=True)
        elif function_name == "extension":
            py_code = PythonCode("fhirpath_utils.extension(#, %s)" % params[0].code, getattr(self.o_module, "Extension"))
        elif function_name == "resolve":
            py_code = PythonCode("fhirpath_utils.resolve(#, [%s])" % ", ".join(self.resolve_ctx), None)
        else:
            raise NotImplementedError("Not implemented function %s" % function_name)
        parentNode["py_code"][ctx] = py_code

    # Enter a parse tree produced by fhirpathParser#paramList.
    def enterParamList(self, ctx: fhirpathParser.ParamListContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#paramList.
    def exitParamList(self, ctx: fhirpathParser.ParamListContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code

    # Enter a parse tree produced by fhirpathParser#quantity.
    def enterQuantity(self, ctx: fhirpathParser.QuantityContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#quantity.
    def exitQuantity(self, ctx: fhirpathParser.QuantityContext):
        node, parentNode, code = self._exit(ctx)
        number = ctx.children[0].getText()
        unit = code[0].code
        parentNode["py_code"][ctx] = PythonCode("[fhirpath_utils.SystemQuantity(value=%s.decimal(value=decimal.Decimal('%s')), %s)]" % (self.o_module.__name__, number, unit), self.utils.SystemQuantity)

    # Enter a parse tree produced by fhirpathParser#unit.
    def enterUnit(self, ctx: fhirpathParser.UnitContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#unit.
    def exitUnit(self, ctx: fhirpathParser.UnitContext):
        node, parentNode, code = self._exit(ctx)
        if code:
            parentNode["py_code"][ctx] = code[0]
        else:
            parentNode["py_code"][ctx] = PythonCode("code=%s.string(value=%s), system=%s.string('http://unitsofmeasure.org')" % (self.o_module.__name__, ctx.getText(), self.o_module.__name__), str)

    # Enter a parse tree produced by fhirpathParser#dateTimePrecision.
    def enterDateTimePrecision(self, ctx: fhirpathParser.DateTimePrecisionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#dateTimePrecision.
    def exitDateTimePrecision(self, ctx: fhirpathParser.DateTimePrecisionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("unit=%s.string(value='%s')" % (self.o_module.__name__, ctx.getText()), str)

    # Enter a parse tree produced by fhirpathParser#pluralDateTimePrecision.
    def enterPluralDateTimePrecision(self, ctx: fhirpathParser.PluralDateTimePrecisionContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#pluralDateTimePrecision.
    def exitPluralDateTimePrecision(self, ctx: fhirpathParser.PluralDateTimePrecisionContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode("unit=%s.string(value='%s')" % (self.o_module.__name__, ctx.getText()), str)

    # Enter a parse tree produced by fhirpathParser#typeSpecifier.
    def enterTypeSpecifier(self, ctx: fhirpathParser.TypeSpecifierContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#typeSpecifier.
    def exitTypeSpecifier(self, ctx: fhirpathParser.TypeSpecifierContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = code[0]

    # Enter a parse tree produced by fhirpathParser#qualifiedIdentifier.
    def enterQualifiedIdentifier(self, ctx: fhirpathParser.QualifiedIdentifierContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#qualifiedIdentifier.
    def exitQualifiedIdentifier(self, ctx: fhirpathParser.QualifiedIdentifierContext):
        node, parentNode, code = self._exit(ctx)
        parentNode["py_code"][ctx] = PythonCode(".".join([c.code for c in code]), None)

    # Enter a parse tree produced by fhirpathParser#identifier.
    def enterIdentifier(self, ctx: fhirpathParser.IdentifierContext):
        self._enter(ctx)

    # Exit a parse tree produced by fhirpathParser#identifier.
    def exitIdentifier(self, ctx: fhirpathParser.IdentifierContext):
        node, parentNode, code = self._exit(ctx)
        var = ctx.getText()
        var_type = self.var_types.get(var)
        if isinstance(ctx.parentCtx, fhirpathParser.MemberInvocationContext) and \
                len(ctx.parentCtx.parentCtx.children) > 1 and \
                ctx.parentCtx.parentCtx.children[-1] == ctx.parentCtx:
            var = var.replace("`", "")
            if var == "type":
                var = "type_"
        elif isinstance(ctx.parentCtx, fhirpathParser.FunctionContext) and \
                ctx.parentCtx.children[0] == ctx:
            pass
        elif isinstance(ctx.parentCtx, fhirpathParser.QualifiedIdentifierContext):
            var_type = None
        else:
            if self.where_vars:
                where_var = self.where_vars[-1]
                out_type = where_var.out_type
                var_type, __ = utils._get_type(out_type, var)
                var = "fhirpath_utils.get(%s,'%s')" % (where_var.code, var.replace("`", ""))
            elif var in self.this_elem:
                var_type, __ = self.this_elem[var]
                var = "fhirpath_utils.get(%s,'%s')" % (self.this, var.replace("`", ""))
            else:
                var = self._handle_var(var.replace("`", ""))
        parentNode["py_code"][ctx] = PythonCode(var, var_type)

    def _handle_var(self, var):
        var_type = self.var_types.get(var)
        if var_type:
            self.resolve_ctx.add(var)
            if var_type.__name__ == "dateTime":
                var = "dateutil.parser.parse(str(%s))" % var
            elif var_type.__name__ == "dateStringV3":
                var = "str(dateutil.parser.parse(%s).isoformat())" % var
            if var_type != list:
                if var not in self.list_var:
                    var = "[%s]" % var
        else:
            var = "[]"
        return var

    def _enter(self, ctx):
        parentNode = self.parentStack[-1]
        node = {"py_code": OrderedDict()}
        for child in ctx.children:
            if not isinstance(child, TerminalNodeImpl):
                node["py_code"][child] = PythonCode("", None)
        self.parentStack.append(node)

    def _exit(self, ctx):
        node = self.parentStack.pop()
        parentNode = self.parentStack[-1]
        code = []
        for child in ctx.children:
            if not isinstance(child, TerminalNodeImpl):
                code.append(node["py_code"][child])
        return node, parentNode, code

    def _get_new_varname(self):
        self.counter += 1
        return "v" + str(self.counter)

