# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1+20240516-alpha]

### Changed

- Update readme and args
- Quickfix dateString handling with workaround, will be solved properly with malac-hd#40

## [0.1.0+20240502-alpha]

### Added

- Create first feature set of alpha release

### Changed

- Changed some external mappings here for full compatibility

### Fixed

- Fixed some bugs in output of generateDS (generateDS fixes will come)

### Security

- Added the default gitlab SAST pipeline