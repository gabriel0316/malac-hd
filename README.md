# MaLaC-HD

MaLaC-HD (MApping LAnguage Compiler for Heath Data) is intended to integrate and improve the results of some preliminary projects (i.e. https://github.com/HL7Austria/CDA2FHIR), focusing on transformation speed and easy extensibility after compilation/convertion. MaLaC-HD is not limited to the translation of CDA to FHIR, it can be used to transform different input formats to different output formats, using a conversion/mapping rule language and the XSD schemas of the input and output format. It also supports the additional implementation of further conversion/mapping rule languages, beside of the currently supported FHIR Mapping Language (FML). 

## Pursued Objectives
The development of MaLaC-HD focuses on the following three main objectives and their respective sub-objectives:

- The compilation of the conversion/mapping rules by MaLaC-HD and the resulting mapping of these rules in a common programming language must be easily readable and directly related to the conversion/mapping rules.
  - The conversion/mapping rules must be ballotable so that they can be queried and discussed for completeness and correctness as part of a guideline or even as a standalone part in a community, such as the HL7 community.
- MaLaC-HD must not be dependent on any conversion rule language in order to be able to easily process conversion/mapping rules that can be mapped in different conversion rule languages.
  - In particular, the limits of the respective conversion rule language require simple narrative extensibility in order to draw attention to the fact that further code must be added manually in the respective generated code by means of placeholders.
- The result of compiling the conversion/mapping rules must be trimmed for speed and stability so as not to add any obstructive delays.
  - The compiled conversion/mapping rules should be executable as stand-alone program code without MaLaC-HD.

## Getting Started

These instructions will get you a copy of the project up and running.

### Prerequisites

Install the requirements with [pip](https://pip.pypa.io/en/stable/installation/) out of the root directory of the project:

```
pip install -r requirements.txt
```

#### Preparing your FML maps

As the direct support of FML will come with the beta version this fall, a workaround has to be done right now in the alpha, by pre-compiling/converting the FML to StructreMaps and ConceptMaps by using the [java-validator](https://www.hl7.org/fhir/validator/), i.e.:
```
java -jar .\validator_cli.jar -ig ./tests/structuremap/aut_lab/cdaToBundle.map -output tests/structuremap/aut_lab/cda-2-bundle-merged-transform.4.fhir.xml -compile http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureMap/at-cda-to-bundle -version 4.0
```

### Using MaLaC-HD

with `python malac_hd.py --help` you will get an overview:

```
____________________ MaLaC-HD 0.1.1+20240516-alpha started ____________________
usage: malac_hd.py [-h] -m MAPPING [-co CONVERT_OUTPUT] [-ti TRANSFORM_INPUT] [-to TRANSFORM_OUTPUT] [-s]

This is the MApping LAnguage Compiler for Health Data, short MaLaC-HD. We differentiate between two modes, converting and transforming. The convertion is compiling a given mapping to python code, that itself can be run with its own argument handling for transforming inputs. Additionally, the transformation can   
also be be done by MaLaC-HD directly after convertion, i.e. for direct testing purposes.

options:
  -h, --help            show this help message and exit
  -m MAPPING, --mapping MAPPING
                        the mapping file path, the conversion/mapping rule language is detected by file ending, right now only StructureMaps and ConceptMaps can be given as mappings
  -co CONVERT_OUTPUT, --convert_output CONVERT_OUTPUT
                        the conversion python file path, if not given, saved in the working directory with the map-file name
  -ti TRANSFORM_INPUT, --transform_input TRANSFORM_INPUT
                        the transformation input file path, the ressource type is detected by its root node inside the xml
  -to TRANSFORM_OUTPUT, --transform_output TRANSFORM_OUTPUT
                        the transformation output file path, the ressource type is detected by its root node inside the xml
  -s, --silent          do not print the converted python mapping to console
```

#### Example Usage 

Usage of an in MaLaC-HD included example:
```
python malac_hd.py -m ./tests/structuremap/aut_lab/cda-2-bundle-merged-transform.4.fhir.xml -co ./tests/structuremap/aut_lab/out/bundle.4.fhir.xml.py -i ./tests/structuremap/aut_lab/Lab_Allgemeiner_Laborbefund.at.cda.xml -to ./tests/structuremap/aut_lab/out/bundle.4.fhir.xml
```

## Support
Please use our gitlab issues for questions or support.

## Authors and acknowledgment
We want to thank
- [ELGA GmbH](https://www.elga.gv.at/) with their [CDA2FHIR](https://collab.hl7.at/display/BAL/AG+ELGA+CDA+Laborbefund+zu+FHIR) projects and
- [AIT Austrian Institute of Technology GmbH](https://www.ait.ac.at/) with their [SmartFOX](https://www.smart-fox.at/) project.

## License
This is a LGPL licensed project, with a small add on that any usage of this project or results of this project should contain a for the consumer visible icon, that will be announced here in the near future.