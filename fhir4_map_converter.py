#!/usr/bin/env python

#
# Generated Fri Dec  1 12:48:08 2023 by generateDS.py version 2.43.3.
# Python 3.10.2 (tags/v3.10.2:a58ebcc, Jan 17 2022, 14:12:15) [MSC v.1929 64 bit (AMD64)]
#
# Command line options:
#   ('-o', 'fhir4.py')
#   ('-s', 'fhir4_map_converter.py')
#
# Command line arguments:
#   fhir-single.xsd
#
# Command line:
#   .\generateDS.py -o "fhir4.py" -s "fhir4_map_converter.py" fhir-single.xsd
#
# Current working directory (os.getcwd()):
#   generateDS-2.43.3
#

import os
import sys
from lxml import etree as etree_
import builtins
import re
import inspect
import typing
import datetime
import ast
import re
import decimal
import json

from antlr4 import *
from antlr4.tree.Tree import ParseTreeWalker
from antlr4.error.ErrorListener import ErrorListener
from antlr4.error.Errors import LexerNoViableAltException

import fhir4 as supermod
import malac_hd
from fhirpath.r4.listener import PythonListener
from fhirpath.r4.generated.fhirpathLexer import fhirpathLexer as FHIRPathLexer
from fhirpath.r4.generated.fhirpathParser import fhirpathParser as FHIRPathParser
import utils


def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Globals
#

ExternalEncoding = ''
SaveElementTreeNode = True

#
# Data representation classes
# All of these that do not have any mapping information, will not be found here
#

class StructureMapSub(supermod.StructureMap, malac_hd.ConvertMaster):

    additional_header = ""
    uuid_method = "str(uuid.uuid4())"
    now_method = "datetime.now()"

    def __init__(self, id=None, meta=None, implicitRules=None, language=None, text=None, contained=None, extension=None, modifierExtension=None, url=None, identifier=None, version=None, name=None, title=None, status=None, experimental=None, date=None, publisher=None, contact=None, description=None, useContext=None, jurisdiction=None, purpose=None, copyright=None, structure=None, import_=None, group=None, **kwargs_):
        super(StructureMapSub, self).__init__(id, meta, implicitRules, language, text, contained, extension, modifierExtension, url, identifier, version, name, title, status, experimental, date, publisher, contact, description, useContext, jurisdiction, purpose, copyright, structure, import_, group,  **kwargs_)

        self.var_types = {}
        self.none_check = set()
        self.list_var = set()
        self.list_var_resolved = set()
        self.group_types = {}
        self.default_types_maps = {}
        self.default_types_maps_subtypes = {}
        self.o_module = None
        self.i_module = None
        self.py_code = ""
        self.indent = 0
        self.transform_default_needed = False
        self.aliases = {}
        self.all_groups = []

    def convert(self, silent=True, source=".", standalone=True):
        for structure in self.get_structure():
            s = structure.get_url().value
            r_name = s[s.rfind('/') + 1:]
            if structure.get_alias():
                self.aliases[structure.get_alias().value] = r_name
            else:
                self.aliases[r_name] = r_name

        if standalone:
            first_group = self.get_group()[0]
            for i in first_group.get_input():
                t = i.get_type().value
                mod = None
                for structure in self.get_structure():
                    s = structure.get_url().value
                    if s == t or (structure.get_alias() and structure.get_alias().value == t):
                        mod = __import__(malac_hd.list_i_o_modules[s[:s.rfind('/')]])
                if not mod:
                    for structure in self.get_structure():
                        s = structure.get_url().value
                        s_short = s[s.rfind('/') + 1:]
                        if s_short == t:
                            mod = __import__(malac_hd.list_i_o_modules[s[:s.rfind('/')]])
                if not mod:
                    raise BaseException("Could find in/out module")
                if i.get_mode().value == "source":        
                    self.i_module = mod
                else:
                    self.o_module = mod

        self.group_types.clear()
        self.default_types_maps.clear()
        self.transform_default_needed = False
        self.all_groups += self.get_group()

        self.py_code = ""
        imported_py_code = ""
        if standalone:
            self._header()

        # add all the translates of the none, one or multiple conceptMaps of this strucutreMap
        self.translate_dict_py_code = ""
        for contained in self.get_contained():
            if tmp := contained.get_ConceptMap():
                self.translate_dict_py_code += "\n"
                tmp.convert(silent=True, return_header_and_footer_for_standalone=False, return_translate_def = False, return_dict_add=True)
                self.translate_dict_py_code += tmp.py_code

        for import_ in self.get_import():
            # search all files in the same directory for the url
            url = import_.value
            dirname = os.path.dirname(source)
            basename = os.path.basename(source)
            ext = basename[basename.index("."):]
            found = False
            for filename in os.listdir(dirname):
                if filename.endswith(ext):
                    path = dirname + "/" + filename
                    try:
                        imported = parse(path, silence=silent)
                    except:
                        continue
                    if imported.get_url() and imported.get_url().value == url:
                        imported.i_module = self.i_module
                        imported.o_module = self.o_module
                        imported_py_code += imported.convert(silent=silent, source=path, standalone=False)
                        self.translate_dict_py_code += imported.translate_dict_py_code
                        self.all_groups += imported.get_group()
                        self.default_types_maps.update(imported.default_types_maps)
                        self.default_types_maps_subtypes.update(imported.default_types_maps_subtypes)
                        found = True
                        break
            if not found:
                raise BaseException("Could not import %s, file not found." % url)

        for s_map_group in self.get_group():
            if not s_map_group.typeMode or s_map_group.typeMode.value == "none":
                pass
            elif s_map_group.typeMode.value == "types":
                group_name = s_map_group.name.value
                types = []
                for s_map_input in s_map_group.get_input():
                    type_name = s_map_input.get_type().value
                    if s_map_input.get_mode().value == "target":
                        clazz = getattr(self.o_module, type_name)
                    elif s_map_input.get_mode().value == "source":
                        try:
                            clazz = getattr(self.i_module, type_name)
                        except BaseException:
                            type_name = "POCD_MT000040_" + type_name  # TODO avoid this
                            clazz = getattr(self.i_module, type_name)
                    types.append(clazz)
                if len(types) != 2:
                    raise BaseException(f"Invalid default group: {group_name} does not have 2 parameters.")
                self.default_types_maps[tuple(types)] = group_name
            else:
                raise BaseException(f"Unsupported typeMode {s_map_group.typeMode.value}")

        for (a_src_type, a_trg_type), a_group_name in self.default_types_maps.items():
            for (b_src_type, b_trg_type), b_group_name in self.default_types_maps.items():
                if a_src_type != b_src_type and a_trg_type == b_trg_type:
                    if issubclass(a_src_type, b_src_type):
                        if a_group_name not in self.default_types_maps_subtypes:
                            self.default_types_maps_subtypes[a_group_name] = []
                        self.default_types_maps_subtypes[a_group_name].append((b_src_type, b_group_name))

        group_queue = list(self.get_group())
        postponed = []
        last_postponed = []
        failed_groups = []
        skipped_groups = []
        while group_queue:
            while group_queue:
                s_map_group = group_queue.pop(0)
                converted = self._handle_group(s_map_group)
                if not converted:
                    postponed.append(s_map_group)
            if postponed:
                if len(postponed) == len(last_postponed):
                    for p in postponed:
                        if p.name.value in self.group_types:
                            failed_groups.append(p)
                        else:
                            skipped_groups.append(p)
                    postponed = []
                group_queue = list(postponed)
                last_postponed = postponed
                postponed = []

        # add translate with its dictionary
        if standalone:
            self.py_code += ConceptMapSub().convert(silent=True, return_header_and_footer_for_standalone=False, return_translate_def=True, return_dict_add=False)
            self.py_code += self.translate_dict_py_code
        
        self.py_code += imported_py_code
        if standalone:
            self._footer()

        if not silent:
            print("\n%s" % self.py_code)

        if failed_groups:
            group_names = ", ".join([p.name.value for p in failed_groups])
            raise BaseException(
                f"Too little type information. Please provide types for groups on one or more of these groups: {group_names}")
        elif skipped_groups:
            group_names = ", ".join([p.name.value for p in skipped_groups])
            print(f"Skipped not called groups because of missing type info: {group_names}", file=sys.stderr)

        return self.py_code

    def _header(self):
        self.py_code += '''
import sys
import argparse
import time
import uuid
import re
import io
import json
from datetime import datetime
import dateutil.parser
from html import escape as html_escape
import utils
'''
        self.py_code += "import "+self.i_module.__name__+"\n"
        if self.o_module.__name__ != self.i_module.__name__:
            self.py_code += "import "+self.o_module.__name__+"\n"
        if getattr(self.o_module, "string", None):
            self.py_code += "from "+self.o_module.__name__+" import string, base64Binary, markdown, code, dateTime, uri, boolean, decimal\n"  # TODO check if there is a better solution
        elif getattr(self.i_module, "string", None):
            self.py_code += "from "+self.i_module.__name__+" import string, base64Binary, markdown, code, dateTime, uri, boolean, decimal\n"  # TODO check if there is a better solution
        self.py_code += '''
fhir4.Resource.exportJsonAttributes = utils.exportJsonAttributesResource
fhir4.ResourceContainer.exportJsonResult = utils.exportJsonResultResourceContainer
fhir4.Narrative.exportJsonResult = utils.exportJsonResultNarrative
fhir4.Element.exportJsonResult = utils.exportJsonResultElement

'''
        self.py_code += "description_text = \"This has been compiled by the MApping LAnguage Compiler for Health Data, short MaLaC-HD. See arguments for more details.\"\n"
        self.py_code += "one_timestamp = %s\n" % self.now_method
        self.py_code += "fhirpath_utils = utils.FHIRPathUtils(%s)\n" % (self.o_module if getattr(self.o_module, "string", None) else self.i_module).__name__ # TODO has to be some FHIR module 
        self.py_code += "shared_vars = {}\n"
        self.py_code += "\n"
        self.py_code += self.additional_header

        self.py_code += "def init_argparse() -> argparse.ArgumentParser:\n"
        self.py_code += "    parser = argparse.ArgumentParser(description=description_text)\n"
        self.py_code += "    parser.add_argument(\n"
        self.py_code += "       '-s', '--source', help='the source file path', required=True\n"
        self.py_code += "    )\n"
        self.py_code += "    parser.add_argument(\n"
        self.py_code += "       '-t', '--target', help='the target file path the result will be written to', required=True\n"
        self.py_code += "    )\n"
        self.py_code += "    return parser\n"
        self.py_code += "\n"

        self.py_code += "def transform(source_path, target_path):\n"
        self.py_code += "    start = time.time()\n"
        self.py_code += "    print('+++++++ Transformation from '+source_path+' to '+target_path+' started +++++++')\n"
        self.py_code += "\n"

        s_map_group = self.get_group()[0]
        group_name = s_map_group.get_name().value
        for s_map_input in s_map_group.get_input():
            if s_map_input.get_mode().value == "target":
                target = s_map_input.get_type().value
                target_name = s_map_input.get_name().value
            if s_map_input.get_mode().value == "source":
                source_name = s_map_input.get_name().value
        self.py_code += "    if source_path.endswith('.xml'):\n"
        self.py_code += "        " + source_name + " = "+self.i_module.__name__+".parse(source_path, silence=True)\n"
        if self.i_module.__name__.startswith("fhir"):
            self.py_code += "    elif source_path.endswith('.json'):\n"
            self.py_code += "        with open(source_path, 'r', newline='', encoding='utf-8') as f:\n"
            self.py_code += "            " + source_name + " = utils.parse_json(" + self.i_module.__name__ + ", json.load(f))\n"
        self.py_code += "    else:\n"
        self.py_code += "        raise BaseException('Unknown source file ending')\n"
        try:
            clazz = getattr(self.o_module, target)
        except BaseException:
            target = "POCD_MT000040_" + target  # TODO avoid this
            clazz = getattr(self.o_module, target)
        self.py_code += "    " + target_name + " = "+self.o_module.__name__+"."+target+"()\n"
        self.py_code += "    "+group_name+"("+source_name+", "+target_name+")\n"
        self.py_code += "    with open(target_path, 'w', newline='', encoding='utf-8') as f:\n"
        self.py_code += "        if target_path.endswith('.xml'):\n"
        self.py_code += "            f.write('<?xml version=\"1.0\" encoding=\"UTF-8\"?>\\n')\n"
        self.py_code += "            "+ target_name+ ".export(f, 0, namespacedef_='xmlns=\"http://hl7.org/fhir\" xmlns:v3=\"urn:hl7-org:v3\"')\n"
        if self.o_module.__name__.startswith("fhir"):
            self.py_code += "        elif target_path.endswith('.json'):\n"
            self.py_code += "            json.dump("+ target_name +".exportJson(), f)\n"
        self.py_code += "        else:\n"
        self.py_code += "            raise BaseException('Unknown target file ending')\n"
        self.py_code += "\n"
        self.py_code += "    print('altogether in '+str(round(time.time()-start,3))+' seconds.')\n"
        self.py_code += "    print('+++++++ Transformation from '+source_path+' to '+target_path+' ended  +++++++')\n"

    def _footer(self):
        self.py_code += "\ndef unpack_container(resource_container):\n"
        cclass = getattr(self.o_module, "ResourceContainer", getattr(self.i_module, "ResourceContainer", None))
        for name, elem_type in inspect.get_annotations(cclass.__init__).items():
            if name != "gds_collector_":
                self.py_code += "    if resource_container.%s is not None:\n" % name
                self.py_code += "        return resource_container.%s\n" % name
        self.py_code += "    return None\n"

        if self.transform_default_needed:
            self.py_code += "\n"
            self.py_code += "default_types_maps = {\n"
            for types, name in self.default_types_maps.items():
                self.py_code += "    (%s.%s, %s.%s): %s,\n" % (types[0].__module__, types[0].__name__, types[1].__module__, types[1].__name__, name)
            self.py_code += "}\n"
            self.py_code += "\n"
            self.py_code += "def transform_default(source, target, target_type=None):\n"
            self.py_code += "    target_type = target_type or type(target)\n"
            self.py_code += "    default_map = default_types_maps.get((type(source), target_type))\n"
            self.py_code += "    if default_map:\n"
            self.py_code += "        default_map(source, target)\n"
            self.py_code += "    else:\n"
            self.py_code += "        raise BaseException('No default transform found for %s -> %s' % (type(source), target_type))\n"

        self.py_code += '''
if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    transform(args.source, args.target)
'''

    def _handle_group(self, s_map_group):
        self.indent = 0
        self.var_types.clear()
        self.none_check.clear()
        self.list_var.clear()
        self.list_var_resolved.clear()
        group_name = s_map_group.get_name().value
        names = []
        types = []
        for s_map_input in s_map_group.get_input():
            var_name = s_map_input.get_name().value
            type_name = s_map_input.get_type().value if s_map_input.get_type() else None
            if not type_name:
                if group_name in self.group_types and var_name in self.group_types[group_name]:  # lookup type from calls
                    clazz = self.group_types[group_name][var_name]
                else:
                    return False
            elif s_map_input.get_mode().value == "target":
                type_name = self.aliases.get(type_name, type_name).replace("-", "_")
                try:
                    clazz = getattr(self.o_module, type_name)
                except BaseException:
                    type_name = "POCD_MT000040_" + type_name  # TODO avoid this
                    clazz = getattr(self.o_module, type_name)
            elif s_map_input.get_mode().value == "source":
                type_name = self.aliases.get(type_name, type_name).replace("-", "_")
                try:
                    clazz = getattr(self.i_module, type_name)
                except BaseException:
                    type_name = "POCD_MT000040_" + type_name  # TODO avoid this
                    clazz = getattr(self.i_module, type_name)
            names.append(var_name)
            types.append(clazz.__module__ + "." + clazz.__name__)
            self.var_types[var_name] = clazz

        self._append_py()
        self._append_py("def %s(%s):" % (group_name, ", ".join(names)))
        self.indent = 4
        rule_queue = list(s_map_group.get_rule())

        if s_map_group.get_extends():
            base_name = s_map_group.get_extends().value
            for s_map_group_2 in self.get_group():
                if s_map_group_2.get_name().value == base_name:
                    base_group = s_map_group_2
                    break
            base_names = []
            for s_map_input in base_group.get_input():
                base_names.append(s_map_input.get_name().value)
            if len(names) > len(base_names):
                self._append_py("%s(%s)" % (base_name, ", ".join(base_names)))
                self._save_group_types(base_group.name.value, base_names)
            else:
                self._append_py("%s(%s)" % (base_name, ", ".join(names)))
                self._save_group_types(base_group.name.value, names)
        elif rule_queue == []:
            self._append_py("pass")

        while rule_queue != []:
            s_map_rule = rule_queue.pop(0)
            if isinstance(s_map_rule, int):
                self.indent += s_map_rule
                continue
            elif isinstance(s_map_rule, tuple):
                self.list_var.add(s_map_rule[0])
                self.list_var_resolved.discard(s_map_rule[0])
                self.none_check.discard(s_map_rule[0])
                self._append_py("%s = %s" % s_map_rule)
                continue
            elif isinstance(s_map_rule, str):
                self.none_check.add(s_map_rule)
                self.list_var_resolved.discard(s_map_rule)
                continue

            add_indent = 0
            remove_indent = 0
            checked_none = []
            resolved_list = []

            for s_source in s_map_rule.get_source():
                ctx = s_source.get_context()
                if ctx and ctx.value in self.none_check:
                    self._append_py("if %s:" % ctx.value)
                    self.none_check.discard(ctx.value)
                    checked_none.append(ctx.value)
                    remove_indent += -4
                    self.indent += 4
                elif ctx and ctx.value in self.list_var:
                    self._append_py("for _%s in %s:" % (ctx.value, ctx.value))
                    self.var_types["_%s" % ctx.value] = self.var_types.get(ctx.value, None)
                    self.list_var.discard(ctx.value)
                    self.list_var_resolved.add(ctx.value)
                    resolved_list.append(ctx.value)
                    remove_indent += -4
                    self.indent += 4
                if s_source.get_variable() or s_source.get_condition():
                    if s_map_rule.get_target() or s_map_rule.get_rule():
                        indents = self._handle_source(s_source)
                    else:
                        indents = self._handle_source_no_rules(s_source)
                    add_indent += indents[0]
                    remove_indent += indents[1]

            for s_map_target in s_map_rule.get_target():
                transform = s_map_target.get_transform()
                # all values from http://hl7.org/fhir/R4B/valueset-map-transform.html have to be handled
                ctx = s_map_target.get_context()
                target_indent = 0
                target_checked = []
                target_resolved = []
                target_after = []
                out_var_after = False
                out_var_insert = None
                if ctx and ctx.value in self.none_check:
                    self._append_py("if %s:" % ctx.value)
                    self.none_check.discard(ctx.value)
                    target_checked.append(ctx.value)
                    if s_map_target.get_variable():
                        target_after.insert(0, (" " * self.indent) + "    %s = None" % s_map_target.get_variable().value)
                        target_after.insert(0, (" " * self.indent) + "else:")
                        target_checked.append(s_map_target.get_variable().value)
                    target_indent += -4
                    self.indent += 4
                elif ctx and ctx.value in self.list_var:
                    if s_map_target.get_variable():
                        self._append_py("%s = []" % (s_map_target.get_variable().value))
                        self.list_var.discard(s_map_target.get_variable().value)
                        self.list_var_resolved.add(s_map_target.get_variable().value)
                        target_resolved.append(s_map_target.get_variable().value)
                        out_var_insert = (" " * self.indent) + "    %s.append(_%s)" % (s_map_target.get_variable().value, s_map_target.get_variable().value)
                        target_after.insert(0, out_var_insert)
                        out_var_after = True
                    self._append_py("for _%s in %s:" % (ctx.value, ctx.value))
                    self.var_types["_%s" % ctx.value] = self.var_types.get(ctx.value, None)
                    self.list_var.discard(ctx.value)
                    self.list_var_resolved.add(ctx.value)
                    target_resolved.append(ctx.value)
                    target_indent += -4
                    self.indent += 4
                if transform != "append":
                    for param in s_map_target.get_parameter():
                        if param.valueId and param.valueId.value in self.none_check:
                            self.none_check.discard(param.valueId.value)
                            target_checked.append(param.valueId.value)
                            if s_map_target.get_variable():
                                target_after.insert(0, (" " * self.indent) + "    %s = None" % self._get_out_var(s_map_target.get_variable().value))
                                target_after.insert(0, (" " * self.indent) + "else:")
                                target_checked.append(s_map_target.get_variable().value)
                            target_indent += -4
                            self.indent += 4
                        elif param.valueId and param.valueId.value in self.list_var:
                            if s_map_target.get_variable() and not out_var_after:
                                self._append_py("%s = []" % (s_map_target.get_variable().value))
                                self.list_var.discard(s_map_target.get_variable().value)
                                self.list_var_resolved.add(s_map_target.get_variable().value)
                                target_resolved.append(s_map_target.get_variable().value)
                                if out_var_after:
                                    target_after.remove(out_var_insert)
                                out_var_insert = (" " * self.indent) + "    %s.append(_%s)" % (s_map_target.get_variable().value, s_map_target.get_variable().value)
                                target_after.insert(0, out_var_insert)
                                out_var_after = True
                            self._append_py("for _%s in %s:" % (param.valueId.value, param.valueId.value))
                            self.var_types["_%s" % param.valueId.value] = self.var_types.get(param.valueId.value, None)
                            self.list_var.discard(param.valueId.value)
                            self.list_var_resolved.add(param.valueId.value)
                            target_resolved.append(param.valueId.value)
                            target_indent += -4
                            self.indent += 4
                if s_map_target.get_listMode() and (transform is not None or s_map_target.get_listMode()[0].value != "share"):
                    raise NotImplementedError("Target list mode '%s' not implemented yet (for transform %s)" % (s_map_target.get_listMode()[0].value, transform))
                if transform is None:
                    self._handle_target_assign(s_map_target)
                elif transform.value == "cast":
                    self._handle_target_cast(s_map_target)
                elif transform.value == "uuid":
                    self._handle_target_uuid(s_map_target)
                elif transform.value == "append":
                    self._handle_target_append(s_map_target)
                elif transform.value == "copy":
                    self._handle_target_copy(s_map_target)
                elif transform.value == "evaluate":
                    self._handle_target_evaluate(s_map_target)
                elif transform.value == "create":
                    self._handle_target_create(s_map_rule.get_source()[0], s_map_target, s_map_rule.get_dependent())
                elif transform.value == "reference":
                    self._handle_target_reference(s_map_target)
                elif transform.value == "truncate":
                    self._handle_target_truncate(s_map_target)
                elif transform.value == "pointer":
                    self._handle_target_pointer(s_map_target)
                elif transform.value == "cc":
                    self._handle_target_cc(s_map_target)
                elif transform.value == "c":
                    self._handle_target_c(s_map_target)
                elif transform.value == "id":
                    self._handle_target_id(s_map_target)
                elif transform.value == "qty":
                    self._handle_target_qty(s_map_target)
                elif transform.value == "cp":
                    self._handle_target_cp(s_map_target)
                elif transform.value == "translate":
                    self._handle_target_translate(s_map_target)
                elif transform.value in ["pointer", "cc", "c"]:
                    raise NotImplementedError("Transform '%s' is not yet implemented" % transform.value)
                elif transform.value in ["escape", "dateop"]:
                    raise NotImplementedError("Transform '%s' is not yet supported" % transform.value)
                else:
                    raise BaseException("Unknown transform '%s'" % transform.value)
                self.indent += target_indent
                for target in target_checked:
                    self.none_check.add(target)
                    self.list_var.discard(target)
                for target in target_resolved:
                    self.none_check.discard(target)
                    self.list_var.add(target)
                    self.list_var_resolved.discard(target)
                for target in target_after:
                    self.py_code += target + "\n"

            for d in s_map_rule.get_dependent():
                target_indent = 0
                target_checked = []
                target_resolved = []
                d_name = d.name.value
                var_names = [v.value for v in d.get_variable()]
                for var_name in var_names:
                    if var_name in self.none_check:
                        self.none_check.discard(var_name)
                        target_checked.append(var_name)
                        target_indent += -4
                        self.indent += 4
                    elif var_name in self.list_var:
                        self._append_py("for _%s in %s:" % (var_name, var_name))
                        self.var_types["_%s" % var_name] = self.var_types.get(var_name, None)
                        self.list_var.discard(var_name)
                        self.list_var_resolved.add(var_name)
                        target_resolved.append(var_name)
                        target_indent += -4
                        self.indent += 4
                self._append_py("%s(%s)" % (d_name, ", ".join([self._get_out_var(var_name) for var_name in var_names])))
                self._save_group_types(d_name, var_names)
                self.indent += target_indent
                for target in target_checked:
                    self.none_check.add(target)
                    self.list_var.discard(target)
                for target in target_resolved:
                    self.none_check.discard(target)
                    self.list_var.add(target)
                    self.list_var_resolved.discard(target)

            rule_queue = [add_indent] + s_map_rule.get_rule() + [remove_indent] + checked_none + resolved_list + rule_queue

        return True

    def _save_group_types(self, group_name, variables):
        group_vars = None
        for s_map_group in self.all_groups:
            if s_map_group.get_name().value == group_name:
                group_vars = [i.get_name().value for i in s_map_group.get_input()]
                if len(variables) != len(group_vars):
                    raise BaseException(f"Invalid group call {group_name}: parameters do not match.")
                break
        if not group_vars:
            raise BaseException(f"Invalid group call {group_name}: group not found.")
        group_types = self.group_types.get(group_name, {})
        idx = 0
        for v in variables:
            if v in self.var_types:
                group_types[group_vars[idx]] = self.var_types[v]
            idx += 1
        self.group_types[group_name] = group_types

    def _handle_condition(self, condition, s_source):
        result = self._handle_fhirpath(condition, None, s_source, condition=True)
        return result[0]

    def _handle_source(self, s_source):
        indent = 0
        if s_source.get_variable():
            src = self._path(s_source)
            list_mode = s_source.get_listMode()
            in_var = s_source.get_variable().value
            s_type, is_list = self._get_type(s_source, set_var_type=False)
            if not s_type and s_source.get_element() and s_source.get_element().value in ["dataString", "other", "dataBase64Binary", "xmlText"]:
                # fallback for EN, ...
                s_source.get_element().value = "valueOf_"
                src = self._path(s_source) + ".strip()"
                s_type = str
            self.var_types[in_var] = s_type
            indent += 4
            if s_type and not is_list:
                if in_var == "vvv":
                    s_source.get_variable().value = src
                    self._append_py("if %s:" % src)
                    self.var_types[src] = s_type
                else:
                    self._append_py("%s = %s" % (in_var, src))
                    self._append_py("if %s:" % (in_var))
            elif not list_mode:
                if in_var == "vvv":
                    elem = s_source.get_element().value
                    if elem in dir(builtins):
                        elem += "_"
                    while elem in self.var_types:
                        elem += "_" # TODO better GC for var_types (once all dependent rules are processed, remove from var_types)
                    else:
                        self.var_types[elem] = s_type
                    in_var = elem
                    s_source.get_variable().value = elem
                if not s_type:
                    self._append_py("for %s in (%s if isinstance(%s, list) else ([] if not %s else [%s])):" % tuple([in_var] + ([src] * 4)))
                else:
                    self._append_py("for %s in %s or []:" % (in_var, src))
            elif list_mode.value == "first":
                self._append_py("if len(%s) > 0:" % (src))
                self._append_py("    %s = %s[0]" % (in_var, src))
            elif list_mode.value == "last":
                self._append_py("if len(%s) > 0:" % (src))
                self._append_py("    %s = %s[-1]" % (in_var, src))
            elif list_mode.value == "not_first":
                self._append_py("for %s in %s[1:]:" % (in_var, src))
            elif list_mode.value == "not_last":
                self._append_py("for %s in %s[:-1]:" % (in_var, src))
            elif list_mode.value == "only_one":
                self._append_py("if len(%s) == 1:" % (src))
                self._append_py("    %s = %s[0]" % (in_var, src))
            else:
                raise BaseException("Unknown list mode %s" % list_mode.value)
            self.indent += 4
        if s_source.get_type():
            type_ = s_source.get_type().value
            self._append_py("if isinstance(%s, %s.%s):" % (in_var, self.i_module.__name__, type_))
            self.var_types[in_var] = getattr(self.i_module, type_)
            self.indent += 4
            indent += 4
        if s_source.get_condition():
            condition = self._handle_condition(s_source.get_condition().value, s_source)
            if condition is not None:
                self._append_py("if %s:" % condition)
                self.indent += 4
                indent += 4
        return 0, -1 * indent

    def _handle_source_no_rules(self, s_source):
        in_var = s_source.get_variable().value
        src = self._path(s_source)
        var_type = self.var_types.get(s_source.get_context().value)
        if var_type:
            elem = s_source.get_element().value
            var_type = getattr(var_type(), elem)
            if isinstance(var_type, list):
                self._append_py("for %s in %s:" % (in_var, src))
                self.indent += 4
                var_type, __ = self._get_type(s_source, set_var_type=False)
                if var_type and var_type.__name__ == "ResourceContainer":
                    self._append_py("%s = unpack_container(%s)" % (in_var, in_var))
                indent = 4
                if s_source.get_type():
                    type_ = s_source.get_type().value
                    self._append_py("if isinstance(%s, %s.%s):" % (in_var, self.i_module.__name__, type_))
                    self.var_types[in_var] = getattr(self.i_module, type_)
                    self.indent += 4
                    indent += 4
                if s_source.get_condition():
                    condition = self._handle_condition(s_source.get_condition().value, s_source)
                    if condition is not None:
                        self._append_py("if %s:" % condition)
                        self.indent += 4
                        indent += 4
                return 0, -1 * indent
            else:
                var_type, __ = self._get_type(s_source, set_var_type=False)
                if var_type and var_type.__name__ == "ResourceContainer":
                    self._append_py("%s = unpack_container(%s)" % (in_var, src))
                else:
                    self._append_py("%s = %s" % (in_var, src))
                indent = 0
                if s_source.get_type():
                    type_ = s_source.get_type().value
                    self._append_py("if isinstance(%s, %s.%s):" % (in_var, self.i_module.__name__, type_))
                    self.var_types[in_var] = getattr(self.i_module, type_)
                    self.indent += 4
                    indent += 4
                if s_source.get_condition():
                    condition = self._handle_condition(s_source.get_condition().value, s_source)
                    if condition is not None:
                        self._append_py("if %s:" % condition)
                        self.indent += 4
                        indent += 4
                return 0, -1 * indent
        else:
            self._append_py("# could not check if list (norules)")
            self._append_py("for %s in (%s if isinstance(%s, list) else ([] if not %s else [%s])):" % tuple([in_var] + ([src] * 4)))
            self.indent += 4
            var_type, __ = self._get_type(s_source, set_var_type=False)
            if var_type and var_type.__name__ == "ResourceContainer":
                self._append_py("%s = unpack_container(%s)" % (in_var, src))
            else:
                self._append_py("%s = %s" % (in_var, src))
            indent = 4
            if s_source.get_type():
                type_ = s_source.get_type().value
                self._append_py("if isinstance(%s, %s.%s):" % (in_var, self.i_module.__name__, type_))
                self.var_types[in_var] = getattr(self.i_module, type_)
                self.indent += 4
                indent += 4
            if s_source.get_condition():
                condition = self._handle_condition(s_source.get_condition().value, s_source)
                if condition is not None:
                    self._append_py("if %s:" % condition)
                    self.indent += 4
                    indent += 4
            return 0, -1 * indent

    def _get_out_var(self, out_var):
        if out_var in self.list_var_resolved:
            return "_" + out_var
        else:
            return out_var

    def _handle_target_assign(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value)
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        shared_rule_id = None
        if s_map_target.get_listMode() and s_map_target.get_listMode()[0].value == "share":
            shared_rule_id = s_map_target.get_listRuleId().value
            self._append_py("if '%s' in shared_vars:" % shared_rule_id)
            self._append_py("    %s = shared_vars['%s']" % (out_var, shared_rule_id))
            self._append_py("else:")
            self.indent += 4
        if not var_type:
            self._append_py("%s = %s # unknown type" % (out_var, trg))
        else:
            self._append_py("%s = %s()" % (out_var, var_type.__module__ + "." + var_type.__name__))
            if is_list:
                self._append_py("%s.append(%s)" % (trg, out_var))
            else:
                self._append_py("if %s is not None:" % trg)
                self._append_py("    %s = %s" % (out_var, trg))
                self._append_py("else:")
                self._append_py("    %s = %s" % (trg, out_var))
        if shared_rule_id:
            self.indent -= 4
            self._append_py("    shared_vars['%s'] = %s" % (shared_rule_id, out_var))

    def _handle_target_cast(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        in_var = self._get_out_var(s_map_target.get_parameter()[0].valueId.value)
        out_type = s_map_target.get_parameter()[1].valueString.value
        trg = self._path(s_map_target)
        var_type, __ = self._get_type(s_map_target)
        var_type_name = var_type.__name__ if var_type else None
        if not var_type:
            elems = {}
            if s_map_target.get_element():
                ctxType = self.var_types.get(s_map_target.get_context().value)
                for name, elem_type in inspect.get_annotations(ctxType.__init__).items():
                    if name.startswith(s_map_target.get_element().value):
                        elems[elem_type] = name
            elem = elems.get(out_type)
            if elem:
                trg = self._get_out_var(s_map_target.get_context().value) + "." + elem
                var_type_name = out_type
        # TODO implement other types: https://github.com/hapifhir/org.hl7.fhir.core/blob/890737e17b6922e7fe2bf5674675251dd258994b/org.hl7.fhir.r5/src/main/java/org/hl7/fhir/r5/utils/structuremap/StructureMapUtilities.java#L1822
        if out_type == "string":
            if var_type_name == "datetime":
                out_transform = "dateutil.parser.parse(str(%s))" % in_var
            elif var_type_name == "dateString":
                out_transform = "dateutil.parser.parse(str(%s)).isoformat()" % in_var
            elif var_type_name == "dateTime":
                out_transform = "dateTime(value=dateutil.parser.parse(%s).isoformat())" % in_var
            elif var_type_name == "str":
                out_transform = in_var
            elif var_type_name in ["string", "code"]:
                if hasattr(__import__(var_type.__module__) if var_type else self.o_module, "string"):
                    out_transform = "string(value=str(%s))" % in_var
                else:
                    out_transform = in_var
            else:
                if hasattr(__import__(var_type.__module__) if var_type else self.o_module, "string"):
                    out_transform = "string(value=str(%s)) # unknown cast %s -> string" % (in_var, var_type_name)
                else:
                    out_transform = "%s # unknown cast %s -> string" % (in_var, var_type_name)
            if out_var:
                self._append_py("%s = %s" % (out_var, out_transform))
                out_transform = out_var
            self._append_py("%s = %s" % (trg, out_transform))
        else:
            raise NotImplementedError("Unsupported cast %s -> %s (%s = %s)" % (var_type_name, out_type, trg, in_var))

    def _handle_target_uuid(self, s_map_target):
        trg = self._path(s_map_target)
        out_var = s_map_target.get_variable()
        if out_var:
            out_var = self._get_out_var(out_var.value)
            if hasattr(self.o_module, "string"):
                self._append_py("%s = string(value=%s)" % (out_var, self.uuid_method))
            else:
                self._append_py("%s = %s" % (out_var, self.uuid_method))
            self._append_py("%s = %s" % (trg, out_var))
            self.var_types[s_map_target.get_variable().value] = self.o_module.string
        else:
            if hasattr(self.o_module, "string"):
                self._append_py("%s = string(value=%s)" % (trg, self.uuid_method))
            else:
                self._append_py("%s = %s" % (trg, self.uuid_method))
        self.var_types[self._path(s_map_target, orig_var=True)] = self.o_module.string

    def _handle_target_append(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        var_type_name = var_type.__name__ if var_type else None
        params = []
        for param in s_map_target.get_parameter():
            if param.valueString:
                params.append("'%s'" % param.valueString.value)
            elif param.valueId:
                param_type, is_list_src = self._get_type(param.valueId.value, set_var_type=False)
                param_type = param_type.__name__ if param_type else None
                param = self._get_out_var(param.valueId.value)
                if param_type == "StrucDoc_Text":
                    params.append("%s.valueOf_" % (param))
                elif param_type == "string" or param_type == "str":
                    params.append("('' if %s is None else %s if isinstance(%s, str) else %s.value)" % tuple([param] * 4))
                else:
                    raise NotImplementedError("Unsupported type %s for append" % param_type)
            else:
                params.append("'unknown append type %s'" % param)
        if var_type_name == "string":
            if hasattr(__import__(var_type.__module__), "string"):
                out_transform = "string(value=(%s))" % " + ".join(params)
            else:
                out_transform = " + ".join(params)
        elif var_type_name == "uri":
            out_transform = "uri(value=(%s))" % " + ".join(params)
        elif var_type_name == "div":
            out_transform = "utils.builddiv(%s, %s)" % (self.o_module.__name__, " + ".join(params))
        else:
            raise NotImplementedError("Unsupported type %s for append" % param_type)
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_copy(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        src = s_map_target.get_parameter()[0]
        var_type, is_list = self._get_type(s_map_target)
        var_type_name = var_type.__name__ if var_type else None
        if src.valueString:
            trg = self._path(s_map_target, expected_type="string")
            # TODO add support for other types
            if var_type_name in ["string", "code", "canonical"]:
                if hasattr(__import__(var_type.__module__), "string"):
                    out_transform = "string(value='%s')" % src.valueString.value
                else:
                    out_transform = "'%s')" % src.valueString.value
            elif var_type_name == "uri":
                out_transform = "uri(value='%s')" % src.valueString.value
            elif var_type_name == "str":
                out_transform = "'%s'" % src.valueString.value
            elif var_type_name == "bool":
                out_transform = src.valueString.value.title()
            elif var_type_name == "dateTime":
                out_transform = "dateTime(value='%s')" % src.valueString.value
            elif var_type_name == "dateString":
                out_transform = "'%s'" % src.valueString.value
            elif var_type_name == "datetime":
                out_transform = "'%s'" % src.valueString.value
            elif inspect.get_annotations(var_type.__init__).get("value", "") == var_type_name + "Enum":
                if hasattr(__import__(var_type.__module__), "string"):
                    out_transform = "string(value='%s')" % src.valueString.value
                else:
                    out_transform = "'%s')" % src.valueString.value
            else:
                raise NotImplementedError("Unsupported type %s for copy (%s = %s)" % (var_type_name, trg, src.valueString.value))
        elif src.valueBoolean:
            trg = self._path(s_map_target, expected_type=None)
            if var_type_name == "bool":
                out_transform = str(src.valueBoolean.value)
        elif src.valueId:
            trg = self._path(s_map_target, expected_type=None)
            src_type, is_list_src = self._get_type(src.valueId.value, set_var_type=False)
            src_type_name = src_type.__name__ if src_type else None
            src = self._get_out_var(src.valueId.value)
            if not var_type_name:
                elems = {}
                if s_map_target.get_element():
                    ctxType = self.var_types.get(s_map_target.get_context().value)
                    for name, elem_type in inspect.get_annotations(ctxType.__init__).items():
                        if name.startswith(s_map_target.get_element().value):
                            elems[elem_type] = name
                elem = elems.get(src_type_name)
                if not elem and src_type_name == "str":
                    elem = elems.get("dateTime")
                    src_type_name = "dateTime"
                if elem:
                    trg = self._get_out_var(s_map_target.get_context().value) + "." + elem
                    var_type_name = src_type_name
            if var_type_name == "string":
                if src_type_name == "str":
                    if hasattr(__import__(var_type.__module__), "string"):
                        out_transform = "string(value=%s)" % src
                    else:
                        out_transform = src
                elif src_type_name == "string":
                    if hasattr(__import__(var_type.__module__), "string"):
                        out_transform = src
                    else:
                        out_transform = "%s.value" % src
                else:
                    raise BaseException("Unknown source type '%s' for string" % src_type_name)
            elif var_type_name == "str":
                if src_type_name is None:
                    out_transform = "%s # unknown source type for string" % src
                elif src_type_name == "str":
                    out_transform = src
                elif src_type_name == "datetime":
                    out_transform = src
                elif src_type_name in ["string", "code"]:
                    if hasattr(__import__(src_type.__module__), "string"):
                        out_transform = "%s.value" % src
                    else:
                        out_transform = src
                elif inspect.get_annotations(src_type.__init__).get("value", "") == src_type_name + "Enum":
                    out_transform = "%s.value" % src
                else:
                    raise BaseException("Unknown source type '%s' for string" % src_type_name)
            elif var_type_name == "dateTime":
                out_transform = "dateTime(value=dateutil.parser.parse(%s).isoformat())" % src
            elif var_type_name == "code":
                if hasattr(__import__(var_type.__module__), "string"):
                    out_transform = "string(value=%s)" % src
                else:
                    out_transform = src
            elif var_type_name == "bool":
                out_transform = src
            elif var_type_name == "int":
                out_transform = src
            elif var_type_name == "decimal":
                out_transform = "decimal(value=%s)" % src
            elif var_type_name == "markdown":
                if src_type_name == "ED":
                    out_transform = "utils.ed2markdown(%s, %s)" % (self.o_module.__name__, src)
                else:
                    raise BaseException("Unknown source type '%s' for markdown" % src_type_name)
            elif var_type_name == "div":
                if src_type_name == "StrucDoc_Text":
                    out_transform = "utils.strucdoctext2html(%s, %s)" % (self.o_module.__name__, src)
                elif src_type_name == "ED":
                    out_transform = "utils.ed2html(%s, %s)" % (self.o_module.__name__, src)
                else:
                    out_transform = "string(value=%s) # unknown source type for div %s" % (src, src_type_name)
            else:
                out_transform = src
        else:
            trg = self._path(s_map_target, expected_type=None)
            raise NotImplementedError("Unsupported parameter for copy (%s = ?)" % trg)
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_reference(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        src = self._get_out_var(s_map_target.get_parameter()[0].valueId.value)
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        self._append_py(f"if not {src}.id:")
        self._append_py(f"    {src}.id = string(value={self.uuid_method})")
        out_transform = f"string(value=type({src}).__name__ + '/' + {src}.id.value)"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_truncate(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        var_type_name = var_type.__name__ if var_type else None
        srcId = self._get_out_var(s_map_target.get_parameter()[0].valueId.value)
        param_type, is_list_src = self._get_type(srcId, set_var_type=False)
        param_type = param_type.__name__ if param_type else None
        if param_type == "StrucDoc_Text":
            param = "%s.valueOf_" % (srcId)
        elif param_type == "string" or param_type == "str" or param_type == "dateStringV3":
            param = "('' if %s is None else %s if isinstance(%s, str) else %s.value)" % tuple([srcId] * 4)
        else:
            raise NotImplementedError("Unsupported type %s for append" % param_type)
        srcLen = s_map_target.get_parameter()[1].valueInteger.value
        param += "[:%s]" % srcLen
        if var_type_name == "string" or var_type_name == "dateString":
            if hasattr(__import__(var_type.__module__), "string"):
                out_transform = "string(value=(%s))" % param
            else:
                out_transform = param
        elif var_type_name == "uri":
            out_transform = "uri(value=(%s))" % param
        else:
            raise NotImplementedError("Unsupported truncate %s for append" % param_type)
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_pointer(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        src = self._get_out_var(s_map_target.get_parameter()[0].valueId.value)
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        # TODO assign random UUID to id if None?
        out_transform = f"string(value='urn:uuid:' + {src}.id.value)"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_cc(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        params = []
        for param in s_map_target.get_parameter():
            if param.valueId:
                param = self._get_out_var(param.valueId.value)
                param = f"({param} if isinstance({param}, str) else {param}.value)"
            else:
                param = f"'{param.valueString.value}'"
            params.append(param)
        mod = self.o_module.__name__
        if len(params) == 1:
            out_transform = f"{mod}.CodeableConcept(text={mod}.string(value={params[0]}))"
        else:
            type_param = f", display={mod}.string(value={params[2]})" if len(params) > 2 else ""
            out_transform = f"{mod}.CodeableConcept(coding=[{mod}.Coding(system={mod}.uri(value={params[0]}), code={mod}.string(value={params[1]}){type_param})])"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_c(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        params = []
        for param in s_map_target.get_parameter():
            if param.valueId:
                param = self._get_out_var(param.valueId.value)
                param = f"({param} if isinstance({param}, str) else {param}.value)"
            else:
                param = f"'{param.valueString.value}'"
            params.append(param)
        mod = self.o_module.__name__
        type_param = f", display={mod}.string(value={params[2]})" if len(params) > 2 else ""
        out_transform = f"{mod}.Coding(system={mod}.uri(value={params[0]}), code={mod}.string(value={params[1]}){type_param})"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_id(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        params = []
        for param in s_map_target.get_parameter():
            if param.valueId:
                param = self._get_out_var(param.valueId.value)
                param = f"({param} if isinstance({param}, str) else {param}.value)"
            else:
                param = f"'{param.valueString.value}'"
            params.append(param)
        mod = self.o_module.__name__
        type_param = f", type_={mod}.CodeableConcept(coding=[{mod}.Coding(system={mod}.uri(value='http://terminology.hl7.org/3.1.0/CodeSystem-v2-0203'), code={mod}.string(value={params[2]}))])" if len(params) > 2 else ""
        out_transform = f"{mod}.Identifier(system={mod}.uri(value={params[0]}), value={mod}.string(value={params[1]}){type_param})"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_qty(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        params = []
        for param in s_map_target.get_parameter():
            if param.valueId:
                param = self._get_out_var(param.valueId.value)
                param = f"({param} if isinstance({param}, str) else {param}.value)"
            else:
                param = f"'{param.valueString.value}'"
            params.append(param)
        mod = self.o_module.__name__
        if len(params) == 1:
            out_transform=f"utils.qty_from_str({mod}, {param})"
        else:
            system_code = f", system={mod}.uri(value={params[2]}), code={mod}.string(value={params[3]})" if len(params) > 2 else ""
            out_transform = f"{mod}.Quantity(value={mod}.decimal(value={params[0]}), unit={mod}.string(value={params[1]}){system_code})"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_target_cp(self, s_map_target):
        out_var = self._get_out_var(s_map_target.get_variable().value) if s_map_target.get_variable() else None
        trg = self._path(s_map_target)
        var_type, is_list = self._get_type(s_map_target)
        params = []
        for param in s_map_target.get_parameter():
            if param.valueId:
                param = self._get_out_var(param.valueId.value)
                param = f"({param} if isinstance({param}, str) else {param}.value)"
            else:
                param = f"'{param.valueString.value}'"
            params.append(param)
        mod = self.o_module.__name__
        if len(params) == 1:
            out_transform = f"{mod}.ContactPoint(value={mod}.string(value={params[0]}))"
        else:
            out_transform = f"{mod}.ContactPoint(system={mod}.uri(value={params[0]}), value={mod}.string(value={params[1]}))"
        if out_var:
            self._append_py("%s = %s" % (out_var, out_transform))
            out_transform = out_var
        if is_list:
            self._append_py("%s.append(%s)" % (trg, out_transform))
        else:
            self._append_py("%s = %s" % (trg, out_transform))

    def _handle_fhirpath(self, s_value, var_type_, this=None, target_list=False, condition=False):

        def recover(e):
            raise e

        textStream = InputStream(s_value)

        lists = {}
        this_elem = {}
        var_types = dict(self.var_types)
        if this:
            src = self._path(this)
            src_type, src_list = self._get_type(this, set_var_type=False)
            for elem, type_str in inspect.get_annotations(src_type.__init__).items():
                this_elem[elem] = utils._get_type(src_type, elem)
            var_types["$this"] = src_type
            lists["$this"] = src_list
            var_types[src.split(".")[-1]] = src_type
            lists[src.split(".")[-1]] = src_list
            in_var = this.get_variable().value if this.get_variable() else None
            if in_var:
                src = in_var
        else:
            src = None
            src_list = False

        pythonListener = PythonListener(var_types, self.list_var, self.o_module if getattr(self.o_module, "string", None) else self.i_module, src, this_elem)
        errorListener = ErrorListener()

        lexer = FHIRPathLexer(textStream)
        lexer.recover = recover
        lexer.removeErrorListeners()
        lexer.addErrorListener(errorListener)

        parser = FHIRPathParser(CommonTokenStream(lexer))
        parser.buildParseTrees = True
        parser.removeErrorListeners()
        parser.addErrorListener(errorListener)

        walker = ParseTreeWalker()
        walker.walk(pythonListener, parser.expression())

        py_code = pythonListener.py_code()

        if target_list:
            result = pythonListener._get_new_varname()
        else:
            result = "utils.single(%s)" % py_code.code

        if condition:
            return result, None, None

        if not var_type_:
            # TODO improve type support
            out_type = py_code.out_type
            out_type_name = out_type.__name__ if out_type else None
            if out_type_name in ["str", "string"]:
                out_type = getattr(self.o_module, "string")
                var_type_name = "string"
            elif out_type_name == "boolean" or out_type_name == "bool":
                out_type = getattr(self.o_module, "boolean")
                var_type_name = "boolean"
            elif out_type_name == "dateTime" or out_type_name == "datetime":
                out_type = getattr(self.o_module, "dateTime")
                var_type_name = "dateTime"
            else:
                var_type_name = out_type_name
        else:
            out_type = var_type_
            value_annotation = inspect.get_annotations(var_type_.__init__).get("value", "")
            if value_annotation == var_type_.__name__ + "Enum":
                var_type_name = "enum"
            else:
                var_type_name = var_type_.__name__

        # TODO improve type support
        if var_type_name == "uri":
            target_value = 'uri(value=%s)' % result
        elif var_type_name == "enum":
            if py_code.out_type == str:
                target_value = "%s.%s(value=%s)" % (out_type.__module__, out_type.__name__, result)
            elif py_code.out_type == getattr(self.o_module, "string"):
                target_value = "%s.%s(value=%s.value)" % (out_type.__module__, out_type.__name__, result)
            else:
                target_value = "%s.%s(value=str(%s))" % (out_type.__module__, out_type.__name__, result)
        elif var_type_name == "string":
            if py_code.out_type == str:
                target_value = "string(value=%s)" % result
            elif py_code.out_type == getattr(self.o_module, "string"):
                target_value = result
            else:
                target_value = "string(value=str(%s))" % result
        elif var_type_name == "str":
            if py_code.out_type == str:
                target_value = result
            elif py_code.out_type == getattr(self.o_module, "string", str):
                target_value = "%s.value" % result
            else:
                target_value = "str(%s)" % result
        elif var_type_name == "base64Binary":
            target_value = 'base64Binary(value=%s)' % result
        elif var_type_name == "boolean":
            if py_code.out_type == bool:
                target_value = "boolean(value=%s)" % result
            elif py_code.out_type == getattr(self.o_module, "bool", bool):
                target_value = result
            else:
                target_value = "boolean(value=str(%s))" % result
        elif var_type_name == "bool":
            if py_code.out_type == bool:
                target_value = result
            elif py_code.out_type == getattr(self.o_module, "bool", bool):
                target_value = "%s.value" % result
            else:
                target_value = "bool(%s)" % result
        elif var_type_name == "dateTime":
            if py_code.out_type == datetime.datetime:
                target_value = "dateTime(value=%s.isoformat())" % result
            elif py_code.out_type == getattr(self.o_module, "dateTime", datetime.datetime):
                target_value = result
            else:
                target_value = "dateTime(value=dateutil.parser.parse(str(%s)).isoformat())" % result
        elif var_type_name == "datetime":
            if py_code.out_type == datetime.datetime:
                target_value = result
            elif py_code.out_type == getattr(self.o_module, "dateTime", datetime.datetime):
                target_value = "dateutil.parser.parse(%s.value)" % result
            else:
                target_value = "dateutil.parser.parse(str(%s))" % result
        else:
            target_value = result

        if target_list:
            return "[%s for %s in %s]" % (target_value, result, py_code.code), out_type, pythonListener._get_new_varname()
        else:
            return target_value, out_type, pythonListener._get_new_varname()

    def _handle_target_evaluate(self, s_map_target):
        out_var = s_map_target.get_variable()
        s_value = s_map_target.get_parameter()[0].valueString.value
        if s_map_target.get_context():
            var_type, is_list = self._get_type(s_map_target)
            var_type_name = var_type.__name__ if var_type else None
            is_list = self._is_list(s_map_target)
        else:
            var_type = None
            var_type_name = None
            is_list = True
        target_value, out_type, var_name = self._handle_fhirpath(s_value, var_type, target_list=is_list)
        if out_var:
            out_var = self._get_out_var(out_var.value)
        if out_type and not var_type_name:
            var_type_name = out_type.__name__
            if out_var:
                self.var_types[s_map_target.get_variable().value] = out_type

        if out_var:
            if is_list:
                self.list_var.add(out_var)
                self.none_check.discard(out_var)
            else:
                self.none_check.add(out_var)
                self.list_var.discard(out_var)
        if not is_list and s_map_target.get_context():
            self.none_check.add(self._path(s_map_target))
            self.list_var.discard(self._path(s_map_target))

        if not s_map_target.get_context():
            self._append_py("%s = %s" % (out_var, target_value))
        elif is_list:
            trg = self._path(s_map_target, expected_type=out_type)
            if out_var:
                self._append_py("%s = %s" % (out_var, target_value))
                target_value = out_var
            self._append_py("for %s in %s:" % (var_name, target_value))
            if var_type and var_type.__name__ == "ResourceContainer":
                var_name = "%s.ResourceContainer(%s=%s)" % (self.o_module.__name__, var_type_name, var_name)
            self._append_py("    %s.append(%s)" % (trg, var_name))
        else:
            self._assign_or_append(s_map_target, self._is_list(s_map_target), target_value, var_type_name, check_none=True)

    def _handle_target_create(self, s_map_source, s_map_target, s_map_dependent):
        src = self._path(s_map_source)
        trg = self._path(s_map_target)
        src_var = s_map_source.get_variable()
        src_var = self._get_out_var(src_var.value) if src_var else None
        src = src_var or src
        out_var = s_map_target.get_variable()
        out_var = self._get_out_var(out_var.value) if out_var else None
        if not s_map_target.get_parameter():
            var_type, is_list = self._get_type(s_map_target, set_var_type=False)
            var_type_name = var_type.__module__ + "." + var_type.__name__
            source_type, __ = self._get_type(src)
            if is_list:
                if out_var is not None and out_var != "vvv":
                    self._append_py("%s = %s()" % (out_var, var_type_name))
                    self._append_py("%s.append(%s)" % (trg, out_var))
                    trg = out_var
                else:
                    self._append_py("%s.append(%s())" % (trg, var_type_name))
                    trg = trg + "[-1]"
            else:
                if out_var is not None and out_var != "vvv":
                    self._append_py("%s = %s()" % (out_var, var_type_name))
                    self._append_py("%s = %s" % (trg, out_var))
                else:
                    self._append_py("%s = %s()" % (trg, var_type_name))
            if inspect.get_annotations(var_type.__init__).get(
                    "value") == var_type.__name__ + "Enum":  # special handling for required codes in FHIR
                var_type_name = "code"
                target_type = getattr(self.o_module, var_type_name)
            else:
                var_type_name = None
                target_type = var_type
            if source_type:
                group_name = self.default_types_maps.get((source_type, target_type))
                if not group_name or group_name in self.default_types_maps_subtypes:
                    self.transform_default_needed = True
                    if var_type_name:
                        self._append_py("transform_default(%s, %s, %s)" % (src, trg, var_type_name))
                    else:
                        self._append_py("transform_default(%s, %s)" % (src, trg))
                else:
                    self._append_py("%s(%s, %s)" % (group_name, src, trg))
            elif var_type_name:
                self.transform_default_needed = True
                self._append_py("transform_default(%s, %s, %s)" % (src, trg, var_type_name))
            else:
                self.transform_default_needed = True
                self._append_py("transform_default(%s, %s)" % (src, trg))
        else:
            type_param = s_map_target.get_parameter()[0].get_valueString().value
            if out_var:
                self.var_types[s_map_target.get_variable().value] = getattr(self.o_module, type_param)
            self._assign_or_append(s_map_target, self._is_list(s_map_target), "%s.%s()" % (self.o_module.__name__, type_param), type_param)

    # from https://hl7.org/fhir/R4B/mapping-language.html:
    # Params:
    #    source, 
    #    map_uri, 
    #    output	
    # use the translate operation. The source is some type of code or coded datatype, and the source and map_uri are passed to the translate operation. 
    # The output determines what value from the translate operation is used for the result of the operation (code, system, display, Coding, or CodeableConcept)
    def _handle_target_translate(self, s_map_target):                  
        # TODO handle if param.valueString.value.startswith("#"):
        # TODO check if parameter[0] could sometimes be a valueString instead of valueId.value 
        self._append_py("trans_out = translate(url='%s', code=(%s if isinstance(%s, str) else %s.value), silent=False)" % tuple([s_map_target.parameter[1].valueString.value[1:]]+[s_map_target.parameter[0].valueId.value] * 3))

        self._append_py("matches = [match['concept'] for match in trans_out['match'] if match['equivalence']=='equivalent' or match['equivalence']=='equal']")
        if s_map_target.parameter[2].valueString.value == "CodeableConcept":
            self._append_py("match = fhir4.CodeableConcept(coding=[fhir4.Coding(system=fhir4.uri(value=match['system']), version=fhir4.string(value=match['version']), code=fhir4.string(value=match['code']), display=fhir4.string(value=match['display']), userSelected=fhir4.string(value=match['userSelected'])) for match in matches])")
        else:
            self._append_py("# if there are mutliple 'equivalent' or 'equal' matches and CodeableConcept is not the output param, than throw an error") 
            self._append_py("if len(matches) != 1: sys.exit(\"There are none or multiple 'equivalent' or 'equal' matches in the results of the translate and output type is not CodeableConcept!\")") 
            if s_map_target.parameter[2].valueString.value == "Coding":
                self._append_py("match = fhir4.Coding(system=fhir4.uri(value=matches[0]['system']), version=fhir4.string(value=matches[0]['version']), code=fhir4.string(value=matches[0]['code']), display=fhir4.string(value=matches[0]['display']), userSelected=fhir4.string(value=matches[0]['userSelected']))")
            elif s_map_target.parameter[2].valueString.value in ["code", "system", "display"]:
                self._append_py("match = matches[0]['"+s_map_target.parameter[2].valueString.value+"']")
            else: 
                sys.exit("Following translate output param is unknown: "+s_map_target.parameter[2].valueString.value)

        if hasattr(self.o_module, "string") and s_map_target.parameter[2].valueString.value not in ["CodeableConcept", "Coding"]:
            self._assign_or_append(s_map_target, self._is_list(s_map_target), "string(value=match)", s_map_target.parameter[2].valueString.value)
        else:
            self._assign_or_append(s_map_target, self._is_list(s_map_target), "match", s_map_target.parameter[2].valueString.value)

    def _is_list(self, source_or_target):
        var_type = self.var_types.get(source_or_target.get_context().value)
        if var_type and source_or_target.get_element():
            if source_or_target.get_element().value:
                elem = source_or_target.get_element().value
                var_type = getattr(var_type(), elem, None)
            if var_type is not None and isinstance(var_type, list):
                return True
        return False

    def _assign_or_append(self, s_map_target, is_list, py_code, out_type=None, check_none=False):
        trg = self._path(s_map_target, expected_type=out_type)
        out_var = s_map_target.get_variable()
        if out_var:
            out_var = self._get_out_var(out_var.value)
            self._append_py("%s = %s" % (out_var, py_code))
            py_code = out_var
        var_type, __ = self._get_type(s_map_target, set_var_type=False)
        indent = 0
        if is_list and check_none:
            indent += 4
        if var_type and var_type.__name__ == "ResourceContainer":
            py_code = "%s.ResourceContainer(%s=%s)" % (self.o_module.__name__, out_type, py_code)
        if is_list:
            self._append_py((" " * indent) + "%s.append(%s)" % (trg, py_code))
        else:
            self._append_py("%s = %s" % (trg, py_code))

    def _get_type(self, lookup, set_var_type=True):
        if isinstance(lookup, str):
            parts = lookup.split(".")
            context = parts[0]
            element = parts[1] if len(parts) == 2 else None
            variable = None
        else:
            context = lookup.get_context().value
            element = lookup.get_element().value if lookup.get_element() else None
            orig_var = lookup.get_variable().value if lookup.get_variable() else None
            variable = self._get_out_var(orig_var)
        var_type = self.var_types.get(context)
        var_type, is_list = utils._get_type(var_type, element)
        if variable and set_var_type:
            self.var_types[orig_var] = var_type
        return var_type, is_list

    def _append_py(self, code=""):
        self.py_code += (" " * self.indent) + code + "\n"

    def _path(self, src_or_trg, expected_type=None, orig_var=False):
        elem = src_or_trg.get_element()
        elem_value = elem.value if elem else None
        if elem_value == "type":
            elem_value = "type_"
        elif elem_value == "class":
            elem_value = "class_"
        ctx = src_or_trg.get_context()
        if ctx and elem:
            ctx = self._get_out_var(ctx.value) if not orig_var else ctx.value
            elem_type = self._get_type(src_or_trg, set_var_type=False)
            ctx_type = self._get_type(ctx, set_var_type=False)
            if elem_value == "data" and ctx_type[0] and "data" not in inspect.get_annotations(ctx_type[0].__init__):
                elem_value = "valueOf_"
            if ctx_type[0] and not elem_type[0]:
                ex_type = expected_type[0].upper() + expected_type[1:] if expected_type else None
                if expected_type and self._get_type(ctx + "." + elem_value + ex_type, set_var_type=False):
                    elem_value = elem_value + ex_type
            return ctx + "." + elem_value
        elif elem:
            return elem_value
        else:
            return self._get_out_var(ctx.value) if not orig_var else ctx.value

supermod.StructureMap.subclass = StructureMapSub
# end class StructureMapSub

class StructureDefinitionSub(supermod.StructureDefinition, malac_hd.ConvertMaster):
    def __init__(self, id=None, meta=None, implicitRules=None, language=None, text=None, contained=None, extension=None, modifierExtension=None, url=None, identifier=None, version=None, name=None, title=None, status=None, experimental=None, date=None, publisher=None, contact=None, description=None, useContext=None, jurisdiction=None, purpose=None, copyright=None, keyword=None, fhirVersion=None, mapping=None, kind=None, abstract=None, context=None, contextInvariant=None, type_=None, baseDefinition=None, derivation=None, snapshot=None, differential=None, **kwargs_):
        super(StructureDefinitionSub, self).__init__(id, meta, implicitRules, language, text, contained, extension, modifierExtension, url, identifier, version, name, title, status, experimental, date, publisher, contact, description, useContext, jurisdiction, purpose, copyright, keyword, fhirVersion, mapping, kind, abstract, context, contextInvariant, type_, baseDefinition, derivation, snapshot, differential,  **kwargs_)

    def convert(self, input, o_module):
        pass

supermod.StructureDefinition.subclass = StructureDefinitionSub
# end class StructureDefinitionSub

class ConceptMapSub(supermod.ConceptMap, malac_hd.ConvertMaster):   
    from collections import defaultdict
    con_map_6d = None

    def __init__(self, id=None, meta=None, implicitRules=None, language=None, text=None, contained=None, extension=None, modifierExtension=None, url=None, identifier=None, version=None, name=None, title=None, status=None, experimental=None, date=None, publisher=None, contact=None, description=None, useContext=None, jurisdiction=None, purpose=None, copyright=None, sourceUri=None, sourceCanonical=None, targetUri=None, targetCanonical=None, group=None, **kwargs_):
        super(ConceptMapSub, self).__init__(id, meta, implicitRules, language, text, contained, extension, modifierExtension, url, identifier, version, name, title, status, experimental, date, publisher, contact, description, useContext, jurisdiction, purpose, copyright, sourceUri, sourceCanonical, targetUri, targetCanonical, group,  **kwargs_)

    # the source param is not beeing handled, because there are no known possbilities of importing/including conceptMaps in other conceptMaps
    def convert(self, silent=True, return_header_and_footer_for_standalone=True, source=".", return_translate_def=True, return_dict_add=True):
        self.py_code = ""
        if return_header_and_footer_for_standalone:
            self._header_standalone()
        if return_translate_def:
            self._header_translate_def()
        if return_dict_add:
            self.con_map_6d = self.nested_dict(5, list)
            for one_group in self.get_group():
                for one_element in one_group.get_element():
                    for one_target in one_element.get_target():
                        self.con_map_6d \
                            [tmp.get_value() if (tmp := self.get_sourceUri()) or (tmp := self.get_sourceCanonical()) else "%"] \
                            [tmp.get_value() if (tmp := self.get_targetUri()) or (tmp := self.get_targetCanonical()) else "%"] \
                            [tmp.get_value() if (tmp := one_group.get_source()) else "%"] \
                            [tmp.get_value() if (tmp := one_group.get_target()) else "%"] \
                            [tmp.get_value() if (tmp := one_element.get_code()) else "%"] \
                            .append({"equivalence":one_target.get_equivalence().get_value(), 
                                    "concept":{
                                        "system": tmp.get_value() if (tmp := one_group.get_source()) else "", 
                                        "version": "", # TODO version of codesystem out of url?
                                        "code": tmp.get_value() if (tmp := one_target.get_code()) else "",
                                        "display": tmp.get_value() if (tmp := one_target.get_display()) else "",
                                        "userSelected": False},
                                    "source": tmp.get_value() if (tmp := self.get_url()) or (tmp := self.get_id()) else "%"})
                
                if unmapped := one_group.get_unmapped(): # using as many unsafe url characters here, that really should not be used as system id/url or codes of a real codesystem, like [ ] { } | \ ” % ~ # < >
                    tmp_equivalence = "inexact"
                    tmp_code = ""
                    tmp_display = ""
                    tmp_code_lvl = ""
                    if unmapped.get_mode().get_value() == "provided":
                        tmp_code_lvl = "|"
                        tmp_equivalence = "equal"
                        tmp_code = "|" + (tmp.get_value() if (tmp := unmapped.get_code()) else "") 
                    elif unmapped.get_mode().get_value() == "fixed":
                        tmp_code_lvl = "~"
                        tmp_code = "~"+unmapped.get_code().get_value() # must be given if fixed
                        tmp_display = unmapped.get_display().get_value() or ""
                    elif unmapped.get_mode().get_value() == "other-map":
                        #tmp_match = "lambda: translate(url="+unmapped.get_url()+", conceptMapVersion=conceptMapVersion, code=code, system=system, version=version, source=source, coding=coding, codeableConcept=codeableConcept, target=target, targetsystem=targetsystem, reverse=reverse, silent=silent)" # cant access the varibales inside the dict...
                        tmp_code_lvl = "#"
                        tmp_code = "#"+unmapped.get_url().get_value()
                    else:
                        sys.exit(unmapped.get_mode()+" as mode for unmapped is not defined! Please use the modes from https://hl7.org/fhir/R4B/valueset-conceptmap-unmapped-mode.html .")
                    self.con_map_6d \
                        [tmp.get_value() if (tmp := self.get_sourceUri()) or (tmp := self.get_sourceCanonical()) else "%"] \
                        [tmp.get_value() if (tmp := self.get_targetUri()) or (tmp := self.get_targetCanonical()) else "%"] \
                        [tmp.get_value() if (tmp := one_group.get_source()) else "%"] \
                        [tmp.get_value() if (tmp := one_group.get_target()) else "%"] \
                        [tmp_code_lvl] \
                        .append({"equivalence":tmp_equivalence, 
                                "concept":{
                                    "system": "", 
                                    "version": "", # TODO version of codesystem out of url?
                                    "code": tmp_code,
                                    "display": tmp_display,
                                    "userSelected": False},
                                "source": tmp.get_value() if (tmp := self.get_url()) or (tmp := self.get_id()) else "%"})

            self.py_code += "\n"
            j_dump = json.dumps(self.con_map_6d, indent=4)
            # replace all the json lower case bools to python usual pascal case bools
            j_dump = j_dump.replace(": false",": False").replace(": true",": True")
            self.py_code += 'con_map_7d["'+(tmp.get_value() if (tmp := self.get_url()) or (tmp := self.get_id()) else "%")+'"] = ' + j_dump
            
        if return_header_and_footer_for_standalone:
            self._footer_standalone()

        if not silent:
            print("\n%s" % self.py_code)
        
        return self.py_code
        
    # from https://stackoverflow.com/a/39819609/6012216
    def nested_dict(self, n, type): 
        if n == 1:
            return self.defaultdict(type)
        else:
            return self.defaultdict(lambda: self.nested_dict(n-1, type))  

    def _header_standalone(self):
        self.py_code += '''import argparse
import time
import fhir4
import sys

description_text = "This has been compiled by the MApping LAnguage Compiler for Health Data, short MaLaC-HD. See arguments for more details."

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description_text)
    parser.add_argument(
       '-c', '--code', help="""code --	
The code that is to be translated. If a code is provided, a system must be provided""",
        required=True
    )
    parser.add_argument(
       '-sys', '--system', help="""uri --	
The system for the code that is to be translated""",
        required=True
    )
    parser.add_argument(
       '-ver', '--version', help="""string --	
The version of the system, if one was provided in the source data""",
        required=True
    )
    parser.add_argument(
       '-s', '--source', help="""uri --	
Identifies the value set used when the concept (system/code pair) was chosen. 
May be a logical id, or an absolute or relative location. 
The source value set is an optional parameter because in some cases, the client cannot know what the source value set is. 
However, without a source value set, the server may be unable to safely identify an applicable concept map, 
and would return an error. For this reason, a source value set SHOULD always be provided. 
Note that servers may be able to identify an appropriate concept map without a source value set 
if there is a full mapping for the entire code system in the concept map, or by manual intervention""",
        required=True
    )
    parser.add_argument(
       '-cg', '--coding', help="""Coding --	
A coding to translate""",
        required=True
    )
    parser.add_argument(
       '-cc', '--codeableConcept', help="""CodeableConcept --	
A full codeableConcept to validate. The server can translate any of the coding values (e.g. existing translations) 
as it chooses""",
        required=True
    )
    parser.add_argument(
       '-t', '--target', help="""uri -- 
Identifies the value set in which a translation is sought. May be a logical id, or an absolute or relative location. 
If there's no target specified, the server should return all known translations, along with their source""", 
        required=True
    )
    parser.add_argument(
       '-ts', '--targetsystem', help="""uri -- 
Identifies a target code system in which a mapping is sought. This parameter is an alternative to the target parameter - 
only one is required. Searching for any translation to a target code system irrespective of the context (e.g. target valueset) 
may lead to unsafe results, and it is at the discretion of the server to decide when to support this operation""", 
        required=True
    )
    parser.add_argument(
       '-r', '--reverse', action="store_true", help="""boolean -- True if stated, else False -- 
If this is true, then the operation should return all the codes that might be mapped to this code. 
This parameter reverses the meaning of the source and target parameters""", 
        required=True
    )
    parser.add_argument(
       '-st', '--silent', action="store_true", help="""boolean -- True if stated, else False --
Do not print the converted python mapping to console""", 
        required=True
    )
    return parser
'''

    def _header_translate_def(self): 
        self.py_code += '''
# output
# 1..1 result (boolean)
# 0..1 message with error details for human (string)
# 0..* match with (list)
#   0..1 equivalnce (string from https://hl7.org/fhir/R4B/valueset-concept-map-equivalence.html)
#   0..1 concept
#       0..1 system
#       0..1 version
#       0..1 code
#       0..1 display 
#       0..1 userSelected will always be false, because this is a translation
#   0..1 source (conceptMap url)
# TODO implement reverse
def translate(url=None, conceptMapVersion=None, code=None, system=None, version=None, source=None, coding=None, codeableConcept=None, target=None, targetsystem=None, reverse=None, silent=False)\
              -> dict [bool, str, list[dict[str, dict[str, str, str, str, bool], str]]]:
    start = time.time()
    
    # start validation and recall of translate in simple from
    if codeableConcept:
        if isinstance(codeableConcept, str): 
            codeableConcept = fhir4.parseString(codeableConcept, silent)
        elif isinstance(coding, fhir4.CodeableConcept):
            pass
        else:
            sys.exit("The codeableConcept parameter has to be a string or a CodeableConcept Object (called method as library)!")
        # the first fit will be returned, else the last unfitted value will be returned
        # TODO check translate params
        for one_coding in codeableConcept.get_coding:
            if (ret := translate(url=url, source=source, coding=one_coding, 
                                 target=target, targetsystem=targetsystem, 
                                 reverse=reverse, silent=True))[0]:
                return ret
        else: return ret
        
    elif coding:
        if isinstance(coding, str): 
            coding = fhir4.parseString(coding, silent)
        elif isinstance(coding, fhir4.Coding):
            pass
        else:
            sys.exit("The coding parameter has to be a string or a Coding Object (called method as library)!")
        # TODO check translate params
        return translate(url=url,  source=source, coding=one_coding, 
                         target=target, targetsystem=targetsystem, 
                         reverse=reverse, silent=True)
        
    elif code:
        if not isinstance(code,str): 
            sys.exit("The code parameter has to be a string!")
        
    elif target:
        if not isinstance(code,str): 
            sys.exit("The target parameter has to be a string!")
        
    elif targetsystem:
        if not isinstance(code,str): 
            sys.exit("The targetsystem parameter has to be a string!")
        
    else:
        sys.exit("At least codeableConcept, coding, code, target or targetSystem has to be given!")
    # end validation and recall of translate in simplier from

    # look for any information from the one ore more generated conceptMaps into con_map_7d
    match = []
    unmapped = []
    if url not in con_map_7d.keys():
        print('   #ERROR# ConceptMap with URL "'+ url +'" is not loaded to this compiled conceptMap #ERROR#')
    else:
        for url_lvl in con_map_7d:
            if url_lvl == "%" or url_lvl == str(url or ""):#+str(("/?version=" and conceptMapVersion) or ""):
                for source_lvl in con_map_7d[url_lvl]:
                    if source_lvl == "%" or not source or source_lvl == source:
                        for target_lvl in con_map_7d[url_lvl][source_lvl]:
                            if target_lvl == "%" or not target or target_lvl == target:
                                for system_lvl in con_map_7d[url_lvl][source_lvl][target_lvl]:
                                    if system_lvl == "%" or not system or system_lvl == system:#+str(("/?version=" and version) or ""):
                                        for targetsystem_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl]:
                                            if targetsystem_lvl == "%" or not targetsystem or targetsystem_lvl == targetsystem:
                                                for code_lvl in con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl]:
                                                    if code_lvl == "|" or code_lvl == "~" or code_lvl == "#":
                                                        unmapped += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]
                                                    if code_lvl == "%" or not code or code_lvl == code:
                                                        match += con_map_7d[url_lvl][source_lvl][target_lvl][system_lvl][targetsystem_lvl][code_lvl]                
                                                    
    if not match:
        for one_unmapped in unmapped:
            tmp_system = ""
            tmp_version = ""
            tmp_code = ""
            tmp_display = ""
            # replace all "|" values with to translated code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            if one_unmapped["concept"]["code"].startswith("|"):
                tmp_system = system
                tmp_version = version
                tmp_code = one_unmapped["concept"]["code"][1:] + code
            # replace all "~" values with fixed code (provided from https://hl7.org/fhir/R4B/conceptmap-definitions.html#ConceptMap.group.unmapped.mode)
            elif one_unmapped["concept"]["code"].startswith("~"):
                tmp_code = one_unmapped["concept"]["code"][1:]
                tmp_display = one_unmapped["concept"]["display"]
            elif one_unmapped["concept"]["code"].startswith("#"):
                # TODO detect recursion like conceptMapA -> conceptMapB -> ConceptMapA -> ...
                return translate(one_unmapped["concept"]["code"][1:], None, code, system, version, source, 
                                 coding, codeableConcept, target, targetsystem, reverse, silent)
            match.append({"equivalence": one_unmapped["equivalence"], 
                          "concept":{
                            "system": tmp_system, 
                            "version": tmp_version, # TODO version of codesystem out of url?
                            "code": tmp_code,
                            "display": tmp_display,
                            "userSelected": False},
                          "source": one_unmapped["source"]})
                
            
    # see if any match is not "unmatched" or "disjoint"
    result = False
    message = ""
    for one_match in match:
        if one_match["equivalence"] != "unmatched" and one_match["equivalence"] != "disjoint":
            result = True 

    if not silent:
        print('Translation in '+str(round(time.time()-start,3))+' seconds for code "'+code+'" with ConceptMap "'+url+'"')
    return {"result": result, "message": message, "match": match}

# The con_map_7d is a seven dimensional dictionary, for quickly finding the fitting translation
# All dimensions except the last are optional, so a explicit NONE value will be used as key and 
# interpreted as the default key, that always will be fitting, no matter what other keys are fitting.
# If a version is included (purely optional), than the version will be added with a blank before to the key
#
# The 0th dimension is mandatory and stating the ConceptMap with its url (including the version).
#
# The 1st dimension is optional and stating the SOURCE valueset (including the version), as one conceptMap can only 
# have a maximum of one SOURCE, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 2nd dimension is optional and stating the TARGET valueset (including the version), as one conceptMap can only 
# have a maximum of one TARGET, this is reserved for MaLaC-HD ability to process multiple ConceptMaps in one output.
#
# The 3th dimension is optional and stating the SYSTEM (including the version) from the source valueset code, as one 
# code could be used in multiple SYSTEMs from the source valueset to translate. 
# Not stating a SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this SYSTEM element as stating the valueset, that should be
# stated in source, this case will still be supported by MaLaC-HD. Having a conceptMap with a source valueset 
# and a different SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#
# The 4th dimension is optional and stating the TARGET SYSTEM (including the version) from the target valueset code, as one 
# code could be used in multiple SYSTEMs from the target valueset to translate. 
# Not stating a TARGET SYSTEM with a code, is not FHIR compliant and not a whole concept, but still a valid conceptmap.
# As many conceptMaps exists that are worngly using this TARGET SYSTEM element as stating the target valueset, that should be
# stated in target, this case will still be supported by MaLaC-HD. Having a conceptMap with a target valueset 
# and a different TARGET SYSTEM valueset will result in an impossible match and an error will not be recognized by MaLaC-HD.
#   
# The 5th dimension is optional and stating the CODE from the source valueset, as one conceptMap can have none or 
# multiple CODEs from the source to translate. 
#
# The 6th dimension is NOT optional and stating the TARGET CODE from the target valueset. As one source code could be translated 
# in multiple TARGET CODEs, the whole set have to be returend. 
# For a translation with explicitly no TARGET CODE, because of an quivalence of unmatched or disjoint, NONE will be returned. 
#   
# a minimal example, translating "hi" to "servus": 
# con_map_7d = {"myConMap": {None: {None: {"hi": {None: {None: ["equivalent", "<coding><code>servus</code></coding>", "https://my.concept.map/conceptMap/my"]}}}}}
#
# TODO add a dimension for a specific dependsOn property
# TODO add a solution for the unmapped element
con_map_7d = {}
'''

    def _footer_standalone(self): 
        self.py_code += '''
        
if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    ret = translate(args.code, args.system, args.version, args.source, args.coding, 
    args.codeableConcept, args.target, args.targetsystem, args.reverse, args.silent)'''

supermod.ConceptMap.subclass = ConceptMapSub
# end class ConceptMapSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'AdministrativeGender'
        rootClass = supermod.AdministrativeGender
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'AdministrativeGender'
        rootClass = supermod.AdministrativeGender
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from io import BytesIO as StringIO
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'AdministrativeGender'
        rootClass = supermod.AdministrativeGender
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'AdministrativeGender'
        rootClass = supermod.AdministrativeGender
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
